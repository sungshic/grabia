# README #

GraBIA (Graph-Based Image Analyser) is a software tool to automate the extraction and the analysis of single-cell-level data from raw images produced by time-lapse microscopy. 

This software was designed to serve different purposes for three different user groups: endusers, algorithm aficionados, and developers. 

* Endusers include synthetic/systems biologists who are interested in achieving automated characterisation pipelines for time-lapse microscopy. 
* Algorithm aficionados include computer scientists who are interested in assignment problems and the graph-based evaluation of temporal correspondence of moving objects. 
* Developers include software engineers who are interested in learning to develop software for process automation, workflow management, and IoT. 

## Information for lab experimentalists (endusers) ##
GraBIA can be used to streamline the characterisation of synthetically designed genetic circuits based on time-lapse microscopy. 

## Information for algorithm aficionados ##
### Cost function ###
![Screen Shot 2016-06-21 at 2.56.39 PM.png](https://bitbucket.org/repo/kX98bq/images/210380341-Screen%20Shot%202016-06-21%20at%202.56.39%20PM.png)

Given the above equation as a cost function, the problem of finding temporal correspondence can be considered in the light of the [general assignment problem](https://en.wikipedia.org/wiki/Assignment_problem). The cost function is used to evaluate the magnitude of geometrical differences between a pair of cells. As an example, two cell clusters from successive time points were processed through the image processing steps as shown in the above figure. Polygonal data representative of the single cells at time t0 and t1 were sampled. Single cells being sampled at time t1 were subject to the extra step of scaling their dimensions to match that of the t0 cell undergoing comparison. The lower the value returned by the cost function between a pair of single cells, the more closely shaped their geometrical features are.    

### Graph-based correspondence inference ###
![Screen Shot 2016-06-21 at 2.53.35 PM.png](https://bitbucket.org/repo/kX98bq/images/4003387783-Screen%20Shot%202016-06-21%20at%202.53.35%20PM.png)

Single-cell level data can be encoded into a graph data structure as shown above. Each node is representative of the combinatorial pairing choices available from any cells in the clusters shown on the right hand side. The graph path highlighted in red is the shortest weighted path found by [Dikjstra's algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm), which forms the basis of inferring the temporal cell correspondences between the two clusters.  

### Cell lineages and event-driven subsets of statistical groups ###
![Screen Shot 2016-06-21 at 8.01.51 PM.png](https://bitbucket.org/repo/kX98bq/images/3328249648-Screen%20Shot%202016-06-21%20at%208.01.51%20PM.png)

## Information for developers ##
The software's GUI was designed to showcase a proof-of-concept event-handling framework in which all interactions can be defined out of workflows. Such a workflow centric view enabled the handling of events in a systematic and robust manner. GUI elements are encapsulated in units of "Scene". A Scene can have any combination of GUI elements including buttons, text boxes, graphic displays, etc.
      