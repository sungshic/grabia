from kivy.app import App
from kivy.factory import Factory
from gbia_screens.preprocessing.landmarkchooser import ImageCropWidget
#Factory.register("GBIAImageEditor",GBIAImageEditor)
#Factory.register("ImageCropWidget",ImageCropWidget)

cw = ImageCropWidget(img_src='testimg.jpg') # GBIAImageEditor(img_src='testimg.jpg') #img_src=    "photo.jpeg"    )


class MyApp(App):
    def build(self):
        return cw


ap=MyApp()


if __name__ == '__main__':
    ap.run()
