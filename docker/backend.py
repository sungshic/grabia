__author__='spark'

import os
import subprocess
import re

from time import sleep
from threading import Event as ThreadEvent
import logging

class BackendManager(object):
    stop = ThreadEvent()
    is_initialized = ThreadEvent()
    DEFAULT_MACHINE_NAME = 'docker-vm'
    DEFAULT_CONTAINER_NAME = 'docker_gbiabackend'
    machine_name = None
    container_name = None
    logger = logging.getLogger('grabia_app.docker.backend.BackendManager')
    env_dict = {}

    def __init__(self, machine_name=None, container_name=None):
        self.env_dict = dict(os.environ)
        self.is_initialized.clear()
        if machine_name:
            self.machine_name = machine_name
        else:
            self.machine_name = BackendManager.DEFAULT_MACHINE_NAME

        if container_name:
            self.container_name = container_name
        else:
            self.container_name = BackendManager.DEFAULT_CONTAINER_NAME

    # def initializeBackend(self):
    #     # create a docker machine if not available
    #     if not self.isDockerMachineCreated(self.machine_name):
    #         self.logger.debug('BBBBBBBBBBBB Creating a docker machine: ' + self.machine_name)
    #         self.createDockerMachine(self.machine_name)
    #         print self.machine_name, ' created.'
    #         # poll until the docker machine is created
    #         while not self.isDockerMachineRunning(self.machine_name):
    #             if self.stop.is_set():
    #                 return
    #             #self.logger.debug('BBBBBBBBBBBBBBBBB polling for the backend machine to run.')
    #             sleep(1)
    #         print self.machine_name, ' running...'
    #     elif not self.isDockerMachineRunning(self.machine_name):
    #         self.logger.debug('BBBBBBBBBBBBB Starting an existing docker machine: ' + self.machine_name)
    #         self.restartDockerMachine(self.machine_name)
    #         self.regenerateMachineCerts(self.machine_name)
    #         # poll until the docker machine is created
    #         while not self.isDockerMachineRunning(self.machine_name):
    #             if self.stop.is_set():
    #                 return
    #             #self.logger.debug('BBBBBBBBBBBBBBBBB polling for the backend machine to run.')
    #             sleep(1)
    #         # self.regenerateMachineCerts(self.machine_name) # regenerate cert again just in case things might have changed
    #         self.logger.debug(self.machine_name + ' started.')
    #
    #     # set up docker env
    #     self.logger.debug('BBBBBBBBBBBBBBB setting up docker env for ' + self.machine_name)
    #     self.setupDockerEnv(self.machine_name)
    #     if self.isBackendContainerRunning(self.container_name):
    #         self.restartTheBackend()
    #     else:
    #         self.startTheBackend()
    #     self.logger.debug('BBBBBBBBBBBBBBBB Backend container, ' + self.container_name + ' started.')
    #
    #     # poll until the container is running
    #     while not self.isBackendContainerRunning(self.container_name):
    #         if self.stop.is_set():
    #             return
    #         #self.logger.debug('BBBBBBBBBBBBBBBBB polling for the backend container to run.')
    #         sleep(1)
    #     self.logger.debug('BBBBBBBBBBBBBBBBB Backend initialization completed.')
    #     self.is_initialized.set()


# edit by spark to bypass docker-machine
#    def isDockerMachineCreated(self, machine_name):
#        cmd_ret_str = subprocess.check_output(['docker-machine', 'ls', '-q', '--filter', 'name=(^'+machine_name+'$)'], env=self.env_dict)
#        self.logger.debug('BBBBBBBBBBBBBBBBB Backend created, docker-machine ls: '+cmd_ret_str)
#        sleep(1)
#        if cmd_ret_str.strip('\n') == machine_name:
#            return True
#        else:
#            return False

# edit by spark to bypass docker-machine
#    def isDockerMachineRunning(self, machine_name):
#        return True # edit by spark to bypass docker-machine
#        cmd_ret_str = subprocess.check_output(['docker-machine', 'ls', '-q', '--filter', 'name=(^'+machine_name+'$)', '--filter', 'state=running'], env=self.env_dict)
#        self.logger.debug('BBBBBBBBBBBBBBBBB Backend running, docker-machine ls: '+cmd_ret_str)
#        sleep(1)
#        if cmd_ret_str.strip('\n') == machine_name:
#            return True
#        else:
#            return False

    def isBackendContainerRunning(self, container_name):
        cmd_ret_str = subprocess.check_output(['docker', 'ps', '-q', '--filter', 'name='+container_name, '--filter', 'status=running'], env=self.env_dict)
        self.logger.debug('BBBBBBBBBBBBBBBBB Backend docker ps: '+cmd_ret_str)
        sleep(1)
        if cmd_ret_str:
            return True
        else:
            return False

# edit by spark to bypass docker-machine
#    def regenerateMachineCerts(self, machine_name):
#        p_handle = subprocess.Popen(['docker-machine', 'regenerate-certs', '-f', machine_name], env=self.env_dict)
#        return p_handle


# edit by spark to bypass docker-machine
#    def createDockerMachine(self, machine_name):
#        p_handle = subprocess.Popen(['docker-machine', 'create', '--driver', 'virtualbox', '--virtualbox-cpu-count', '2', machine_name],
#                                    env=self.env_dict, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
#        return p_handle


# edit by spark to bypass docker-machine
#    def startDockerMachine(self, machine_name):
#        p_handle = subprocess.Popen(['docker-machine', 'start', machine_name], env=self.env_dict,
#                                    stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
#        return p_handle

# edit by spark to bypass docker-machine
#    def restartDockerMachine(self, machine_name):
#        p_handle = subprocess.Popen(['docker-machine', 'restart', machine_name], env=self.env_dict,
#                                    stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
#        return p_handle

    # this method sets up the global env to match those required by the gbia docker-machine
    # It might cause conflict on platforms where other docker-machine already exists
    # For future releases, it would be desirable to have these env vars set up
    # confined in the local context of gbia_backend
# edit by spark to bypass docker-machine
#    def setupDockerEnv(self, machine_name):
#        vm_env_str = subprocess.check_output(['docker-machine', 'env', machine_name])
#        re_ret = re.findall('\s*export\s*(\S*)="(\S*)"\s*\n*', vm_env_str)
#
#        for env_var in re_ret:
#            self.env_dict[env_var[0]] = env_var[1]
#            self.logger.debug('Backend env: ' + str(env_var))
#        self.logger.debug('BBBBBBBBBBBBBBBBB docker env: '+str(self.env_dict))

    def stopTheBackend(self):
        p_handle = subprocess.Popen(['docker-compose', '-f', '/grabia/docker/docker-compose.yml', 'stop'], env=self.env_dict,
                                    stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        return p_handle

    def startTheBackend(self):
        p_handle = subprocess.Popen(['docker-compose', '-f', '/grabia/docker/docker-compose.yml', 'up', '-d'], env=self.env_dict,
                                    stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        return p_handle

    def restartTheBackend(self):
        p_handle = subprocess.Popen(['docker-compose', '-f', '/grabia/docker/docker-compose.yml', 'restart'], env=self.env_dict,
                                    stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        return p_handle


# edit by spark to bypass docker-machine
#    def getTheMachineIP(self, machine_name):
#        ip_str = None
#        if self.isDockerMachineCreated(machine_name):
#            ip_str = subprocess.check_output(['docker-machine', 'ip', machine_name])
#
#        return ip_str.strip('\n')



