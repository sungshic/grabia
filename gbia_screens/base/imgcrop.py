
from kivy.uix.widget import Widget
from kivy.uix.button import Button

from kivy.uix.scatter import Scatter
from kivy.uix.image import Image, AsyncImage
from kivy.core.image import Image as CoreImage
#from kivy.core.image import ImageData
from kivy.uix.boxlayout import BoxLayout

from kivy.properties import ObjectProperty,  NumericProperty, StringProperty

from kivy.graphics import Line, Rectangle, Color
#from kivy.graphics.texture import Texture
#from kivy.core.image.img_pygame import ImageLoaderPygame
#from kivy.core.image.img_pil import ImageLoaderPIL

from kivy.lang import Builder

from gbia_screens.manager import WFScreen
from kivy.clock import Clock

from PIL import Image as PILImage
from StringIO import StringIO
from itertools import izip


Builder.load_string('''
<ImageCropWidget>:
    scatter: _scatter
    rect: _rect
    image: _image
    toolbox: _toolbox
    Scatter:
        id: _scatter
        pos: self.pos
        size: root.size
        Image:
            id: _image
            pos: self.pos
            size: root.size
            source: root.img_src
    BoxLayout:
        id: _toolbox
        size_hint: (1,None)
        orientation: 'horizontal'
    Widget:
        id: _rect
        size: (root.width/3, root.height/3)
        background_color: [0.,1.,0.,0.80]
''')



class ImageCropWidget(Widget):
    scatter = ObjectProperty(None)
    image = ObjectProperty(None)
    rect = ObjectProperty(None)
    toolbox = ObjectProperty(None)
    xmin = NumericProperty(20)
    xmax = NumericProperty(40)
    ymin = NumericProperty(20)
    ymax = NumericProperty(40)
    saveFilepath = StringProperty(None)

    img_src = StringProperty(None)

    root_screen_ref = ObjectProperty(None)

    cur_selection_area = None
    cur_selection_pos = None
    cur_selection_size = None

    def __init__(self, is_finished=None, **kwargs):
        print 'debugggggg ', kwargs
        super(ImageCropWidget, self).__init__(**kwargs)
        #self.bind(size = self.rescale_rec)
        #self.oldsize = self.size
        print self.size
        print self.pos
        #self.hideLimits()
        #self.defaultLimits()
        self._resizing = []

    # for pairing up (x,y) coord tuples from a single long tuple with alternating x and y coords
    def encodeCoordsFromList(self, t, size=2):
        it = iter(t)
        return izip(*[it]*size)

    def saveCroppedImage(self, imgtexture, imgsize, coord_list):
        kivy_coords_list = list(self.encodeCoordsFromList(coord_list))
        img_coord_list = [self.convertKivyPosToMPLPos(coord, self.image, imgtexture.size) for coord in kivy_coords_list]
        #img_coord_list = [self.convertKivyPosToMPLPos(coord, self.image, imgsize) for coord in kivy_coords_list]

        print 'kivy_coord_list: ', kivy_coords_list
        print 'img_coord_list: ', img_coord_list
        (x_list, y_list) = zip(*img_coord_list)
        xmin = min(x_list)
        xmax = max(x_list)
        ymin = min(y_list)
        ymax = max(y_list)
        #crop_box = shapely.box(xmin, ymin, xmax, ymax)
        #texture_size = imgtexture.size
        #size_ratio = max(float(texture_size[0])/imgsize[0], float(texture_size[1])/imgsize[1])

        print 'SAVECROOOOPEDIMAGE ', self.saveFilepath
        crop_texture = imgtexture.get_region(xmin, ymin, (xmax-xmin), (ymax-ymin))
        im = PILImage.frombytes(str.upper(crop_texture.colorfmt), crop_texture.size, crop_texture.pixels) #StringIO(imgdata))
        im.save(self.saveFilepath) #'test_spark.png')

    def convertKivyPosToMPLPos(self, touch_pos, kivy_widget, orig_img_size):
        kivy_pos = kivy_widget.pos
        ##memimg_width = self.pil_image.size[0] ##self.memimage.img_loader.width
        ##memimg_height = self.pil_image.size[1] ##self.memimage.img_loader.height
        img_width = orig_img_size[0] #self.image.width
        img_height = orig_img_size[1] #self.image.height
        #memimg_len = max(memimg_width, memimg_height)
        kivyimg_width = kivy_widget.width
        kivyimg_height = kivy_widget.height
        #kivy_len = min(kivy_widget_width, kivy_widget_height)

        # convert from Kivy to GBIA coordinate
        width_ratio = float(img_width) / kivyimg_width
        height_ratio = float(img_height) / kivyimg_height
        print 'img size: ', img_width, img_height
        print 'ratios: ', width_ratio, height_ratio

        ratio = max(width_ratio, height_ratio)
        print 'KivyToGBIA ratio: ', ratio

        img_kivy_width = img_width / ratio
        img_kivy_height = img_height / ratio

        print 'img kivy size: ', img_kivy_width, img_kivy_height

        offset_pos = (abs(kivyimg_width - img_kivy_width)/2.0, abs(kivyimg_height - img_kivy_height)/2.0)
        print 'offset_pos: ', offset_pos
        #gbia_xpos = touch_pos[0]-self.image.x-offset_pos[0]
        #gbia_ypos = touch_pos[1]-self.image.y-offset_pos[1]
        gbia_xpos = touch_pos[0]-offset_pos[0]
        gbia_ypos = touch_pos[1]-offset_pos[1]

        # convert from GBIA to MPL coordinate and finally return MPL coordinate
        mpl_width_ratio = img_width / img_kivy_width
        mpl_height_ratio = img_height / img_kivy_height

        #print 'convertion: ', touch_pos, gbia_xpos, gbia_ypos, mpl_width_ratio, mpl_height_ratio

        return (gbia_xpos * mpl_width_ratio, gbia_ypos * mpl_height_ratio)

    def clearCurAreaSelection(self):
        if self.cur_selection_area:
            self.canvas.remove(self.cur_selection_area)
        self.cur_selection_area = None

    def on_touch_down(self, touch):
        # if the touch occurred outside the widget
        if not self.collide_point(*touch.pos):
            # see if the super class knows how to handle this touch
            super(ImageCropWidget,self).on_touch_down(touch)
        elif touch.button in( 'left'): # if left mouse button is clicked inside the widget
            print "debuggggg left click ", touch.pos
            self.cur_selection_pos = touch.pos # keep a record of the first mouse click position
            if self.cur_selection_area:
                self.canvas.remove(self.cur_selection_area)
            self.cur_selection_area = None

        return True

    def on_touch_move(self, touch):
        # if the touch occurred inside the widget
        if self.collide_point(*touch.pos):
            cur_pos = touch.pos
            cur_size = (cur_pos[0] - self.cur_selection_pos[0], cur_pos[1] - self.cur_selection_pos[1])
            if self.cur_selection_area is None:
                with self.canvas:
                    self.cur_selection_area = Line(rectangle=self.cur_selection_pos+cur_size) # initialize
            else:
                with self.canvas:
                    self.cur_selection_area.rectangle = self.cur_selection_pos+cur_size # update size
            self.cur_selection_size = cur_size
        else:
            super(ImageCropWidget,self).on_touch_move(touch)

        return True

    def on_touch_up(self, touch):
        if self.cur_selection_area:
            print 'selection: ', self.cur_selection_area.points, self.cur_selection_area.points.__class__
            print 'image: ', self.image.pos, self.image.size, self.cur_selection_size
            print 'scatter pos: ', self.scatter.pos
            print 'scale: ', self.scatter.scale
            print dir(self.image), self.image.texture#.pixels.__class__
            self.saveCroppedImage(self.image.texture, self.image.size, self.cur_selection_area.points) #, self.cur_selection_size)
            self.root_screen_ref.next_btn.disabled = False
        # if the touch occurred outside the widget
        if not self.collide_point(*touch.pos):
            super(ImageCropWidget,self).on_touch_up(touch)
        return True

