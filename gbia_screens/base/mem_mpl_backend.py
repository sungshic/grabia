__author__='spark'

from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty
from kivy.uix.image import Image, AsyncImage
from kivy.core.image.img_pygame import ImageLoaderPygame

from matplotlib.backend_bases import MouseEvent
import StringIO
import logging

MPL_DPI=100

Builder.load_string('''
<GBIAMemoryImage>:
    memory_data: None
    mpl_fig_ref: None
''')


class GBIAMemoryImage(AsyncImage):
    """Display an image already loaded in memory."""
    memory_data = ObjectProperty(None)
    img_loader = ObjectProperty(None)
    mpl_fig_ref = ObjectProperty(None)
    # Kivy 1.9 does not support uix.Image to be initialized from memory-based image data.
    # matplotlib integration through image transfer requires the use of memory-based image data
    # the following hack will save image data into a temporary file for Kivy to be happy for now...
    _filename = '_tmp_io_file.png'
    logger = logging.getLogger('grabia_app.gbia_screens.statistics.lineages.GBIAMemoryImage')

    def __init__(self, **kwargs): #(self, memory_data, **kwargs):
        super(GBIAMemoryImage, self).__init__(**kwargs)

        print 'debuggg: MemoryImage initializing... '
        #self.memory_data = memory_data
        if self.memory_data:
            ##data = StringIO.StringIO(self.memory_data)
            self.img_loader = ImageLoaderPygame(filename=self._filename, ext='png', inline=True, rawdata=self.memory_data, nocache=True)


    def on_memory_data(self, *args):
        """Load image from memory."""
        #data = StringIO.StringIO(self.memory_data)
        if self.memory_data:
            #data = io.BytesIO(self.memory_data)
            ##data = StringIO.StringIO(self.memory_data)
            ####with open(self._filename, 'wb') as f:
            ####	f.write(data.read())
            #CoreImage can be instantiated from memory-based image data
            #loaded_img = CoreImage(data, ext="png")
            #with self.canvas:
                #AsyncImage(source='self.filename')
            #print '#################################: ', self._filename
            #self.img_loader = ImageLoaderPygame(self._filename, nocache=True)
            #self.img_loader.filename = self._filename
            ##self.img_loader._data = self.img_loader.load(data) #self._filename)
            ##self.texture = Texture.create_from_data(self.img_loader._data, self.img_loader._mipmap)
            ####self.img_loader = ImageLoaderPygame(filename=self._filename, nocache=True)
            self.img_loader = ImageLoaderPygame(filename=self._filename, ext='png', inline=True, rawdata=self.memory_data, nocache=True)
            self.texture = self.img_loader.texture
            ##self.img_loader = ImageLoaderPygame(filename=None, ext='png', inline=True, rawdata=self.memory_data, nocache=True)
                ##self.add_widget(Image(source=self._filename))
                #im._coreimage = loaded_img
                #self.add_widget(im)
                 #Rectangle(texture=loaded_img.texture)

    def on_touch_move(self, touch):
        #print "debuggggg mouse moved ", touch.pos
        ##buf, size = self.fig.canvas.print_to_buffer()
        ##self.pil_image = PILImage.frombuffer('RGBA', size, buf, 'raw', 'RGBA', 0, 1)
        ##self.fig_disp_data = ImageData(self.pil_image.size[0], self.pil_image.size[1], 'rgba', self.pil_image.tobytes(), source=None)
        ##self.memimage.memory_data = self.fig_disp_data
        #print 'on touch move...'
        super_ret = super(GBIAMemoryImage, self).on_touch_move(touch)

        #self.fig_disp_data = StringIO.StringIO()
        #self.mplcanvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
        #self.memimage.memory_data = self.fig_disp_data##.getvalue()
        mpl_event_name = 'motion_notify_event'
        #(mpl_x, mpl_y) = self.convertKivyPosToMPLPos(touch.pos, self.memimage)
        (mpl_x, mpl_y) = self.convertKivyPosToMPLPos(touch.pos, self, self.mpl_fig_ref.canvas.get_width_height())
        mpl_mouseevent = MouseEvent(mpl_event_name, self.mpl_fig_ref.canvas, x=mpl_x, y=mpl_y, button=1, key=self.mpl_fig_ref.canvas._key, step=0, dblclick=touch.is_double_tap, guiEvent=None)
        self.mpl_fig_ref.canvas.callbacks.process(mpl_event_name, mpl_mouseevent) # trigger mpl mouse event
        self.mpl_fig_ref.canvas.draw()
        self.updateMPLScreen()
        return super_ret

    def on_touch_up(self, touch):
        #print 'ON TOUCH UPPPPPPPP'
        super_ret = super(GBIAMemoryImage, self).on_touch_up(touch)
        mpl_event_name2 = 'button_release_event'

        #(mpl_x, mpl_y) = self.convertKivyPosToMPLPos(touch.pos, self.memimage)
        (mpl_x, mpl_y) = self.convertKivyPosToMPLPos(touch.pos, self, self.mpl_fig_ref.canvas.get_width_height())
        #print 'calculated mpl_xy: ', mpl_x, mpl_y

        #(mpl_x, mpl_y) = (75, 452)
        #ax_bbox = self.cellbrowser.ax_ps0_btn.get_position()
        #ax_min = ax_bbox.min
        #ax_max = ax_bbox.max
        #midpts = ((ax_max[0]-ax_min[0])/2.0, (ax_max[1]-ax_min[1])/2.0)
        #canv_dim = self.cellbrowser.mpl_fig_ref.canvas.get_width_height()
        #(mpl_x, mpl_y) = (canv_dim[0]*midpts[0], canv_dim[1]*midpts[1])
        #print 'deadreckoned mpl_xy: ', mpl_x, mpl_y

        #print 'canvas size: ', self.cellbrowser.mpl_fig_ref.canvas.get_width_height()
        #(mpl_x, mpl_y) = (50, 93)

        mpl_mouseevent2 = MouseEvent(mpl_event_name2, self.mpl_fig_ref.canvas, x=mpl_x, y=mpl_y, button=1, key=self.mpl_fig_ref.canvas._key, step=0, dblclick=touch.is_double_tap, guiEvent=None)
        self.mpl_fig_ref.canvas.callbacks.process(mpl_event_name2, mpl_mouseevent2) # trigger mpl mouse event
        self.mpl_fig_ref.canvas.draw()
        self.updateMPLScreen()

        return super_ret

    def on_touch_down(self, touch):
        #print 'ON TOUCH DOWWWWWWWWWWWWWWWWWWWWWWN'
        super_ret = super(GBIAMemoryImage, self).on_touch_down(touch)
        # if the touch occurred outside the widget
        #if not self.collide_point(*touch.pos):
        # see if the super class knows how to handle this touch
        #    return super(GBIAStatsScreen, self).on_touch_down(touch)
        #elif touch.button in( 'left'): # if left mouse button is clicked inside the widget
        if self.collide_point(*touch.pos) and touch.button in( 'left'): # if left mouse button is clicked inside the widget
            #print "debuggggg left click, touch.pos ", self.to_window(*touch.pos)
            #print "debuggggg left click, self.pos ", self.pos
            #print "debuggggg left click, self.scatter.pos ", self.scatter.pos, self.scatter.x, self.scatter.y
            #print "debuggggg left click, self.boxlayout.pos ", self.convertKivyPosToMPLPos(touch.pos, self.boxlayout)
            #print "debuggggg left click, self.memimage.pos ", self.memimage.pos, self.memimage.width, self.memimage.height, self.memimage.img_loader.size
            #			self.cur_selection_pos = touch.pos # keep a record of the first mouse click position
            #			if self.cur_selection_area:
            #				self.canvas.remove(self.cur_selection_area)
            #			self.cur_selection_area = None
            mpl_event_name = 'button_press_event'
            #mpl_event_name2 = 'button_release_event'

            #(mpl_x, mpl_y) = self.convertKivyPosToMPLPos(touch.pos, self.memimage, self.cellbrowser.mpl_fig_ref.canvas.get_width_height())
            (mpl_x, mpl_y) = self.convertKivyPosToMPLPos(touch.pos, self, self.mpl_fig_ref.canvas.get_width_height())

            #ax_bbox = self.cellbrowser.ax_ps0_btn.get_position()
            #ax_min = ax_bbox.min
            #ax_max = ax_bbox.max
            #midpts = ((ax_max[0]-ax_min[0])/2.0, (ax_max[1]-ax_min[1])/2.0)
            #canv_dim = self.cellbrowser.mpl_fig_ref.canvas.get_width_height()
            #(mpl_x, mpl_y) = (canv_dim[0]*midpts[0], canv_dim[1]*midpts[1])
            #print 'touch pos: ', touch.pos
            #print 'deadreckoned mpl_xy: ', mpl_x, mpl_y

            #(mpl_x, mpl_y) = (50, 93)

            mpl_mouseevent = MouseEvent(mpl_event_name, self.mpl_fig_ref.canvas, x=mpl_x, y=mpl_y, button=1, key=self.mpl_fig_ref.canvas._key, step=0, dblclick=touch.is_double_tap, guiEvent=None)
            #mpl_mouseevent2 = MouseEvent(mpl_event_name2, self.cellbrowser.mpl_fig_ref.canvas, x=mpl_x, y=mpl_y, button=1, key=self.cellbrowser.mpl_fig_ref.canvas._key, step=0, dblclick=touch.is_double_tap, guiEvent=None)
            self.mpl_fig_ref.canvas.callbacks.process(mpl_event_name, mpl_mouseevent) # trigger mpl mouse event
            self.mpl_fig_ref.canvas.draw()
            #self.cellbrowser.mpl_fig_ref.canvas.callbacks.process(mpl_event_name2, mpl_mouseevent2) # trigger mpl mouse event

            #self.tmp_cur_data = [a * 2 for a in self.tmp_cur_data]
            #self.axe.plot(self.tmp_cur_data)
            #plt.ioff()
            #plt.draw()
            ##buf, size = self.fig.canvas.print_to_buffer()
            ##self.pil_image = PILImage.frombuffer('RGBA', size, buf, 'raw', 'RGBA', 0, 1)
            ##self.fig_disp_data = ImageData(self.pil_image.size[0], self.pil_image.size[1], 'rgba', self.pil_image.tobytes(), source=None)
            ##self.memimage.memory_data = self.fig_disp_data

            ####self.fig_disp_data = StringIO.StringIO()
            ####self.mplcanvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
            #print 'disp mem class: ', str(self.fig_disp_data.__class__)
            ####self.memimage.memory_data = self.fig_disp_data##.getvalue()
            #self.memimage.on_memory_data()
            self.updateMPLScreen()

            #return True
        return super_ret # pass the touch event to others in the chain

    def updateMPLScreen(self):
        self.fig_disp_data = StringIO.StringIO()
        self.mpl_fig_ref.canvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
        #print 'disp mem class: ', str(self.fig_disp_data.__class__)
        self.memory_data = self.fig_disp_data##.getvalue()

    def convertKivyPosToMPLPos(self, touch_pos, kivy_widget, mpl_canvas_size):
        kivy_pos = kivy_widget.pos
        self.logger.debug('CONVVVVVVVVERT Kivy Pos To MPL Pos: ' + str(kivy_pos))
        #memimg_width = self.memimage.img_loader.width
        #memimg_height = self.memimage.img_loader.height
        mpl_width = mpl_canvas_size[0]
        mpl_height = mpl_canvas_size[1]
        kivyimg_width = kivy_widget.width
        kivyimg_height = kivy_widget.height

        # convert from Kivy to GBIA coordinate
        width_ratio = float(mpl_width) / kivyimg_width
        height_ratio = float(mpl_height) / kivyimg_height
        self.logger.debug('CONVVVVVVVVERT Kivy Pos To MPL Pos mpl to kivy ratio: ' + str((width_ratio, height_ratio)))
        #print 'memimg size: ', memimg_width, memimg_height
        #print 'ratios: ', width_ratio, height_ratio

        ratio = max(width_ratio, height_ratio)
        #print 'KivyToGBIA ratio: ', ratio

        #memimg_kivy_width = memimg_width / ratio
        #memimg_kivy_height = memimg_height / ratio
        img_kivy_width = mpl_width / ratio
        img_kivy_height = mpl_height / ratio

        #print 'img kivy size: ', img_kivy_width, img_kivy_height

        offset_pos = (abs(kivyimg_width - img_kivy_width), abs(kivyimg_height - img_kivy_height))
        if ratio < 1.0:
            offset_pos = (offset_pos[0]/2.0, offset_pos[1]/2.0)

#        if height_ratio < 1.0:
#            kivy_pos = (kivy_pos[0]/2.0, kivy_pos[1]/2.0)

        self.logger.debug('CONVVVVVVVVERT Kivy Pos To MPL Pos, offset_pos: ' + str(offset_pos))
        self.logger.debug('CONVVVVVVVVERT Kivy Pos To MPL Pos, touch_pos: ' + str(touch_pos))
        #print 'offset_pos: ', offset_pos
        #gbia_xpos = touch_pos[0]-self.memimage.x-offset_pos[0]
        #gbia_ypos = touch_pos[1]-self.memimage.y-offset_pos[1]
        gbia_xpos = touch_pos[0]-offset_pos[0]-kivy_pos[0]
        ##gbia_ypos = touch_pos[1]-offset_pos[1]
        gbia_ypos = touch_pos[1]-offset_pos[1]-kivy_pos[1]

        # convert from GBIA to MPL coordinate and finally return MPL coordinate
        mpl_width_ratio = mpl_width / img_kivy_width
        mpl_height_ratio = mpl_height / img_kivy_height



        #print 'convertion: ', touch_pos, gbia_xpos, gbia_ypos, mpl_width_ratio, mpl_height_ratio
        mpl_x = gbia_xpos * mpl_width_ratio
        mpl_y = gbia_ypos * mpl_height_ratio
        self.logger.debug('CONVVVVVVVVERT Kivy Pos To MPL Pos, mpl_pos: ' + str((mpl_x, mpl_y)))
        #import pdb; pdb.set_trace()
        #return (gbia_xpos * mpl_width_ratio, gbia_ypos * mpl_height_ratio)
        return (mpl_x, mpl_y)

