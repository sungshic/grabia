__author__ = 'spark'
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from mpldatacursor import HighlightingDataCursor, DataCursor
from matplotlib.widgets import Button, Slider
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import time
import pydot as pydot

#sys.path.append('gbimageanalyzer/core')


import pickle

import json
import shapy # need to use pure python port of shapely due to portability issues
import networkx as nx

from gbimageanalyzer import celery_client_task

import logging

logger = logging.getLogger('grabia_app.gbia_screens.base.mplstat')

class ZoomPan:
    def __init__(self):
        self.press = None
        self.cur_xlim = None
        self.cur_ylim = None
        self.x0 = None
        self.y0 = None
        self.x1 = None
        self.y1 = None
        self.xpress = None
        self.ypress = None
 
 
    def zoom_factory(self, ax, base_scale = 2.):
        def zoom(event):
            cur_xlim = ax.get_xlim()
            cur_ylim = ax.get_ylim()
 
            xdata = event.xdata # get event x location
            ydata = event.ydata # get event y location
 
            if event.button == 'up':
                # deal with zoom out
                scale_factor = 1 / base_scale
            elif event.button == 'down':
                # deal with zoom in
                scale_factor = base_scale
            else:
                # deal with something that should never happen
                scale_factor = 1
                print event.button
 
            new_width = (cur_xlim[1] - cur_xlim[0]) * scale_factor
            new_height = (cur_ylim[1] - cur_ylim[0]) * scale_factor
 
            relx = (cur_xlim[1] - xdata)/(cur_xlim[1] - cur_xlim[0])
            rely = (cur_ylim[1] - ydata)/(cur_ylim[1] - cur_ylim[0])
 
            ax.set_xlim([xdata - new_width * (1-relx), xdata + new_width * (relx)])
            ax.set_ylim([ydata - new_height * (1-rely), ydata + new_height * (rely)])
            #ax.figure.canvas.draw()
            ###plt.draw()
 
        fig = ax.get_figure() # get the figure of interest
        fig.canvas.mpl_connect('scroll_event', zoom)
 
        return zoom
 
    def pan_factory(self, ax):
        def onPress(event):
            if event.inaxes != ax: return
            self.cur_xlim = ax.get_xlim()
            self.cur_ylim = ax.get_ylim()
            self.press = self.x0, self.y0, event.xdata, event.ydata
            self.x0, self.y0, self.xpress, self.ypress = self.press
 
        def onRelease(event):
            self.press = None
            #ax.figure.canvas.draw()
            ###plt.draw()
 
        def onMotion(event):
            if self.press is None: return
            if event.inaxes != ax: return
            dx = event.xdata - self.xpress
            dy = event.ydata - self.ypress
            self.cur_xlim -= dx
            self.cur_ylim -= dy
            ax.set_xlim(self.cur_xlim)
            ax.set_ylim(self.cur_ylim)
            #ax.figure.canvas.draw()
            ###plt.draw()
 
        fig = ax.get_figure() # get the figure of interest
 
        # attach the call back
        fig.canvas.mpl_connect('button_press_event',onPress)
        fig.canvas.mpl_connect('button_release_event',onRelease)
        fig.canvas.mpl_connect('motion_notify_event',onMotion)
 
        #return the function
        return onMotion



class ClickPlot:

    """
    A clickable matplotlib figure

    Usage:
    >>> import clickplot
    >>> retval = clickplot.showClickPlot()
    >>> print retval['subPlot']
    >>> print retval['x']
    >>> print retval['y']
    >>> print retval['comment']

    See an example below
    """

    def __init__(self, cellbrowser, fig=None):

        """
        Constructor

        Arguments:
        fig -- a matplotlib figure
        """

        print 'CLLLLLLLLLLLLLLLLLLLICKPLOT fig', fig, fig.canvas._key
        if fig != None:
            self.fig = fig
        else:
            self.fig = plt.get_current_fig_manager().canvas.figure
        self.nSubPlots = len(self.fig.axes)
        self.dragFrom = None
        self.comment = '0'
        self.markers = []

        self.retVal = {'comment' : self.comment, 'x' : None, 'y' : None,
            'subPlot' : None}

        self.sanityCheck()
        #self.supTitle = plt.suptitle("comment: %s" % self.comment)
        print 'INNNNNNNNNIIIIIIIIIIIIITTTTTTTTT ClickPlot'
        self.fig.canvas.mpl_connect('button_press_event', self.onClick)
        self.fig.canvas.mpl_connect('button_release_event', self.onRelease)
        #self.fig.canvas.mpl_connect('scroll_event', self.onScroll)
        #self.fig.canvas.mpl_connect('key_press_event', self.onKey)
        self.cellbrowser = cellbrowser
        self.baseimg_cache = {}
        self.cellimg_cache = {}

    def clearMarker(self):

        """Remove marker from retVal and plot"""

        self.retVal['x'] = None
        self.retVal['y'] = None
        self.retVal['subPlot'] = None
        for i in range(self.nSubPlots):
            subPlot = self.selectSubPlot(i)
            for marker in self.markers:
                if marker in subPlot.lines:
                    subPlot.lines.remove(marker)
        self.markers = []
        #self.fig.canvas.draw()
        ###plt.draw()
        self.fig.canvas.draw()

    def getSubPlotNr(self, event):

        """
        Get the nr of the subplot that has been clicked

        Arguments:
        event -- an event

        Returns:
        A number or None if no subplot has been clicked
        """

        i = 0
        axisNr = None
        for axis in self.fig.axes:
            if axis == event.inaxes:
                axisNr = i
                break
            i += 1
        return axisNr

    def sanityCheck(self):

        """Prints some warnings if the plot is not correct"""

        subPlot = self.selectSubPlot(0)
        minX = subPlot.dataLim.min[0]
        maxX = subPlot.dataLim.max[0]
        for i in range(self.nSubPlots):
            subPlot = self.selectSubPlot(i)
            _minX = subPlot.dataLim.min[0]
            _maxX = subPlot.dataLim.max[0]
            if abs(_minX-minX) != 0 or (_maxX-maxX) != 0:
                import warnings
                warnings.warn('Not all subplots have the same X-axis')

    def draw(self):
        self.fig.canvas.draw()

    def show(self):

        """
        Show the plot

        Returns:
        A dictionary with information about the response
        """

        #plt.show()
        self.fig.show()
        self.retVal['comment'] = self.comment
        return self.retVal

    def selectSubPlot(self, i):

        """
        Select a subplot

        Arguments:
        i -- the nr of the subplot to select

        Returns:
        A subplot
        """

        #plt.subplot('%d1%d' % (self.nSubPlots, i+1))
        return self.fig.axes[i]


    def onClick(self, event):

        """
        Process a mouse click event. If a mouse is right clicked within a
        subplot, the return value is set to a (subPlotNr, xVal, yVal) tuple and
        the plot is closed. With right-clicking and dragging, the plot can be
        moved.

        Arguments:
        event -- a MouseEvent event
        """
        print 'MPLLLLLLLLLLLLLLLL ON CLICK', self.cellbrowser.mpl_widgets[0], self.cellbrowser.mpl_widgets[0].active
        #self.cellbrowser.mpl_widgets[0]._click(event)
        subPlotNr = self.getSubPlotNr(event)
        if subPlotNr == None:
            return

        self.myeventlog = event
        # find out if an artist in the cellline subplot is clicked
        clicked_subplot = self.selectSubPlot(subPlotNr)
        if clicked_subplot == self.cellbrowser.ax2:
            plot_artists = clicked_subplot.artists
            print 'onClick cellline subplot clicked'
            clicked_artists = [a for a in plot_artists if a.patch.contains(event)[0]] # find a list of artists that coincide with the mouseevent
            if len(clicked_artists) > 0:
                print 'onClick cellline artist clicked', clicked_artists
                # currently only assumes that only a single artist is clicked
                cur_artist = clicked_artists[0]
                if cur_artist in self.cellbrowser.hdcursor.cellline_artistLUT:
                    celluuid = self.cellbrowser.hdcursor.cellline_artistLUT[clicked_artists[0]]
                    print 'onClick cellline artist has a cell uuid', celluuid
                    # highlight the currently chosen artist in red
                    # this involves reverting the previously chosen one, if any,
                    # to blue
                    #prev_line_artist = self.getHighlightingArtist(celluuid)
                    if self.cellbrowser.hdcursor.cur_cell_ab != None:
                        if self.cur_line_artist: # if previous line artist is None
                            self.cellbrowser.hdcursor.cur_cell_ab.patch.set_edgecolor('b')
                        else: # no line artist is associated to the cell
                            self.cellbrowser.hdcursor.cur_cell_ab.patch.set_edgecolor('k') # set to black
                    self.cellbrowser.hdcursor.cur_cell_ab = cur_artist
                    cur_artist.patch.set_edgecolor('r')

                    self.cellbrowser.hdcursor.cur_celluuid = celluuid
                    line_artist = self.getHighlightingArtist(celluuid)
                    self.cur_line_artist = line_artist # memorize
                    if celluuid not in self.cellbrowser.hdcursor.cur_line_uuids:
                        if line_artist:
                            print 'onClick celluuid is a line artist'
                            self.cellbrowser.hdcursor.hideHighlights()
                            self.cellbrowser.hdcursor.showHighlight(line_artist)
                            self.cellbrowser.hdcursor.cur_line_uuids = self.cellbrowser.lineLUT[line_artist]
                            ###plt.draw()
                            event.canvas.draw()
                    #else:
                    self.cellbrowser.hdcursor.drawCellImgOverview(celluuid, self.cellbrowser.ax4)

        if event.button == 1:

            self.clearMarker()
            for i in range(self.nSubPlots):
                subPlot = self.selectSubPlot(i)
                marker = plt.axvline(event.xdata, 0, 1, linestyle='--', \
                    linewidth=2, color='gray')
                self.markers.append(marker)

            event.canvas.draw()
            self.retVal['subPlot'] = subPlotNr
            self.retVal['x'] = event.xdata
            self.retVal['y'] = event.ydata

        else:
            # Start a dragFrom
            self.dragFrom = event.xdata
        return True

    def getHighlightingArtist(self, celluuid):
        line_artist = None
        for cur_artist in self.cellbrowser.hdcursor.lineLUT.keys():
            if celluuid in self.cellbrowser.hdcursor.lineLUT[cur_artist]:
                line_artist = cur_artist
                break
        return line_artist



    def onKey(self, event):

        """
        Handle a keypress event. The plot is closed without return value on
        enter. Other keys are used to add a comment.

        Arguments:
        event -- a KeyEvent
        """

        if event.key == 'enter':
            plt.close()
            return

        if event.key == 'escape':
            self.clearMarker()
            return

        if event.key == 'backspace':
            self.comment = self.comment[:-1]
        elif len(event.key) == 1:
            self.comment += event.key
        #self.supTitle.set_text("comment: %s" % self.comment)
        event.canvas.draw()

    def onRelease(self, event):

        """
        Handles a mouse release, which causes a move

        Arguments:
        event -- a mouse event
        """

        if self.dragFrom == None or event.button != 3:
            return
        dragTo = event.xdata
        dx = self.dragFrom - dragTo
        for i in range(self.nSubPlots):
            subPlot = self.selectSubPlot(i)
            xmin, xmax = subPlot.get_xlim()
            xmin += dx
            xmax += dx
            subPlot.set_xlim(xmin, xmax)
        ###event.canvas.draw()
        event.canvas.draw()
        return True

    def onScroll(self, event):

        """
        Process scroll events. All subplots are scrolled simultaneously

        Arguments:
        event -- a MouseEvent
        """

        for i in range(self.nSubPlots):
            subPlot = self.selectSubPlot(i)
            xmin, xmax = subPlot.get_xlim()
            dx = xmax - xmin
            cx = (xmax+xmin)/2
            if event.button == 'down':
                dx *= 1.1
            else:
                dx /= 1.1
            _xmin = cx - dx/2
            _xmax = cx + dx/2
            subPlot.set_xlim(_xmin, _xmax)
        event.canvas.draw()

 
class CellLineDataCursor(HighlightingDataCursor):
    def __init__(self, mpl_ax, celllineLUT, all_linesLUT, cur_line_uuids, line_cursor, *args, **kwargs):
        super(CellLineDataCursor, self).__init__(*args, **kwargs)
        #HighlightingDataCursor(self, *args, **kwargs)
        self.highlighted_artist = None
        self.mpl_ax = mpl_ax
        self.cellline_lineLUT = celllineLUT
        self.all_linesLUT = all_linesLUT
        self.cur_line_uuids = cur_line_uuids
        self.line_cursor = line_cursor
        self.baseimg_cache = {}
        self.cellimg_cache = {}

    def update(self, event, annotation):
        #self.highlighted_artist = event.artist
        #super(CellLineDataCursor, self).update(event, annotation)
        print '2: hello'
        print 'mouseevent: ' + str(event.mouseevent)
        self.cellline_event = event
        if event.mouseevent == 'button_press_event':
            celluuid = self.cellline_lineLUT[event.artist]
            self.cur_celluuid = celluuid
            if celluuid not in self.cur_line_uuids:
                line_artist = self.getHighlightingArtist(celluuid)
                print 'trying out uuid: ' + str(celluuid)
                if line_artist:
                    self.line_cursor.hideHighlights()
                    self.line_cursor.showHighlight(line_artist)
                    self.cur_line_uuids = self.all_linesLUT[line_artist]
                    ###plt.draw()
                    event.canvas.draw()
                else:
                    print 'no line artist found under the uuid'

        #lineage_img = self.getCellLineageImage(celluuid_list)
        #self.mpl_ax.imshow(lineage_img, cmap='gray')
        #self.drawCellLineGraph(celluuid_list, 4, self.cellline_ax)

    def getHighlightingArtist(self, celluuid):
        line_artist = None
        for cur_artist in self.all_linesLUT.keys():
            if celluuid in self.all_linesLUT[cur_artist]:
                line_artist = cur_artist
                break
        return line_artist



class SingleCellStatBrowser():
    mpl_fig_ref = None
    mpl_widgets = []
    class MyHighlightingDataCursor(HighlightingDataCursor):
        def __init__(self, cellbrowser, mpl_ax, cellline_ax, lineLUT, *args, **kwargs):
            super(SingleCellStatBrowser.MyHighlightingDataCursor, self).__init__(*args, **kwargs)
            #HighlightingDataCursor(self, *args, **kwargs)
            self.highlighted_artist = None
            self.cellbrowser = cellbrowser
            self.mpl_ax = mpl_ax
            self.cellline_ax = cellline_ax
            self.lineLUT = lineLUT
            self.baseimg_cache = {}
            self.cellimg_cache = {}
            self.cur_overview_img = None
            self.cur_lineage_img = None

            self.cur_line_uuids = None
            self.cur_celluuid = None
            self.cur_cell_ab = None

        def hideHighlights(self):
            for artist in self.highlights.values():
                if self.display == 'multiple':
                    continue
                artist.set_visible(False)

        def showHighlight(self, artist):
            print 'showHighlight'
            self.show_highlight(artist) #HighlightingDataCursor method
            celluuid_list = self.lineLUT[artist]
            self.drawCellLineageImage(celluuid_list, self.mpl_ax)
            self.drawCellLineGraph(celluuid_list, 4, self.cellline_ax)
            ###plt.draw()
            self.cellbrowser.mpl_fig_ref.canvas.draw()

        def updateLineHandles(self, *args, **kwargs):
            super(SingleCellStatBrowser.MyHighlightingDataCursor, self).__init__(*args, **kwargs)

        def update(self, event, annotation):
            print '1: hello'
            if self.highlighted_artist != event.artist:
                self.highlighted_artist = event.artist
                ###super(SingleCellStatBrowser.MyHighlightingDataCursor, self).update(event, annotation)
                self.hide() # hide text annotation
                celluuid_list = self.lineLUT[event.artist]
                self.drawCellLineageImage(celluuid_list, self.mpl_ax)
                self.drawCellLineGraph(celluuid_list, 4, self.cellline_ax)
                ###plt.draw()
                self.cellbrowser.mpl_fig_ref.canvas.draw()

        def drawCellLineageImage(self, celluuid_list, ax):
            lineage_img = self.getCellLineageImage(celluuid_list)
            self.cur_lineage_img = lineage_img
            if self.cellbrowser.isLineageView():
                ax.cla() # clear any previous drawings
                ax.imshow(lineage_img, cmap='gray')

        def drawCellImgOverview(self, celluuid, ax):
            overview_img = self.getCachedOverview(celluuid)
            self.cur_overview_img = overview_img
            if self.cellbrowser.isRawImgView():
                ax.cla() # clear any previous drawings
                #ax.imshow(overview_img, cmap='PuBuGn')
                ax.imshow(overview_img, cmap='gray')

        def showOverviewImage(self):
            if self.cellbrowser.isRawImgView():
                self.mpl_ax.cla()
                if self.cur_overview_img is not None:
                    #self.mpl_ax.imshow(self.cur_overview_img, cmap='PuBuGn')
                    self.mpl_ax.imshow(self.cur_overview_img, cmap='gray')

        def showLineageImage(self):
            if self.cellbrowser.isLineageView():
                self.mpl_ax.cla()
                if self.cur_lineage_img is not None:
                    self.mpl_ax.imshow(self.cur_lineage_img, cmap='gray')

        def showStats(self):
            if self.cellbrowser.isStatsView():
                self.mpl_ax.cla()
                # show stats

        def drawCellLineGraph(self, celluuid_list, weightlimit, ax):
            # get a list of list of cuuid
            print '1 here once'
            mothercelluuid = celluuid_list[0]
            cnode = celery_client_task.getCNodeByUUID(mothercelluuid)

            master_cnode_list = [cnode]
            master_cuuid_list = []
            self.cellline_artists = []
            self.cellline_artistLUT = {}

            cellline_pdg = pydot.Dot(graph_type='digraph')
            cnodeimg = self.getCachedCellImage(cnode['uuid'])
            width_str = str(cnodeimg.shape[1])
            height_str = str(cnodeimg.shape[0])
            first_pdg_node = pydot.Node(cnode['uuid'], shape='box', width=width_str, height=height_str)
            cellline_pdg.add_node(first_pdg_node)

            # build a networkx graph from a bulb graph representing a tinkergraph
            while len(master_cnode_list) > 0:
                cur_cnode = master_cnode_list.pop()
                print cur_cnode
                master_cuuid_list.append(cur_cnode['uuid'])
                # cur_edge_list = cur_cnode.outE('at_t1_temporally_corresponds_at_t2')
                # if cur_edge_list:
                #     sub_cnode_list = [(e.inV(), e.weight) for e in cur_edge_list if e.weight < weightlimit]
                # else:
                #     sub_cnode_list = []
                sub_cnode_list = celery_client_task.getSubCNodeList(cur_cnode['uuid'], weightlimit)
                master_cnode_list = master_cnode_list + [cnode for cnode, weight in sub_cnode_list]

                #cnodeimg = self.getCachedCellImage(cur_cnode.uuid)
                #width_str = str(cnodeimg.shape[1])
                #height_str = str(cnodeimg.shape[0])
                cur_pdg_node = cellline_pdg.get_node('"'+str(cur_cnode['uuid'])+'"') #pydot.Node(cur_cnode.uuid, shape='box', width=width_str, height=height_str)
                if cur_pdg_node == []:
                    print 'no no no'+str(cur_cnode['uuid'])
                else:
                    print cur_pdg_node
                    cnodeimg = self.getCachedCellImage(cur_cnode['uuid'])
                    width_str = str(cnodeimg.shape[1])
                    height_str = str(cnodeimg.shape[0])
                    cur_pdg_node = pydot.Node(cur_cnode['uuid'], shape='box', width=width_str, height=height_str)
                    cellline_pdg.add_node(cur_pdg_node)

                for cur_subcnode, cur_weight in sub_cnode_list:
                    subcnodeimg = self.getCachedCellImage(cur_subcnode['uuid'])
                    width_str = str(subcnodeimg.shape[1])
                    height_str = str(subcnodeimg.shape[0])
                    cur_pdg_subnode = pydot.Node(cur_subcnode['uuid'], shape='box', width=width_str, height=height_str)
                    cellline_pdg.add_node(cur_pdg_subnode)
                    pdg_edge = pydot.Edge(cur_pdg_node, cur_pdg_subnode, weight=str(cur_weight))
                    cellline_pdg.add_edge(pdg_edge)
                    #cellline_pdg.add_node(cur_subcnode.uuid)
                    #cellline_pdg.add_edge(cur_cnode.uuid, cur_subcnode.uuid, weight=cur_weight)

            #cellline_graph = nx.DiGraph()
            #cellline_pdg.write_png('pdg_test.png')
            # we do the pydot graph first to use its box shape with varying heights,
            # a feature lacking in networkx graphs, then do the conversion to nx graph
            # for any downstream processing.
            cellline_graph = nx.from_pydot(cellline_pdg)

            #pos = nx.graphviz_layout(cellline_graph, prog='dot')
            pos = celery_client_task.nxGetGraphvizDotLayout(cellline_graph)
            print pos
            ax.cla() # clear any previous drawings
            edges, edge_weights = zip(*nx.get_edge_attributes(cellline_graph, 'weight').items())
            edge_weight_vals = [round(float(w), 2) for w in edge_weights]
            edge_cmap = plt.cm.Blues #plt.get_cmap('jet')
            #edge_weight_normalized = colors.Normalize(vmin=0, vmax=max(edge_weight_vals))
            #scalar_map = mpl.cm.ScalarMappable(norm=edge_weight_normalized, cmap=edge_cmap)
            edges = nx.draw_networkx_edges(cellline_graph, pos, width=2, edge_color=edge_weight_vals, edge_cmap=edge_cmap, arrows=False, with_labels=False, ax=ax) # draw edges using networkx
            for cuuid in master_cuuid_list:
                cnodeimg = self.getCachedCellImage(cuuid)
                # cnodeimg_eq = cv2.equalizeHist(cnodeimg)
                # rgb_img = cv2.cvtColor(cnodeimg_eq, cv2.COLOR_GRAY2RGB)
                rgb_img = celery_client_task.cv2EqualizeHistAndCvtGrayToRGB(cnodeimg)

                imgbox = OffsetImage(rgb_img, zoom=0.5, cmap='PuBuGn')
                pos_tuple = (str(int(pos[cuuid][0])),str(int(pos[cuuid][1])))
                ab = AnnotationBbox(imgbox, pos[cuuid], xybox=pos[cuuid])
                if cuuid in celluuid_list:
                    if cuuid == self.cur_celluuid:
                        ab.patch.set_edgecolor('r') # highlight the currently chosen cell in red
                        self.cur_cell_ab = ab
                    else:
                        ab.patch.set_edgecolor('b') # highlight the currently chosen cell lineage in blue

                self.cellline_artists.append(ab)
                self.cellline_artistLUT[ab] = cuuid
                ax.add_artist(ab)

            self.cur_line_uuids = celluuid_list

            ylim_tuple = ax.get_ylim()
            #self.cellbrowser.ax_cellline_upperlimit = ylim_tuple[1]
            self.cellbrowser.ax_cellline_limitrange = ylim_tuple[1] - ylim_tuple[0]
            self.cellbrowser.ax_cellline_upperlimit = ylim_tuple[1]
            self.cellbrowser.celllineYScrollUpdate(self.cellbrowser.cellline_yscroll.val)
            self.cellbrowser.celllineYZoomUpdate(self.cellbrowser.cellline_yzoom.val)
            #self.cellbrowser.cellline_yscroll.valmax = ylim_tuple[1]
            #self.cellbrowser.cellline_yscroll.valmin = ylim_tuple[0]
            #self.cellbrowser.cellline_yscroll.valinit = ylim_tuple[1]
            #self.cellbrowser.cellline_yscroll.set_val(ylim_tuple[1])
            #self.cellbrowser.celllineYScrollUpdate(ylim_tuple[1])

            # create new cursors for the selected cellline
            #self.cellline_cursor = CellLineDataCursor(self.mpl_ax, self.cellline_artistLUT, self.lineLUT, celluuid_list, self, self.cellline_artists)
            #self.cellbrowser.fig.colorbar(ax=ax, mappable=edges)
            #self.cellbrowser.ax2_colorbor.update_normal(edges)
            ####self.cellbrowser.ax2_colorbar = plt.colorbar(cax=self.cellbrowser.ax2_colorbar_axe, mappable=edges)

        def getCachedOverview(self, cuuid):
            baseimgnode = celery_client_task.getImageNodeByBlobUUID(cuuid)
            if baseimgnode.uuid in self.baseimg_cache.keys():
                cur_baseimg = self.baseimg_cache[baseimgnode['uuid']] # get cv2 img handle from the cache
            else:
                # cur_baseimg = cv2.imread(baseimgnode['filepath'], cv2.IMREAD_GRAYSCALE)

                cur_baseimg = celery_client_task.cv2ImReadGrayscale(baseimgnode['filepath'])
                self.baseimg_cache[baseimgnode['uuid']] = cur_baseimg # cache the baseimg

            cellnode = celery_client_task.getCNodeByUUID(cuuid)
            #cellpolygon = Polygon(json.loads(cellnode['roi']))
            cellpolygon = shapy.Polygon(json.loads(cellnode['roi']))
            #cellpolygon = Polygon(cellnode.roi)
            cellbounds = cellpolygon.bounds
            # curimg_eq = cv2.equalizeHist(cur_baseimg)
            # overview_img = cv2.cvtColor(curimg_eq, cv2.COLOR_GRAY2RGB)
            #cv2.rectangle(overview_img, (int(cellbounds[0]), int(cellbounds[1])), (int(cellbounds[2]), int(cellbounds[3])), (255, 0, 0), 3) # (x1,y1), (x2,y2)
            crop_pt1 = (int(cellbounds[0]), int(cellbounds[1]))
            crop_pt2 = (int(cellbounds[2]), int(cellbounds[3]))
            crop_frame_color = (255, 0, 0)
            crop_frame_thickness = 3
            overview_img = celery_client_task.cv2CropImg(cur_baseimg, crop_pt1, crop_pt2, crop_frame_color, crop_frame_thickness)
            return overview_img

        def getCachedCellImage(self, cuuid):
            logger.debug('finding cell image of cuuid: '+str(cuuid))
            if cuuid in self.cellimg_cache.keys():
                cellimg = self.cellimg_cache[cuuid]
            else:
                baseimgnode = celery_client_task.getImageNodeByBlobUUID(cuuid)
                if baseimgnode['uuid'] in self.baseimg_cache.keys():
                    cur_baseimg = self.baseimg_cache[baseimgnode['uuid']] # get cv2 img handle from the cache
                else:
                    #cur_baseimg = cv2.imread(baseimgnode['filepath'], cv2.IMREAD_GRAYSCALE)
                    cur_baseimg = celery_client_task.cv2ImReadGrayscale(baseimgnode['filepath'])

                    self.baseimg_cache[baseimgnode['uuid']] = cur_baseimg # cache the baseimg

                cellnode = celery_client_task.getCNodeByUUID(cuuid)
                #cellpolygon = Polygon(json.loads(cellnode['roi']))
                cellpolygon = shapy.Polygon(json.loads(cellnode['roi']))
                #cellpolygon = Polygon(cellnode.roi)
                cellbounds = cellpolygon.bounds
                cellimg = cur_baseimg[cellbounds[1]:cellbounds[3], cellbounds[0]:cellbounds[2]] # take the roi by y1:y2, x1:x2
                self.cellimg_cache[cuuid] = cellimg # cache the cell image
            return cellimg

        def getCellLineageImage(self, celluuid_list):
            cellimg_list = []
            # obtain a list of cell images
            for cuuid in celluuid_list:
                cellimg = self.getCachedCellImage(cuuid)
                cellimg_list.append(cellimg)

            # tile the cell images horizontally
            shape_list = [c.shape for c in cellimg_list] # get a list of cellimg (height, width) tuples
            height_list = zip(*shape_list)[0]
            height_max = max(height_list) # get the max of heights
            height_diff_list = [height_max - h for h in height_list] # get a list of height difference to height_max
            img_dtype = cellimg_list[0].dtype

            padded_imglist = []
            for cellidx, roi_shape in enumerate(shape_list):
                cellwidth = roi_shape[1]
                cellheight = roi_shape[0]
                height_diff = height_diff_list[cellidx]

                curimg = cellimg_list[cellidx]
                img_padding = np.zeros((height_diff, cellwidth), dtype=img_dtype)
                padded_img = np.concatenate((curimg, img_padding), axis=0) # vertically stack curimg and img_padding, the final img's height == height_max
                padded_imglist.append(padded_img)

            lineage_img = np.concatenate(padded_imglist, axis=1) # horizontally stack cellimgs with the same heights

            return lineage_img


    def __init__(self, exp_uuid, *args, **kwargs):
        #HighlightingDataCursor(self, *args, **kwargs)
        # 7.4 um x 7.4 um pixel size (QImaging Retiga-2000R)
        ccd_pix_width = 7.4
        ccd_pix_height = 7.4
        obj_mag = 100
        lens_mag = 1
        cmount_mag = 1
        binning = 1 # 1x1
        self.pixel_area = self.calcPixelAreaInSqMicrons(ccd_pix_width, ccd_pix_height, obj_mag, lens_mag, cmount_mag, binning)
        print 'Pixel size is ' + str(self.pixel_area) + ' square um'
        self.cell_chain_limit = 100
        self.highlighted_artist = None
        self.baseimg_cache = {}
        self.line_handles = []
        self.lineLUT = {}
        print 'IIIIIIIIIIIIIIIII initMPLFigure'
        self.mpl_fig_ref = self.initMPLFigure()
        print 'getting stats from db...'
        #gdb.g = gdb.loadconfig()
        self.getStatistics(exp_uuid, countlimit=0, weightlimit=4, len_threshold=1) # limit to 30 stats
        #stats_worker = threading.Thread(target=partial(self.getStatistics, countlimit=0, weightlimit=4, len_threshold=0))
        #stats_worker.start()
        print 'initializing hdcursorRRRRRRRRRRRRRRRRRR'
        self.hdcursor = SingleCellStatBrowser.MyHighlightingDataCursor(self, self.ax4, self.ax2, self.lineLUT, self.line_handles)
        print 'IIIIIIIIIIIIIIIIIIIInitialized hdcursor'

        self.cur_ax1_btn = None
        self.cur_ax4_btn = None

    def isLineageView(self):
        if self.cur_ax4_btn == self.btn_cur_lineage_img:
            return True
        else:
            return False

    def isRawImgView(self):
        if self.cur_ax4_btn == self.btn_cur_rawimg:
            return True
        else:
            return False

    def isStatsView(self):
        if self.cur_ax4_btn == self.btn_stats:
            return True
        else:
            return False

    def onClickedRawImageBtn(self, event):
        #drawCellImgOverview(celluuid, ax)
        #self.ax4.view_init(elev=30, azim=-60)
        if self.cur_ax4_btn != self.btn_cur_rawimg:
            if self.cur_ax4_btn:
                self.cur_ax4_btn.color = '0.6' # restore the color of the previously selected button
            self.cur_ax4_btn = self.btn_cur_rawimg # assign currently clicked btn as current
            self.cur_ax4_btn.color = '0.975' # clicked color
            self.hdcursor.showOverviewImage()
            ###plt.draw()
            #self.mpl_fig_ref.canvas.draw()
            event.canvas.draw()

    def onClickedLineageImageBtn(self, event):
        #drawCellImgOverview(celluuid, ax)
        #self.ax4.view_init(elev=30, azim=-60)
        if self.cur_ax4_btn != self.btn_cur_lineage_img:
            if self.cur_ax4_btn:
                self.cur_ax4_btn.color = '0.6' # restore the color of the previously selected button
            self.cur_ax4_btn = self.btn_cur_lineage_img # assign currently clicked btn as current
            self.cur_ax4_btn.color = '0.975' # clicked color
            self.hdcursor.showLineageImage()
            ###plt.draw()
            #self.mpl_fig_ref.canvas.draw()
            event.canvas.draw()

    def onClickedStatsBtn(self, event):
        #drawCellImgOverview(celluuid, ax)
        #self.ax4.view_init(elev=30, azim=-60)
        if self.cur_ax4_btn != self.btn_stats:
            if self.cur_ax4_btn:
                self.cur_ax4_btn.color = '0.6' # restore the color of the previously selected button
            self.cur_ax4_btn = self.btn_stats # assign currently clicked btn as current
            self.cur_ax4_btn.color = '0.975' # clicked color
            self.hdcursor.showStats()
            ###plt.draw()
            #self.mpl_fig_ref.canvas.draw()
            event.canvas.draw()

    def doOnClickedPerspective0(self):
        print 'CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC clicked ps0'
        #if self.cur_ax1_btn != self.ps0_btn:
        #    if self.cur_ax1_btn:
        #        self.cur_ax1_btn.color = '0.6' # restore the color of the previously selected button
        self.ax1.view_init(elev=30, azim=-60)
        #    self.cur_ax1_btn = self.ps0_btn  # assign currently clicked btn as current
        #    self.cur_ax1_btn.color = '0.975' # clicked color
            ###plt.draw()
            #self.ax1.draw()
        self.mpl_fig_ref.canvas.draw()
            #self.mpl_fig_ref.canvas.flush_events()

    def doOnClickedPerspective1(self):
        #if self.cur_ax1_btn != self.ps1_btn:
        #    if self.cur_ax1_btn:
        #        self.cur_ax1_btn.color = '0.6' # restore the color of the previously selected button
        self.ax1.view_init(elev=0, azim=-90)
        #    self.cur_ax1_btn = self.ps1_btn  # assign currently clicked btn as current
        #    self.cur_ax1_btn.color = '0.975' # clicked color
            ###plt.draw()
            #self.ax1.draw()
        self.mpl_fig_ref.canvas.draw()
            #self.mpl_fig_ref.canvas.flush_events()

    def onClickedPerspective0(self, event):
        print 'CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC clicked ps0 real ', len(self.lineLUT)
        #if self.cur_ax1_btn != self.ps0_btn:
        #    if self.cur_ax1_btn:
        #        self.cur_ax1_btn.color = '0.6' # restore the color of the previously selected button
        self.ax1.view_init(elev=30, azim=-60)
        #    self.cur_ax1_btn = self.ps0_btn  # assign currently clicked btn as current
        #    self.cur_ax1_btn.color = '0.975' # clicked color
            ###plt.draw()
            #self.ax1.draw()
            #self.mpl_fig_ref.canvas.draw()
            #self.mpl_fig_ref.canvas.flush_events()
        event.canvas.draw()
        return True

    def onClickedPerspective1(self, event):
        #if self.cur_ax1_btn != self.ps1_btn:
        #    if self.cur_ax1_btn:
        #        self.cur_ax1_btn.color = '0.6' # restore the color of the previously selected button
        self.ax1.view_init(elev=0, azim=-90)
        #    self.cur_ax1_btn = self.ps1_btn  # assign currently clicked btn as current
        #    self.cur_ax1_btn.color = '0.975' # clicked color
            ###plt.draw()
            #self.ax1.draw()
            #self.mpl_fig_ref.canvas.draw()
            #self.mpl_fig_ref.canvas.flush_events()
        event.canvas.draw()
        return True


    def onClickedPerspective2(self, event):
        #if self.cur_ax1_btn != self.ps2_btn:
        #    if self.cur_ax1_btn:
        #        self.cur_ax1_btn.color = '0.6' # restore the color of the previously selected button
        self.ax1.view_init(elev=90, azim=-90)
        #    self.cur_ax1_btn = self.ps2_btn  # assign currently clicked btn as current
        #    self.cur_ax1_btn.color = '0.975' # clicked color
            ###plt.draw()
            #self.ax1.draw()
            #self.mpl_fig_ref.canvas.draw()
            #self.mpl_fig_ref.canvas.flush_events()
        event.canvas.draw()
        return True

    def initMPLFigure(self):
        #self.fig = MPLFigure(figsize=(13,13)) #plt.figure(figsize=(13,13))
        #self.fig = plt.figure(figsize=(13,13))
        ###self.fig = plt.figure()
        #self.fig = MPLFigure()
        #FigureCanvas(self.fig) # assign an Agg canvas backend to the fig
        self.fig = plt.figure()
        gs = GridSpec(3,3)
        self.ax1 = self.fig.add_subplot(gs[:2,:2], projection='3d')
        self.ax2 = self.fig.add_subplot(gs[:3,2], autoscale_on=True)
        ####self.ax2_colorbar_axe = self.fig.add_subplot(gs[:3,3], autoscale_on=True)
        #ax2_z = ZoomPan()
        #ax2_z.zoom_factory(self.ax2)
        #ax2_z.pan_factory(self.ax2)
        self.ax2.tick_params(which='both',left='off',right='off',labelleft='off',bottom='off',top='off',labelbottom='off')
        ####self.ax2_colorbar_axe.tick_params(which='both',left='off',right='on', labelright='on', labelleft='off',bottom='off',top='off',labelbottom='off')
        #self.ax3 = self.fig.add_subplot(gs[1,2])
        self.ax4 = self.fig.add_subplot(gs[2,:2])
        self.ax4.tick_params(which='both',left='off',right='off',labelleft='off',bottom='off',top='off',labelbottom='off')
        #ax.plot([0,720, 1440,2160,2880],[2.55, 3.53,2.38,3.35,2.81], [16.02, 15.74, 16.41, 18.59,16.96])
        self.fig.suptitle('Temporal variations in the  single cells\' sizes and fluorescence levels') #, fontsize=20)

        self.ax1.set_xlabel('Time (sec)') #, fontsize=18)
        self.ax1.set_ylabel('Size (um^2)') #, fontsize=16)
        self.ax1.set_zlabel('Fluorescence (AU)') #, fontsize=16)


        # buttons for the main cell line perspectives
        #self.ax_ps0_btn = plt.axes([0.15, 0.9, 0.05, 0.02])
        #self.ax_ps0_btn = self.fig.add_axes([0.05, 0.1, 0.08, 0.04])
        self.ax_ps0_btn = self.fig.add_axes([0.02, 0.1, 0.10, 0.15])
        self.ax_ps0_btn.set_zorder(20) # put the button on top of the zorder
        self.ps0_btn = Button(self.ax_ps0_btn, 'ps0', color='0.6', hovercolor='0.975')
        self.ps0_btn.on_clicked(self.onClickedPerspective0)
        self.mpl_widgets.append(self.ps0_btn)
        print 'BBBBBBBBBBBBBBBUTTTTTTOOOOOOOOOOOOOOOOOOOOOOONNN ', self.ps0_btn, self.ax_ps0_btn.get_position()
        #self.ax_ps1_btn = plt.axes([0.23, 0.9, 0.05, 0.02])
        self.ax_ps1_btn = self.fig.add_axes([0.23, 0.9, 0.05, 0.02])
        self.ax_ps1_btn.set_zorder(20)
        self.ps1_btn = Button(self.ax_ps1_btn, 'ps1', color='0.6', hovercolor='0.975')
        self.ps1_btn.on_clicked(self.onClickedPerspective1)
        self.mpl_widgets.append(self.ps1_btn)
        #self.ax_ps2_btn = plt.axes([0.31, 0.9, 0.05, 0.02])
        self.ax_ps2_btn = self.fig.add_axes([0.31, 0.9, 0.05, 0.02])
        self.ax_ps2_btn.set_zorder(20)
        self.ps2_btn = Button(self.ax_ps2_btn, 'ps2', color='0.6', hovercolor='0.975')
        self.ps2_btn.on_clicked(self.onClickedPerspective2)
        self.mpl_widgets.append(self.ps2_btn)

        # buttons for images and stats
        #self.ax_cur_rawimg_btn = plt.axes([0.15, 0.05, 0.05, 0.02])
        self.ax_cur_rawimg_btn = self.fig.add_axes([0.15, 0.05, 0.05, 0.02])
        #self.ax_cur_lineage_img_btn = plt.axes([0.23, 0.05, 0.05, 0.02])
        self.ax_cur_lineage_img_btn = self.fig.add_axes([0.23, 0.05, 0.05, 0.02])
        #self.ax_stats_btn = plt.axes([0.31, 0.05, 0.05, 0.02])
        self.ax_stats_btn = self.fig.add_axes([0.31, 0.05, 0.05, 0.02])
        self.btn_cur_rawimg = Button(self.ax_cur_rawimg_btn, 'raw img', color='0.6', hovercolor='0.975')
        self.btn_cur_rawimg.on_clicked(self.onClickedRawImageBtn)
        self.btn_cur_lineage_img = Button(self.ax_cur_lineage_img_btn, 'lineage', color='0.6', hovercolor='0.975')
        self.btn_cur_lineage_img.on_clicked(self.onClickedLineageImageBtn)
        self.btn_stats = Button(self.ax_stats_btn, 'stats', color='0.6', hovercolor='0.975')
        self.btn_stats.on_clicked(self.onClickedStatsBtn)



        # scroll bars for the single cell line tree
        #self.ax_cellline_yscroll = plt.axes([0.7, 0.1, 0.2, 0.02])
        self.ax_cellline_yscroll = self.fig.add_axes([0.7, 0.1, 0.2, 0.02])
        #self.ax_cellline_yzoom = plt.axes([0.7, 0.15, 0.2, 0.02])
        self.ax_cellline_yzoom = self.fig.add_axes([0.7, 0.15, 0.2, 0.02])
        self.cellline_yscroll = Slider(self.ax_cellline_yscroll, 'yscroll', 0, 1.0, valinit=1, valfmt='%.2f')
        self.cellline_yzoom = Slider(self.ax_cellline_yzoom, 'yzoom', 0, 1.0, valinit=1, valfmt='%.2f')
        self.ax_cellline_upperlimit = 1.0
        self.ax_cellline_limitrange= 1.0
        self.cellline_yscroll.on_changed(self.celllineYScrollUpdate)
        self.cellline_yzoom.on_changed(self.celllineYZoomUpdate)
        self.cplot = ClickPlot(self, self.fig)
        self.cplot.draw()
        ####self.cplot.show()
        return self.fig

        #self.fig.show()

    def celllineYZoomUpdate(self, val):
        print 'update: ' + str(val)
        cur_upperlimit = np.floor(self.cellline_yscroll.val*self.ax_cellline_upperlimit)
        new_limitrange = np.floor(val*self.ax_cellline_limitrange)
        self.ax2.set_ylim(cur_upperlimit - new_limitrange, cur_upperlimit)
        self.ax2.relim()
        self.mpl_fig_ref.canvas.draw()
        #self.ax2.imshow()
        ###plt.draw()

    def celllineYScrollUpdate(self, val):
        print 'update: ' + str(val)
        new_upperlimit = np.floor(val*self.ax_cellline_upperlimit)
        cur_limitrange = np.floor(self.cellline_yzoom.val*self.ax_cellline_limitrange)
        self.ax2.set_ylim(new_upperlimit-cur_limitrange, new_upperlimit)
        self.ax2.relim()
        ###plt.draw()

    def getStatistics(self, exp_uuid, countlimit=0, weightlimit=5, len_threshold=1):
        #cells_list_t1 = gdb.getBlobsByExpImgIdx(0,0) # gotta make these values passed on through args !!!
        celluuid_list_t1 = celery_client_task.getBlobUUIDsByExpImgIdx(exp_uuid,0) # gotta make these values passed on through args !!!
        #cells_list_t1 = []

        #a_paired = []
        self.line_handles = []
        self.lineLUT = {} #self.hdcursor.lineLUT = {}
        statcount = 0
        for initcell_uuid in celluuid_list_t1:
            prevClusterGFP = -1
            prevClusterSize = -1
            cell_chains = celery_client_task.getTemporalCellChainsByInitCellUUIDs([initcell_uuid], self.cell_chain_limit)
            #clusterChain = stitchKeyClusterChain(initCluster, keyStepSize, clusterChainLimit)

            filtered_chains = self.filterChainsByWeight(cell_chains, weightlimit) # use the threshold edge weight value of 5

            #print len(cell_chains['vertices'])
            #print len(filtered_chains)
            for a_chain in filtered_chains: #cell_chains['vertices']:
                #sys.stdout.write('.')
                new_linehandles = self.processACellChain(a_chain, self.ax1, self.lineLUT, len_threshold)
                self.line_handles = self.line_handles + new_linehandles
                statcount += len(new_linehandles)

            #self.hdcursor.updateLineHandles(self.line_handles)
            ###plt.draw() # this updates the figure
            self.mpl_fig_ref.canvas.draw()
            #print 'stat#:' + str(statcount)
            if countlimit != 0 and statcount > countlimit:
                break # stop gathering stats
            time.sleep(0.1)
            #plt.pause(0.0001) # enabling this pops up a plot window

    def calcPixelAreaInSqMicrons(self, ccd_pix_width, ccd_pix_height, obj_mag, lens_mag, cmount_mag, binning):
        pixel_width = (float(ccd_pix_width) * binning)/(obj_mag * lens_mag * cmount_mag)
        if ccd_pix_width == ccd_pix_height:
            pixel_height = pixel_width
        else:
            pixel_height = (float(ccd_pix_height) * binning)/(obj_mag * lens_mag * cmount_mag)

        pixel_area = pixel_width * pixel_height

        return pixel_area


    def processACellChain(self, a_cell_chain, plot_handle, lineLUT, len_threshold=1):
        chain_len = len(a_cell_chain)
        line_objs = []
        if (chain_len <= self.cell_chain_limit + 1):
            #print a_cell_chain
            #t1Cluster = clusterChain[0]
            #t2Cluster = clusterChain[chainLen - 1]
            #a_paired.append((t1Cluster, t2Cluster))

            time_idx = 0
            plot_data = []
            for cur_cell in a_cell_chain:
                ##cellstats = gdb.getCellStatsByUUID(cur_cell['uuid'])
                cellstats = celery_client_task.getCellStatsByUUID(cur_cell['uuid'])
                #print cur_cell['uuid']
                #print cur_cell['timestamp']
                #print cellstats
                #print cur_cell
                #print cellstats

                if len(cellstats) > 0:
                    avg_fluo = 0
                    tot_size = 0
                    count = 0
                    stats_data = pickle.loads(cellstats[0]['stats'])
                    #print stats_data
                    mean_GFP = float(stats_data['mean'])
                    cell_size = float(stats_data['area'])
                    cell_size_in_sqrmicrons = cell_size * self.pixel_area
                    timestamp = int(cur_cell['timestamp'])

                    plot_data.append((timestamp, cell_size_in_sqrmicrons, mean_GFP))

            if len(plot_data) > len_threshold: # only pick plot_data that would form a line (ie. greater than or equal to two plot points
                plot_data_zipped = zip(*plot_data)
                time_dimension = list(plot_data_zipped[0])
                cellsize_dimension = list(plot_data_zipped[1])
                gfp_dimension = list(plot_data_zipped[2])

                line, = plot_handle.plot(time_dimension, cellsize_dimension, gfp_dimension)
                line_objs.append(line)
                lineLUT[line] = [c['uuid'] for c in a_cell_chain]

                    #csv_writer.writerow([str(a_cell_chain[chain_len-1]['neo4jId']), str(timestamp), "%.2f" % mean_GFP, "%.2f" % cell_size_in_sqrmicrons])
    #			if (prev_cell_GFP > 0 and prevClusterSize > 0):
    #				fluoPercentDiff = (avgFluo - prevClusterGFP)*100/prevClusterGFP
    #				sizePercentDiff = (totSize - prevClusterSize)*100/prevClusterSize
    #				csvWriter2.writerow([str(initCluster['neo4jId']), str(timeIdx*keyInterval), "%.2f" % fluoPercentDiff, "%.2f" % sizePercentDiff])
    #			prevClusterGFP = avgFluo
    #			prevClusterSize = totSize
                #print '-----'

            time_idx += 1
    #	else:
            #print 'ouch!'
        return line_objs
    # end def

    def filterChainsByWeight(self, cell_chains, weight_threshold=10):
        filtered_chains = []
        vertices = cell_chains['vertices']
        edges = cell_chains['edges']

        for chain_idx, an_edge_list in enumerate(edges):
            cur_start_idx = 0
            cur_end_idx = 1
            edge_len = len(an_edge_list)
            for edge_idx, an_edge in enumerate(an_edge_list):
                if an_edge['weight'] > weight_threshold:
                    if cur_start_idx != cur_end_idx - 1:
                        filtered_chains.append(vertices[chain_idx][cur_start_idx:cur_end_idx])
                        # added a cell chain ending before the non-qualifying edge,
                        # so the next cell chain search would begin at the last unqualified vertex
                        cur_start_idx = cur_end_idx
                        cur_end_idx = cur_start_idx + 1
                    else:
                        # cur_start_idx == cur_end_idx - 1, this means only a single vertex is in a chain,
                        # need to skip this chain without adding to the list
                        cur_start_idx = cur_end_idx
                        cur_end_idx = cur_start_idx + 1
                else:
                    # the current edge was a qualifying one, so continue with extending the chain to the next index
                    if edge_idx < edge_len - 1:
                        cur_end_idx += 1
                    else: # unless the current edge was last in the list
                        filtered_chains.append(vertices[chain_idx][cur_start_idx:cur_end_idx+1])
        return filtered_chains
    # end of def



# script begins...

#keyStepSize = 3
#time_idx_count = 10


###cellbrowser =  SingleCellStatBrowser()
#fig.savefig('test.jpg')

##raw_input("Please press enter to continue...")

##print 'end of script'
