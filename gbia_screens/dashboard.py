__author__='spark'

from kivy.lang import Builder
from kivy.properties import StringProperty

from gbia_screens.manager import WFScreen

from gbimageanalyzer import celery_client_task

from kivy.clock import Clock
from threading import Event as ThreadEvent
from functools import partial

Builder.load_string('''
<GBIADashboardScreen>:
    loaddata_btn: _loaddata_btn
    clear_list: []
    canvas.before:
        Color:
            rgba: 0,0,0,0.0
        Rectangle:
            pos: self.pos
            size: self.size
    GridLayout:
        cols: 1
        pos: root.pos
        size: root.size
        FloatLayout:
            size_hint: 1.0, 0.4
            Image:
                source: 'grabia_icon.png'
                size_hint: 1.0, 1.0
                pos_hint: {'x': -.3, 'y': .0}
            Label:
                size_hint: .5, .1
                pos_hint: {'x': .5, 'y': .05}
                font_size: '48sp'
                text: "[b]dashboard[/b]"
                markup: True
        FloatLayout:
            size_hint: 1.0, 0.6
            Label:
                pos_hint: {'y': .05}
                size_hint: 1, .1
                text: "You may [color=ff3333][b]visualise data[/b][/color] from previous analyses or perform a new [color=ff3333][b]image analysis[/b][/color]."
                markup: True
            Button:
                text: 'Image analysis'
                pos_hint: {'x': .525, 'y': .5}
                size_hint: .2, .2
                #on_release: app.wfm.getWorkflow('dashboard_wf').lookupTaskMemory('tid_run_next_wf').executeThreadTask() # app.wfm.executeWorkflow('imagechooser_wf')
                on_release: app.wfm.executeWorkflow('imagechooser_wf')
            Button:
                text: 'Visualise data'
                id: _loaddata_btn
                pos_hint: {'x': .275, 'y': .5}
                size_hint: .2, .2
                on_release: app.wfm.getWorkflow('dashboard_wf').lookupTaskMemory('tid_load_gdbchooser_scr').executeTask()
                #on_release: app.wfm.getWorkflow('dashboard_wf').lookupTaskMemory('tid_run_stats_wf').executeTask()

''')

# this screen sets the following events
# btn1, btn2
class GBIADashboardScreen(WFScreen):
    source = StringProperty()
    #is_finished = ThreadEvent()
    #kwargs = {}
    is_gdb_file = False

    def __init__(self, **kwargs):
        #self.kwargs = kwargs
        #self.is_finished = ThreadEvent() #is_finished
        print 'GBIAINITSCREENNNNNNNNNNNNNNNNNNNNNNN 1', kwargs

        #Clock.schedule_once(partial(self._do_init, **kwargs)) #, kwargs))

        #print 'GBIAINITSCREENNNNNNNNNNNNNNNNNNNNNNN 2', kwargs

#    def _do_init(self, *largs, **kwargs): #, is_finished, **kwargs):
        #super(GBIAInitScreen, self).__init__(scm_ref, ['btn1', 'btn2'], **self.kwargs)
        super(GBIADashboardScreen, self).__init__(**kwargs)
        #import pdb; pdb.set_trace()
        # if self.is_finished:
        #     self.is_finished.set()
        print '>>>>>>>>>>>>>>>>>>>> GBIAInitScreen initialized'
        #if self.is_finished:
        #    self.is_finished.set()
        #pass

    # def enableNextBtn(self):
    #     self.next_btn.disabled = False
    # def updateBtnStates(self):
    #     Clock.schedule_once(self._doUpdateBtnStates)
    #
    # def _doUpdateBtnStates(self, *largs):
    #     if self.isGDBFile():
    #         self.loaddata_btn.disabled = False
    #     else:
    #         self.loaddata_btn.disabled = True

    def isGDBFile(self):
        gdb_files = celery_client_task.getGDBFiles()
        if gdb_files:
            return True
        else:
            return False

    def updateBtnStates(self):
        if self.isGDBFile():
            self.loaddata_btn.disabled = False
        else:
            self.loaddata_btn.disabled = True

    def checkDependencies(self):
        print 'executed checkDependencies'
        pass

    def isDependencyComplete(self):
        print 'executed isDependencyComplete'
        pass

    def setupBackend(self):
        print 'executed setupBackend'
        pass

    def isSetupComplete(self):
        print 'executed isSetupComplete'
        pass

    def doReturn(self):
        print 'executed doReturn'
        pass