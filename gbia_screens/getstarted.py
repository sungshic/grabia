__author__='spark'

from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty
from gbia_screens.manager import WFScreen

Builder.load_string('''
<GetStartedScreen>:
	background: background
	AnchorLayout:
		on_touch_down: self.is_dependency_complete = True # app.wfm.executeTask(), app.wfm.getWorkflow('wf_name').executeTask()
		Scatter:
			id: background
			do_rotation: False
			do_translation: False
			do_scale: False
			Image:
				source: root.source
				width: root.width
				height: root.height
''')

class GetStartedScreen(WFScreen):
	source = StringProperty()
	is_dependency_complete = ObjectProperty(None)

#	def __init__(self):
#		pass

	def checkDependencies(self):
		print 'executed checkDependencies'
		pass

	def isDependencyComplete(self):
		print 'isDependencyComplete?'
		return self.is_dependency_complete

	def setupBackend(self):
		print 'executed setupBackend'
		pass

	def isSetupComplete(self):
		print 'executed isSetupComplete'
		pass

	def doReturn(self):
		print 'executed doReturn'
		pass