__author__='spark'

from kivy.lang import Builder
from kivy.properties import StringProperty

from gbia_screens.manager import WFScreen
from kivy.clock import Clock
from threading import Event as ThreadEvent
from threading import Thread
from functools import partial
from kivy.uix.scrollview import ScrollView

from time import sleep


Builder.load_string('''
<ScrollableLabel>:
    Label:
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text
<GBIAInitScreen>:
#    background: background
    clear_list: []
#    next_btn: _dummy_btn
    progress_text: _progress_text
    canvas:
        Rectangle:
            pos: self.pos
            size: self.size
        Color:
            rgba: 1,0,0,1.0
    Image:
        size_hint_x: 1.0
        source: 'grabia_splash.png'
    FloatLayout:
        pos: root.pos
        size: root.size
        ScrollableLabel:
            id: _progress_text
            size_hint: 1.0, 0.2
            pos_hint: {'x': .1, 'y': .2}
            color: [1,1,0,1]
        Label:
            size_hint: .8, 0.1
            pos_hint: {'x': .1, 'y': .8}
            text: "Initializing GraBIA..."
        Label:
            size_hint: .8, 0.1
            pos_hint: {'x': .1, 'y': .6}
            text: "[color=ff3333][b]G[/color]ra[color=ff3333][b]BIA[/color]"
            markup: True

''')

class ScrollableLabel(ScrollView):
    text = StringProperty('')

# this screen sets the following events
# btn1, btn2
class GBIAInitScreen(WFScreen):
    source = StringProperty()
    backend_m = None
    #kwargs = {}

    def __init__(self, **kwargs):
        #self.kwargs = kwargs
        #self.is_finished = ThreadEvent() #is_finished
        print 'GBIAINITSCREENNNNNNNNNNNNNNNNNNNNNNN 1', kwargs

#        Clock.schedule_once(partial(self._do_init, **kwargs)) #, kwargs))

        print 'GBIAINITSCREENNNNNNNNNNNNNNNNNNNNNNN 2', kwargs

#    def _do_init(self, *largs, **kwargs): #, is_finished, **kwargs):
        #super(GBIAInitScreen, self).__init__(scm_ref, ['btn1', 'btn2'], **self.kwargs)
        print '>>>>>>>>>>>>>>>>>>>> GBIAInitScreen _do_init kwargs: ', kwargs
        super(GBIAInitScreen, self).__init__(**kwargs)
        # if self.is_finished:
        #     self.is_finished.set()
        if kwargs.has_key('backend_m'):
            self.backend_m = kwargs.pop('backend_m')

        if kwargs.has_key('backend_init_func'):
            backend_init_func = kwargs.pop('backend_init_func')
            self.initBackend(backend_init_func) # call the function
        print '>>>>>>>>>>>>>>>>>>>> GBIAInitScreen initialized'
        #pass
    def initBackend(self, init_func):
        backend_init_func_thread = Thread(target=init_func)
        backend_init_func_thread.start()

    def updateProgressText(self, p_handle):
        if p_handle:
            for line in iter(p_handle.stdout.readline, b''):
                cur_text = self.progress_text.text
                self.progress_text.text = cur_text + line #.rstrip()
                self.progress_text.scroll_y = 0 # auto scroll

    def updateProgressTextByString(self, progress_str):
        cur_text = self.progress_text.text
        self.progress_text.text = cur_text + progress_str #.rstrip()
        self.progress_text.scroll_y = 0 # auto scroll

    def doInitializeBackend(self):
        # create a docker machine if not available
        #self.updateProgressTextByString('Checking for a docker machine...\n')
        #if not self.backend_m.isDockerMachineCreated(self.backend_m.machine_name):
        #    self.logger.debug('BBBBBBBBBBBB Creating a docker machine: ' + self.backend_m.machine_name)
        #    self.updateProgressTextByString('Creating a docker machine...\n')
        #    p_handle = self.backend_m.createDockerMachine(self.backend_m.machine_name)

        #   # print self.machine_name, ' created.'
        #    self.updateProgressTextByString('Waiting for a docker machine to boot up.\n')
        #    self.updateProgressText(p_handle)
            # poll until the docker machine is created
        #    while not self.backend_m.isDockerMachineRunning(self.backend_m.machine_name):
        #        if self.scm_ref.stop.is_set():
        #            return
        #        #self.logger.debug('BBBBBBBBBBBBBBBBB polling for the backend machine to run.')
        #        self.updateProgressText(p_handle)
        #        sleep(1)
        #    self.logger.debug(str(self.backend_m.machine_name) + ' running...')
        #    self.updateProgressTextByString('A docker machine is successfully booted up!\n')
        #elif not self.backend_m.isDockerMachineRunning(self.backend_m.machine_name):
        #    self.logger.debug('BBBBBBBBBBBBB Starting an existing docker machine: ' + self.backend_m.machine_name)
        #    self.updateProgressTextByString('A docker machine is already running!\n')
        #    self.updateProgressTextByString('Just restarting the machine to refresh resources...\n')
        #    p_handle = self.backend_m.restartDockerMachine(self.backend_m.machine_name)
        #    self.updateProgressText(p_handle)
        #    # poll until the docker machine is created
        #    while not self.backend_m.isDockerMachineRunning(self.backend_m.machine_name):
        #        if self.scm_ref.stop.is_set():
        #            return
        #        #self.logger.debug('BBBBBBBBBBBBBBBBB polling for the backend machine to run.')
        #        self.updateProgressText(p_handle)
        #        sleep(1)
        #    self.logger.debug(str(self.backend_m.machine_name) + ' running...')
        #    self.updateProgressTextByString('A docker machine is successfully restarted!\n')
        #    p_handle = self.backend_m.regenerateMachineCerts(self.backend_m.machine_name)
        #    self.updateProgressText(p_handle)
        #    # poll until the docker machine is created
        #    while not self.backend_m.isDockerMachineRunning(self.backend_m.machine_name):
        #        if self.scm_ref.stop.is_set():
        #            return
        #        #self.logger.debug('BBBBBBBBBBBBBBBBB polling for the backend machine to run.')
        #        self.updateProgressText(p_handle)
        #        sleep(1)
        #    # self.regenerateMachineCerts(self.machine_name) # regenerate cert again just in case things might have changed
        #    self.logger.debug(self.backend_m.machine_name + ' started.')

        # set up docker env
        #self.logger.debug('BBBBBBBBBBBBBBB setting up docker env for ' + self.backend_m.machine_name)
        #self.updateProgressTextByString('Setting up docker env for ' + str(self.backend_m.machine_name) + '...\n')
        #self.backend_m.setupDockerEnv(self.backend_m.machine_name)
        if self.backend_m.isBackendContainerRunning(self.backend_m.container_name):
            p_handle = self.backend_m.restartTheBackend()
            self.updateProgressTextByString('restarting the backend container.\n')
        else:
            p_handle = self.backend_m.startTheBackend()
            self.updateProgressTextByString('starting the backend container.\n')
        self.logger.debug('BBBBBBBBBBBBBBBB Backend container, ' + self.backend_m.container_name + ' started.')

        self.updateProgressText(p_handle)
        # poll until the container is running
        self.updateProgressTextByString('Downloading docker image from Docker Hub.\n')
        self.updateProgressTextByString('This may take a while depending on the network speed.\n')
        self.updateProgressTextByString('Please go get a coffee. :)\n')
        while not self.backend_m.isBackendContainerRunning(self.backend_m.container_name):
            if self.scm_ref.stop.is_set():
                return
            #self.logger.debug('BBBBBBBBBBBBBBBBB polling for the backend container to run.')
            self.updateProgressText(p_handle)
            sleep(1)
        self.updateProgressTextByString('The backend container is successfully running.\n')
        self.logger.debug('BBBBBBBBBBBBBBBBB Backend initialization completed.')
        self.updateProgressTextByString('Initialisation completed!\n')
        self.backend_m.is_initialized.set()

#    def enableNextBtn(self):
#        self.next_btn.disabled = False

    def getFinishedEvent(self):
        return self.is_finished

    def checkDependencies(self):
        print 'executed checkDependencies'
        pass

    def isDependencyComplete(self):
        print 'executed isDependencyComplete'
        pass

    def setupBackend(self):
        print 'executed setupBackend'
        pass

    def isSetupComplete(self):
        print 'executed isSetupComplete'
        pass

    def doReturn(self):
        print 'executed doReturn'
        pass
