__author__='spark'

import os
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.screenmanager import FadeTransition
from gbia_wf.wf_manager import WFTask

import time
import random
import threading
from threading import Event as ThreadEvent

from kivy.clock import Clock, mainthread
from functools import partial
import logging

class GBIAScreenManager(ScreenManager):
    stop = threading.Event()
    cur_screen_ref = None
    scm_events = {}
    screen_lut = {}
    logger = logging.getLogger('grabia_app.gbia_screens.manager.ScreenManager')

    def __init__(self):
        self.screen_lut = {}

        #rootPath = os.path.dirname(os.path.realpath(sys.argv[0]))
        #rootPath = '/Users/spark/Dropbox/microscopy/231015/sample__1/Pos0'

        #self.photos = []
        #for image in os.listdir(rootPath): # + '/Photos'):
        #    self.photos.append(rootPath + '/' + image)

        self.transitions = [
#			BlurTransition(duration=1.5),
#			PixelTransition(duration=1.5),
#			RippleTransition(duration=1.5),
#			RVBTransition(duration=2.0),
#			RotateTransition(direction='down')
            FadeTransition()
        ]

        # Create the screen manager
        r = 0 #random.randint(0, len(self.transitions) -1)
        #self.screenManager = ScreenManager(transition=self.transitions[r])

        super(GBIAScreenManager, self).__init__(transition=self.transitions[r])

        ###self.page1 = GBIAInitScreen(name='page1', source = self.photos[0])
        #self.page2 = GetStartedScreen(name='page2', source = self.photos[1])
        self.index = 0

        #self.add_widget(self.page1)
        ###self.loadNextScreen('page1', self.page1)
        #self.add_widget(self.page2)

        ###anim = Animation(
        ###	scale=self.page1.background.scale*1.3,
        ###	duration=15.0
        ###)
        ###anim.start(self.page1.background)

    #def initScreen(self, dt):
    #	self.loadNextScreen('page1', self.page1)
    def initScreen(self, screen_name, import_path, class_name, class_args={}):
        ##is_finished = self.addSCMEvent(screen_name)
        ##class_args['is_finished'] = is_finished
        #print 'IIIIIIIIINNNNNNNNNNNIIIIIITTTTTTTTSCREEN ', screen_name, import_path, class_name, class_args
        screen_ref = WFTask.instantiateClass(class_name, import_path, class_args)
        print 'IIIIIIIIINNNNNNNNNNNNIIIIIIII 2 XXXX', screen_ref.__class__, screen_ref
        self.screen_lut[screen_name] = screen_ref
        ##while True:
        ##    if is_finished.is_set() or self.stop.is_set():
        ##        break
        ##    print 'screen loading ... '
        ##    time.sleep(1)
        ##self.removeSCMEvent(screen_name)
        return screen_ref

    def getScreen(self, screen_name, import_path=None, class_name=None, class_args={}):
        screen_ref = self.screen_lut.get(screen_name)
        if screen_ref:
            return screen_ref
        else:
            return self.initScreen(screen_name, import_path, class_name, class_args)


    @mainthread
    def loadNextScreen(self, screen_name, screen_ref): #, screen_ref):
        Clock.schedule_once(partial(self._loadNextScreen, screen_name, screen_ref))

    def _loadNextScreen(self, screen_name, screen_ref, *largs): #screen_name, screen_ref):
        #screen_name = kwargs.pop('screen_name')
        #screen_ref = kwargs.pop('screen_ref')
        #Clock.unschedule(self.next)
        #screen_ref = MPLFigureScreen()
        self.logger.debug('LLLLLLLLLLLLLLLLLLLLLOADNEXTSCREEN ' + str(screen_name) + str(screen_ref))
        if (self.current != screen_name):
            if self.cur_screen_ref:
                import kivy
                self.logger.debug('&&&&&&&&&&&&&&&&&&&&&&&&' + str(self.cur_screen_ref.children[0].__class__))
                self.cur_screen_ref.clear_modal_widgets()
                self.remove_widget(self.cur_screen_ref)
                #self.remove_screen(self.cur_screen_ref)
            self.cur_screen_ref = screen_ref
            self.add_widget(screen_ref)
            self.current = screen_name

    def getSCMEvent(self, event_name):
        return self.scm_events.get(event_name)

    def setSCMEvent(self, event_name):
        event_handle = self.getSCMEvent(event_name)
        if event_handle:
            event_handle.set()

    def isSCMEventSet(self, event_name):
        event_handle = self.getSCMEvent(event_name)
        if event_handle:
            return event_handle.is_set()
        else:
            return False

    def addSCMEvent(self, event_name):
        if not self.scm_events.has_key(event_name):
            self.scm_events[event_name] = ThreadEvent()
            return True
        else:
            return False

    def removeSCMEvent(self, event_name):
        if self.scm_events.has_key(event_name):
            self.scm_events.pop(event_name)
            return True
        else:
            return False

    def showScreen(self, screen_ref, transition):
        print 'executed showScreen'
        pass

    def addScreen(self, screen_ref):
        print 'executed addScreen'
        pass

    def removeScreen(self, screen_ref):
        print 'executed removeScreen'
        pass




class WFScreen(Screen):
    event_lut = {}
    clear_list = []
    scm_ref = None
    logger = logging.getLogger('grabia_app.gbia_screens.manager.WFScreen')
    is_finished = None #ThreadEvent()
    set_finish = True

    def getFinishedEvent(self):
        return self.is_finished

    def clearFinishedEvent(self):
        self.is_finished.clear()

    def __init__(self, set_finish=True, **kwargs):
        #self.logger = logging.getLogger('grabia_app.gbia_screens.manager.WFScreen')
        # initialize the event lut with a list of event names, if given
        self.logger.debug('WFSCREEEEEEEEEEEEEEEEEEEEEEEEEEN ' + str(kwargs))
        self.scm_ref = kwargs.pop('scm_ref')
        event_list = kwargs.pop('event_list')
        self.set_finish = set_finish

        #self.is_finished = is_finished
        self.event_lut = {} # initialize
        for e_name in event_list:
            self.addEvent(e_name)

        is_finished_event = None
        if kwargs.has_key('is_finished'):
            is_finished_event = kwargs.pop('is_finished')
        screen_name = kwargs.pop('name')
        self.name = screen_name
        if is_finished_event and is_finished_event.__class__ is threading._Event:
            self.is_finished = is_finished_event
        else:
            self.logger.debug("using default is_finished thread event " + str(is_finished_event))
            self.is_finished = ThreadEvent()

        #super(WFScreen, self).__init__(**kwargs)
        Clock.schedule_once(partial(self._do_init, **kwargs))
        #self.is_finished.set()
        print 'WFSCREEEEEEEEEEEEEEEEEEEEEEEEEEN ', kwargs, self.name

    def _do_init(self, *largs, **kwargs):
        super(WFScreen, self).__init__(**kwargs)
        if self.is_finished and self.set_finish:
            self.is_finished.set()

    # remove any modal popup widgets
    def clear_modal_widgets(self):
        print '###??????????? removing clear_list ', self.clear_list
        for modal_widget in self.clear_list:
            if modal_widget.parent:
                print '###??????????? removing a modal widget ', modal_widget
                modal_widget.parent.remove_widget(modal_widget)

    def getEvent(self, event_name):
        event_handle = self.event_lut.get(event_name)
        if event_handle:
            self.logger.debug('getEvent ' + event_name + ' ' + str(event_handle.is_set()))
        else:
            self.logger.debug('getEvent ' + event_name + ' event NOT FOUND')
        return event_handle

    def setEvent(self, event_name):
        event_handle = self.getEvent(event_name)
        if event_handle:
            self.logger.debug('setEvent ' + event_name + ' ' + str(event_handle.is_set()))
            event_handle.set()

    def clearAllEvents(self):
        self.clearFinishedEvent()
        for event_name, event_handle in self.event_lut.iteritems():
            self.logger.debug('clearAllEvent:' + event_name + ' ' + str(event_handle.is_set()))
            event_handle.clear()

    def clearEvent(self, event_name):
        event_handle = self.getEvent(event_name)
        if event_handle:
            self.logger.debug('clearEvent ' + event_name + ' ' + str(event_handle.is_set()))
            event_handle.clear()

    def isEventSet(self, event_name):
        event_handle = self.getEvent(event_name)
        if event_handle:
            return event_handle.is_set()
        else:
            return False

    def addEvent(self, event_name):
        if not self.event_lut.has_key(event_name):
            event_handle = ThreadEvent()
            event_handle.clear()
            self.event_lut[event_name] = event_handle
            return True
        else:
            event_handle = self.event_lut[event_name]
            self.logger.debug('addEvent ' + event_name + ' event already exists ' + str(event_handle.is_set()))
            event_handle.clear()
            return False

    def removeEvent(self, event_name):
        if self.event_lut.has_key(event_name):
            self.event_lut.pop(event_name)
            return True
        else:
            return False

