__author__='spark'

from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.lang import Builder
from kivy.properties import StringProperty

from kivy.uix.image import Image, AsyncImage
from kivy.core.image import Image as CoreImage
from kivy.core.image import ImageData

from kivy.uix.boxlayout import BoxLayout

from kivy.properties import ObjectProperty,  NumericProperty, StringProperty

from kivy.graphics import Line, Rectangle, Color
from kivy.graphics.texture import Texture
from kivy.core.image.img_pygame import ImageLoaderPygame

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.backend_bases import MouseEvent
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
import StringIO

#MPL_DPI=60
MPL_DPI=100

Builder.load_string('''
<MPLFigureScreen>:
#	name: 'helloworld'
    scatter: _scatter
    memimage: _memimage
    boxlayout: _boxlayout
    FloatLayout:
        id: _scatter
        pos: self.pos
        size: root.size
        Scatter:
            id: _boxlayout
            orientation: 'vertical'
            size: root.size
            pos: self.pos
            GBIAMemoryImage:
                id: _memimage
                memory_data: root.fig_disp_data #root.disp_dataval
                pos: root.pos
                size: root.size
                allow_stretch: True
                source: root.img_src
<GBIAMemoryImage>:
    memory_data: None
''')

class MPLFigureScreen(Screen): #BoxLayout):
    fig = None
    axe = None
    fig_disp_data = None
    disp_dataval = None
    memimage = ObjectProperty(None)
    mplcanvas = ObjectProperty(None)
    tmp_cur_data = [1,2,3]

    scatter = ObjectProperty(None)
    boxlayout = ObjectProperty(None)
    img_src = StringProperty(None)
    pil_image = None

    #gbia workflow manager handle


    def mpl_on_click(self, event):
        # 'button_press_callback': whenever a mouse button is pressed
        if event.dblclick:
            print("DBLCLICK", event.x, event.y)
        else:
            print("DOWN    ", event.x, event.y)

    def mpl_on_release(self, event):
        # 'button_release_callback': whenever a mouse button is released
        #print("UP     ", event)
        pass

    def mpl_on_drag(self, event):
        pass
        # 'motion_notify_event': whenever mouse is moved
        #print("MOUSE moved   ", event.x, event.y)


    def __init__(self, **kargs):
        super(MPLFigureScreen, self).__init__(**kargs)
        print '##################### here: ', kargs
        print self
        #self.fig = plt.figure(1) #Figure(figsize=(5,5), dpi=100)
        self.fig = Figure(figsize=(5,5), dpi=100)
        self.mplcanvas = FigureCanvas(self.fig)
        #self.axe = self.fig.add_subplot(1,1,1)
        #self.axe.plot(self.tmp_cur_data)
        self.axe = self.fig.gca(projection='3d')
        theta = np.linspace(-4*np.pi, 4*np.pi, 100)
        z = np.linspace(-2,2,100)
        r = z**2 + 1
        x = r * np.sin(theta)
        y = r * np.cos(theta)
        self.axe.plot(x,y,z, label='parametric curve')

        # setup mpl callbacks
        self.fig.canvas.mpl_connect('button_press_event', self.mpl_on_click)
        self.fig.canvas.mpl_connect('button_release_event', self.mpl_on_release)
        self.fig.canvas.mpl_connect('motion_notify_event', self.mpl_on_drag)

        # setup kivy callbacks
        #self.bind(on_touch_down=self.on_touch_down)
        #self.bind(on_touch_move=self.on_touch_move)

        ##buf, size = self.fig.canvas.print_to_buffer()
        ##self.pil_image = PILImage.frombuffer('RGBA', size, buf, 'raw', 'RGBA', 0, 1)
        #self.disp_dataval = self.ImageData(pil_image.size[0], pil_image.size[1], 'RGBA', pil_image.tostring(), source=None)
        ##print ImageData._supported_fmts
        ##self.fig_disp_data = ImageData(self.pil_image.size[0], self.pil_image.size[1], 'rgba', self.pil_image.tobytes(), source=None)

        #if self.memimage:
        #	self.memimage.memory_data = self.fig_disp_data
        #plt.ioff()
        #plt.draw()
        self.fig_disp_data = StringIO.StringIO()
        self.mplcanvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
        print 'disp mem class: ', str(self.fig_disp_data.__class__)
        self.disp_dataval = self.fig_disp_data.getvalue()
        if self.memimage:
            self.memimage.memory_data = self.fig_disp_data##.getvalue()
            print 'debug: ', self.memimage._filename
        ###self.memimage = MemoryImage(self.fig_disp_data.getvalue())
        #self.memimage.memory_data = self.fig_disp_data.getvalue()
        #with self.canvas:
        #self.boxlayout.add_widget(self.memimage)
        ###self.add_widget(self.memimage)

    def on_touch_move(self, touch):
        #print "debuggggg mouse moved ", touch.pos
        ##buf, size = self.fig.canvas.print_to_buffer()
        ##self.pil_image = PILImage.frombuffer('RGBA', size, buf, 'raw', 'RGBA', 0, 1)
        ##self.fig_disp_data = ImageData(self.pil_image.size[0], self.pil_image.size[1], 'rgba', self.pil_image.tobytes(), source=None)
        ##self.memimage.memory_data = self.fig_disp_data

        self.fig_disp_data = StringIO.StringIO()
        self.mplcanvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
        self.memimage.memory_data = self.fig_disp_data##.getvalue()
        mpl_event_name = 'motion_notify_event'
        (mpl_x, mpl_y) = self.convertKivyPosToMPLPos(touch.pos, self.memimage)
        mpl_mouseevent = MouseEvent(mpl_event_name, self.fig.canvas, x=mpl_x, y=mpl_y, button=1, key=self.fig.canvas._key, step=0, dblclick=touch.is_double_tap, guiEvent=None)
        self.fig.canvas.callbacks.process(mpl_event_name, mpl_mouseevent) # trigger mpl mouse event
        return True

    def on_touch_down(self, touch):
        # if the touch occurred outside the widget
        if not self.collide_point(*touch.pos):
            # see if the super class knows how to handle this touch
            super(MPLFigureScreen, self).on_touch_down(touch)
        elif touch.button in( 'left'): # if left mouse button is clicked inside the widget
            #print "debuggggg left click, touch.pos ", self.to_window(*touch.pos)
            #print "debuggggg left click, self.pos ", self.pos
            #print "debuggggg left click, self.scatter.pos ", self.scatter.pos, self.scatter.x, self.scatter.y
            #print "debuggggg left click, self.boxlayout.pos ", self.convertKivyPosToMPLPos(touch.pos, self.boxlayout)
            #print "debuggggg left click, self.memimage.pos ", self.memimage.pos, self.memimage.width, self.memimage.height, self.memimage.img_loader.size
#			self.cur_selection_pos = touch.pos # keep a record of the first mouse click position
#			if self.cur_selection_area:
#				self.canvas.remove(self.cur_selection_area)
#			self.cur_selection_area = None
            mpl_event_name = 'button_press_event'

            (mpl_x, mpl_y) = self.convertKivyPosToMPLPos(touch.pos, self.memimage)
            mpl_mouseevent = MouseEvent(mpl_event_name, self.fig.canvas, x=mpl_x, y=mpl_y, button=1, key=self.fig.canvas._key, step=0, dblclick=touch.is_double_tap, guiEvent=None)
            self.fig.canvas.callbacks.process(mpl_event_name, mpl_mouseevent) # trigger mpl mouse event

            #self.tmp_cur_data = [a * 2 for a in self.tmp_cur_data]
            #self.axe.plot(self.tmp_cur_data)
            #plt.ioff()
            #plt.draw()
            ##buf, size = self.fig.canvas.print_to_buffer()
            ##self.pil_image = PILImage.frombuffer('RGBA', size, buf, 'raw', 'RGBA', 0, 1)
            ##self.fig_disp_data = ImageData(self.pil_image.size[0], self.pil_image.size[1], 'rgba', self.pil_image.tobytes(), source=None)
            ##self.memimage.memory_data = self.fig_disp_data
            self.fig_disp_data = StringIO.StringIO()
            self.mplcanvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
            #print 'disp mem class: ', str(self.fig_disp_data.__class__)
            self.memimage.memory_data = self.fig_disp_data##.getvalue()
            #self.memimage.on_memory_data()

        return True

    def convertKivyPosToMPLPos(self, touch_pos, kivy_widget):
        kivy_pos = kivy_widget.pos
        ##memimg_width = self.pil_image.size[0] ##self.memimage.img_loader.width
        ##memimg_height = self.pil_image.size[1] ##self.memimage.img_loader.height
        memimg_width = self.memimage.img_loader.width
        memimg_height = self.memimage.img_loader.height
        #memimg_len = max(memimg_width, memimg_height)
        kivyimg_width = kivy_widget.width
        kivyimg_height = kivy_widget.height
        #kivy_len = min(kivy_widget_width, kivy_widget_height)

        # convert from Kivy to GBIA coordinate
        width_ratio = float(memimg_width) / kivyimg_width
        height_ratio = float(memimg_height) / kivyimg_height
        print 'memimg size: ', memimg_width, memimg_height
        print 'ratios: ', width_ratio, height_ratio

        ratio = max(width_ratio, height_ratio)
        print 'KivyToGBIA ratio: ', ratio

        memimg_kivy_width = memimg_width / ratio
        memimg_kivy_height = memimg_height / ratio

        print 'memimg kivy size: ', memimg_kivy_width, memimg_kivy_height

        offset_pos = (abs(kivyimg_width - memimg_kivy_width)/2.0, abs(kivyimg_height - memimg_kivy_height)/2.0)
        print 'offset_pos: ', offset_pos
        gbia_xpos = touch_pos[0]-self.memimage.x-offset_pos[0]
        gbia_ypos = touch_pos[1]-self.memimage.y-offset_pos[1]

        # convert from GBIA to MPL coordinate and finally return MPL coordinate
        mpl_width_ratio = memimg_width / memimg_kivy_width
        mpl_height_ratio = memimg_height / memimg_kivy_height

        #print 'convertion: ', touch_pos, gbia_xpos, gbia_ypos, mpl_width_ratio, mpl_height_ratio

        return (gbia_xpos * mpl_width_ratio, gbia_ypos * mpl_height_ratio)




class GBIAMemoryImage(AsyncImage):
    """Display an image already loaded in memory."""
    memory_data = ObjectProperty(None)
    img_loader = ObjectProperty(None)
    # Kivy 1.9 does not support uix.Image to be initialized from memory-based image data.
    # matplotlib integration through image transfer requires the use of memory-based image data
    # the following hack will save image data into a temporary file for Kivy to be happy for now...
    _filename = '_tmp_io_file.png'

    def __init__(self, **kwargs): #(self, memory_data, **kwargs):
        super(GBIAMemoryImage, self).__init__(**kwargs)

        print 'debuggg: MemoryImage initializing... '
        #self.memory_data = memory_data
        if self.memory_data:
            ##data = StringIO.StringIO(self.memory_data)
            ##self.img_loader = ImageLoaderPygame(filename=self._filename, ext='png', inline=True, rawdata=self.memory_data, nocache=True)
            self._filename = '__inline__'
            self.img_loader = ImageLoaderPygame(filename=self._filename, inline=True, rawdata=self.memory_data, nocache=True)


    def on_memory_data(self, *args):
        """Load image from memory."""
        #data = StringIO.StringIO(self.memory_data)
        if self.memory_data:
            #data = io.BytesIO(self.memory_data)
            ##data = StringIO.StringIO(self.memory_data)
            ####with open(self._filename, 'wb') as f:
            ####	f.write(data.read())
            #CoreImage can be instantiated from memory-based image data
            #loaded_img = CoreImage(data, ext="png")
            #with self.canvas:
                #AsyncImage(source='self.filename')
            #print '#################################: ', self._filename
            #self.img_loader = ImageLoaderPygame(self._filename, nocache=True)
            #self.img_loader.filename = self._filename
            ##self.img_loader._data = self.img_loader.load(data) #self._filename)
            ##self.texture = Texture.create_from_data(self.img_loader._data, self.img_loader._mipmap)
            ####self.img_loader = ImageLoaderPygame(filename=self._filename, nocache=True)
            self.img_loader = ImageLoaderPygame(filename=self._filename, ext='png', inline=True, rawdata=self.memory_data, nocache=True)
            self.texture = self.img_loader.texture
            ##self.img_loader = ImageLoaderPygame(filename=None, ext='png', inline=True, rawdata=self.memory_data, nocache=True)
                ##self.add_widget(Image(source=self._filename))
                #im._coreimage = loaded_img
                #self.add_widget(im)
                 #Rectangle(texture=loaded_img.texture)

