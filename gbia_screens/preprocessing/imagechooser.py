__author__='spark'

from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.carousel import Carousel
from kivy.uix.scrollview import ScrollView
from kivy.lang import Builder
from kivy.adapters.models import SelectableDataItem
from kivy.uix.listview import ListView, ListItemButton, CompositeListItem
from kivy.adapters.listadapter import ListAdapter
from kivy.adapters.dictadapter import DictAdapter
from kivy.uix.gridlayout import GridLayout
from kivy.properties import NumericProperty, ListProperty, \
        BooleanProperty, AliasProperty, ObjectProperty, StringProperty
from kivy.uix.image import Image, AsyncImage
from gbia_screens.base.filebrowser import FileBrowser
from gbia_screens.manager import WFScreen

from kivy.clock import Clock
from threading import Event as ThreadEvent
from functools import partial

from gbimageanalyzer.core.gbia_imglist_manager import ImageListManager

from functools import partial

Builder.load_string('''
<ImageSequenceBrowser>:
    clear_list: [popup_demo]
    folder_path_label: _folder_path_label
    status_label: _status_label
    img_detailed_view: _img_detailed_view
    cur_path: ''
    cur_range: [0]
    cur_range_str: ''
    cur_status_text: 'Please choose a folder with microscopy image sequences for analysis.'
    next_btn: _next_btn
    BoxLayout:
        orientation: 'vertical'
        canvas.before:
            Color:
                rgba: 0.3,0.3,0.4,0.5
            Rectangle:
                pos: root.pos
                size: root.size

        BoxLayout:
            orientation: 'vertical'
            ImageDetailedView:
                id: _img_detailed_view
                root_ref: root
                img_path: None
                pos: root.pos
                size_hint: 1.0, 1.0
        FloatLayout:
            size: root.size
            Label:
                id: _folder_path_label
                text: root.cur_path
                color: 1,1,0,1
                size_hint: .8, .1
                pos_hint: {'x': .1, 'y': .8}
                halign: 'left'
            Label:
                id: _status_label
                text: root.cur_status_text
                color: 1,1,0,1
                size_hint: .8, .1
                pos_hint: {'x': .1, 'y': .6}
                halign: 'left'
            Label:
                id: _range_label
                text: root.cur_range_str
                pos_hint: {'x': .1, 'y': .6}
            Button:
                text: 'Select image folder'
                pos_hint: {'x': .2, 'y': .2}
                size_hint: .2, .1
                #theme: ('green', 'accent')
                on_release: popup_demo.open()
                popup_demo: popup_demo.__self__
            Button:
                id: _dashboard_btn
                text: 'Dashboard'
                pos_hint: {'x': .6, 'y': .4}
                size_hint: .2, .1
                on_release: app.wfm.getWorkflow('dashboard_wf').lookupTaskMemory('tid_load_dashboard_scr').executeTask()
            Button:
                id: _next_btn
                text: 'Next'
                disabled: True
                pos_hint: {'x': .6, 'y': .2}
                size_hint: .2, .1
                on_release: if root.isScreenTaskComplete(): app.wfm.getWorkflow('imagechooser_wf').lookupTaskMemory('tid_run_next_wf').executeTask() #root.setEvent('next_btn')
            Popup:
                id: popup_demo
                title: 'Flat popup demo'
                title_color_tuple: ('Gray', '1000')
                size_hint: .9, .9
                on_parent: if self.parent: self.parent.remove_widget(self)

                FileBrowser:
                    select_string: 'Select'
                    on_success: root.updateImageFolderPath(self.filename); popup_demo.dismiss()
                    on_canceled: popup_demo.dismiss()

<ImageDetailedView>:
    cols: 1
    img_path: None
    img_slider: _img_slider
    imglist_view: _imglist_view
    canvas.before:
        Color:
            rgba: 0.5,0.7,1,0.5
        Rectangle:
            pos: root.pos
            size: root.size
    BoxLayout:
        orientation: 'horizontal'
        GridLayout:
            id: _imglist_view
            cols: 1
            pos: root.pos
            size_hint: 0.5, 1.0
        GBIAImageSlider:
            id: _img_slider
            pos: root.pos
            size_hint: 0.5, 1.0
            #source: root.img_path
            canvas.before:
                Color:
                    rgba: 0.5, 0.7, 1.0, 0.5
                Rectangle:
                    pos: self.pos
                    size: self.size

<Page>:
    background: background
    page_annotation: 'test'
    AnchorLayout:
        Scatter:
            id: background
            do_rotation: False
            do_translation: False
            do_scale: False
            FloatLayout:
                pos: root.pos
                size: root.size
                Image:
                    source: root.source
                    width: root.width
                    height: root.height
                    Label:
                        text: root.page_annotation
                        color: 1,1,0,0.8
                        size_hint: .8, .1
                        pos_hint: .8, .9
                        halign: 'left'

''')

class Page(Screen):
    source = StringProperty()
#    page_annotation = StringProperty()

class GBIAImageSlider(Carousel): #ScreenManager):
    img_widget_lut = {}

    def addImage(self, img_idx, img_path):
        if not self.img_widget_lut.has_key(img_idx):
            img_obj = Page(source=img_path)
            img_obj.page_annotation = 'image #' + str(img_idx)
            self.img_widget_lut[img_idx] = (img_path, img_obj)
            self.add_widget(img_obj)

    def removeImage(self, img_idx):
        if self.img_widget_lut.has_key(img_idx):
            self.remove_widget(self.img_widget_lut[img_idx][1])
            self.img_widget_lut.pop(img_idx)

class OpsDictAdapter(DictAdapter):

    listview_id = NumericProperty(0)
    owning_view = ObjectProperty(None)

    def __init__(self, **kwargs):
        self.listview_id = kwargs['listview_id']
        super(OpsDictAdapter, self).__init__(**kwargs)

    def on_selection_change(self, *args):
        for i in range(len(self.selection)):
            listview_selection_buttons[self.listview_id][i].text = \
                    self.selection[i].text

        if self.listview_id is 0:
            # Scroll to the most recently selected item.
            if len(self.selection) > 0:
                print('selection', self.selection)
                self.owning_view.scroll_to(
                    index=self.sorted_keys.index(self.selection[-1].text))

        elif self.listview_id is 1:
            # Scroll to the selected item that is the minimum of a sort.
            if len(self.selection) > 0:
                self.owning_view.scroll_to(
                    index=self.sorted_keys.index(
                        sorted([sel.text for sel in self.selection])[0]))

        elif self.listview_id is 2:
            # Scroll to the selected item that is the maximum of a sort.
            if len(self.selection) > 0:
                self.owning_view.scroll_to(
                    index=self.sorted_keys.index(
                        sorted([sel.text for sel in self.selection])[-1]))


class ImageDetailedView(GridLayout):
    #imglist_view = ObjectProperty(None)
    def __init__(self, **kwargs):
        #kwargs['cols'] = 1
#        if img_path:
#            self.img_path = img_path
        super(ImageDetailedView, self).__init__(**kwargs)

#    def redraw(self, *args):
#        self.clear_widgets()
#        if self.img_ref.img_src:

    def img_changed(self, list_adapter, *args):
        print 'cur_range: ', self.root_ref.cur_range
        if len(list_adapter.selection) == 0:
            self.img_path = None
        else:
            selected_obj_idx = list_adapter.selection[0].index #selection[0]
            print 'cur_choice: ', selected_obj_idx
            #print 'selection list ', list_adapter.selection
            #print 'data list ', list_adapter.data
            self.img_path = list_adapter.data[selected_obj_idx].img_path
            if selected_obj_idx not in self.root_ref.cur_range:
                if len(self.root_ref.cur_range) == 2:
                    # i don't know if changing this value directly might have an adverse effect to
                    # the screen event trying to grab the value while being updated.
                    # Would be too expensive to call Clock.schedule_once event...
                    # If need be, a "critical section" mechanism much like that used to prevent deadlock is needed.
                    self.root_ref.cur_range[1] = selected_obj_idx # only change the second range val
                elif len(self.root_ref.cur_range) == 1:
                    self.root_ref.cur_range.append(selected_obj_idx)
            elif len(self.root_ref.cur_range) == 2:
                # clear the range and start from ground zero
                self.root_ref.cur_range = [selected_obj_idx]

        print 'cur_range after change: ', self.root_ref.cur_range

        if len(self.root_ref.cur_range) == 2:
            self.root_ref.cur_range_str = 'Current range: ' + str(self.root_ref.cur_range)
        else:
            self.root_ref.cur_range_str = ''
#        self.redraw()

class GBIAImageListItem(SelectableDataItem):
    name = None
    img_path = None
    def __init__(self, img_idx, name, img_path, is_selected, **kwargs):
        #super(GBIAImageListItem, self).__init__(is_selected=is_selected, **kwargs)
        self.img_idx = img_idx
        self.name = name
        self.img_path = img_path
        self.is_selected = is_selected

class GBIAListItemButton(ListItemButton):
    root_ref = None
    is_change_from_code = False

    def __init__(self, root_ref, **kwargs):
        super(GBIAListItemButton, self).__init__(**kwargs)
        self.root_ref = root_ref

    def select(self, *args):
#        if self.is_selected:
#            print 'selected twice'
#            self.root_ref.cur_idx_pick = self.index
#        else:
        if not self.is_change_from_code:
            print 'selected 1 ', self.index
            #self.is_selected = True
            self.root_ref.cur_idx_pick = self.index
        else:
            print 'selected 2 ', self.index
            self.root_ref.addImageToSlider(self.index)
            #self.background_color = self.selected_color
            self.select_from_composite(*args)

        #super(GBIAListItemButton, self).select(*args)
        #print 'GBIALLLLLLLLLLLLLLISTITEMBUTTON ', self.is_selected, self.parent, dir(self)
        #if self.background_color == self.selected_color: # if selected twice, used in lieu of the broken is_selected property
        #    print 'selected twice'
        #    if isinstance(self.parent, CompositeListItem):
        #        self.parent.select_from_child(self, *args)
        #else:
        #    print 'first selection'
        #    self.background_color = self.selected_color
            ##if isinstance(self.parent, CompositeListItem):
            ##    self.parent.select_from_child(self, *args)
            #super(GBIAListItemButton, self).select(*args)
            #self.is_selected = True

    def deselect(self, *args):
        if not self.is_change_from_code:
            print 'deselected 1 ', self.index
            #self.is_selected = False
            self.root_ref.cur_idx_pick = self.index
        else:
            #self.is_selected = False
            print 'deselected 2 ', self.index
            self.root_ref.removeImageFromSlider(self.index)
            self.deselect_from_composite(*args)
        #self.background_color = self.deselected_color
#        if self.is_selected:
#            cur_range = self.root_ref.cur_range
#            if self.index == min(cur_range) or self.index == max(cur_range):
#                self.root_ref.cur_idx_pick = self.index
#                #import pdb; pdb.set_trace()
#                print 'deslect'
#                self.is_selected = False
#                self.deselect_from_composite(*args)
#            elif self.index > min(cur_range) and self.index < max(cur_range):
#
#
#       if self.is_selected and (self.index == min(self.root_ref.cur_range) or self.index == max(self.root_ref.cur_range)):

        #super(GBIAListItemButton, self).deselect(*args)
        #self.is_selected = False
        #print 'deselected'

# this screen sets the following events
# next_btn
class ImageSequenceBrowser(WFScreen):
    source = StringProperty()
    is_dependency_complete = ObjectProperty(None)
    folder_path_label = ObjectProperty(None)
    #imglist_view = ObjectProperty(None)
    img_detailed_view = ObjectProperty(None)
#    cur_path = StringProperty()
    cur_range = ObjectProperty(None)
    cur_range_str = StringProperty()
    cur_status_text = StringProperty()
    cur_idx_pick = None

    img_list_adapter = None
    img_list_view = None


    def __init__(self, **kwargs):
        print 'FFFFFFFFFFFFFFFFFFFFFFFF ImageSequenceBrowser initializing...'
        #import pdb; pdb.set_trace()
#        Clock.schedule_once(partial(self._do_init, **kwargs))
#    def _do_init(self, *largs, **kwargs): #, is_finished, **kwargs):
        #super(ImageSequenceBrowser, self).__init__(scm_ref, ['next_btn'], **self.kwargs)
        super(ImageSequenceBrowser, self).__init__(**kwargs)
        print 'FFFFFFFFFFFFFFFFFFFFFFFF ImageSequenceBrowser initialized.'
        # if self.is_finished:
        #     self.is_finished.set()

    def retrieveUserChoices(self):
        if len(self.cur_range) == 2 and self.cur_path:
            first_img_path_in_range = self.img_list_adapter.data[self.cur_range[0]].img_path
            return [first_img_path_in_range, self.cur_path, self.cur_range]

    def addImageToSlider(self, img_idx):
        if self.img_list_adapter:
            self.img_detailed_view.img_slider.addImage(img_idx, self.img_list_adapter.data[img_idx].img_path)

    def removeImageFromSlider(self, img_idx):
        if self.img_list_adapter:
            self.img_detailed_view.img_slider.removeImage(img_idx)


    def isScreenTaskComplete(self):
        if self.cur_path and len(self.cur_range) == 2:
            return True
        else:
            return False

    def initScreenStates(self):
        self.cur_range = [0]

    def updateImageFolderPath(self, path_str):
        if path_str:
            try:
                self.cur_path = path_str
                self.cur_status_text = 'Please select a sequence range and click Next.'
                #Clock.schedule_once(self.loadImageRangeChooser)
                self.loadImageRangeChooser()
                self.next_btn.disabled = False
            except Exception as folder_ex:
                self.cur_path = ''
                self.cur_status_text = 'You selected an invalid folder.\nPlease select a folder containing image sequences from time-lapse microscopy. ;)'



    def processChangedSelection(self, list_adapter, *args):
        ##self.img_detailed_view.img_changed(list_adapter, *args)
        Clock.schedule_once(partial(self._processChangedSelection, list_adapter, *args))

    def _processChangedSelection(self, list_adapter, *args):
        print 'cur_range: ', self.cur_range, self.cur_idx_pick
#        if len(list_adapter.selection) == 0:
#            self.cur_range_str = ''
#        else:
#        import pdb; pdb.set_trace()

        self.deselectAllViewItems()

        selected_obj_idx = [selection.index for selection in list_adapter.selection] #[0].index #selection[0]
        print 'cur_choice: ', selected_obj_idx
        #print 'selection list ', list_adapter.selection
        #print 'data list ', list_adapter.data
        last_idx = self.cur_idx_pick #max(selected_obj_idx)
        #self.img_detailed_view.img_path = list_adapter.data[last_idx].img_path #select_item_view(list_adapter.get_view(last_idx)).img_path
        if last_idx not in self.cur_range:
            if len(self.cur_range) == 2:
                # i don't know if changing this value directly might have an adverse effect to
                # the screen event trying to grab the value while being updated.
                # Would be too expensive to call Clock.schedule_once event...
                # If need be, a "critical section" mechanism much like that used to prevent deadlock is needed.
                self.cur_range[1] = last_idx # only change the second range val
                self.cur_range = sorted(self.cur_range)
            elif len(self.cur_range) == 1:
                self.cur_range.append(last_idx)
                self.cur_range = sorted(self.cur_range)
        elif len(self.cur_range) == 2:
            # clear the range and start from ground zero
            self.cur_range = [last_idx]
#            self.img_list_view
#            import pdb; pdb.set_trace()
            #btn_obj = self.img_list_adapter.get_view(last_idx)
            #if not btn_obj.is_selected:
            #    self.img_list_adapter.select_item_view(btn_obj)
            #    self.img_detailed_view.img_slider.addImage(last_idx, list_adapter.data[last_idx].img_path) #btn_obj.img_path)

        else: # len(self.cur_range) == 0:
            self.cur_range = [last_idx]

        print 'cur_range after change: ', self.cur_range

        if len(self.cur_range) == 2:
            self.cur_range_str = 'Current range: ' + str(self.cur_range)
        else:
            self.cur_range_str = ''


        idx_list = range(min(self.cur_range), max(self.cur_range)+1)
        print 'index range to highlight: ', idx_list, list_adapter.selection, dir(self.img_list_view) #list_adapter.data
        #import pdb; pdb.set_trace()

        for idx in idx_list:
            btn_obj = list_adapter.get_view(idx)
            #if not btn_obj.is_selected:
            btn_obj.is_change_from_code = True
#            list_adapter.select_item_view(btn_obj)
            btn_obj.select()
            btn_obj.is_change_from_code = False
                #btn_obj.select(*args)

        print 'image widget lut: ', self.img_detailed_view.img_slider.img_widget_lut


    def deselectAllViewItems(self):
        print 'range reset: ', self.img_list_adapter.selection
        for btn_obj in self.img_list_adapter.selection:
            #if btn_obj.is_selected:
            btn_obj.is_change_from_code = True
            self.img_list_adapter.deselect_item_view(btn_obj)
#            btn_obj.deselect()
            #            list_adapter.get_view(btn_obj.index).trigger_action(duration=0)
            btn_obj.is_change_from_code = False

        idx_list = range(min(self.cur_range), max(self.cur_range)+1)

        for idx in idx_list:
            btn_obj = self.img_list_adapter.get_view(idx)
            #if not btn_obj.is_selected:
            btn_obj.is_change_from_code = True
            #            list_adapter.select_item_view(btn_obj)
            btn_obj.deselect()
            btn_obj.is_change_from_code = False
        #print 'selection after reset ', self.img_list_adapter.selection

    def loadImageRangeChooser(self):
        ilm = ImageListManager(self.cur_path)
        img_path_list = ilm.getImagePathList(0,0,0)

        print 'LLLLLLLLLOADIMAGERANGECHOOSER ', img_path_list
        list_item_args_converter = \
                lambda row_index, selectable: {'text': str(selectable.img_idx),
                                               'img_path': selectable.img_path,
                                               'size_hint_y': None,
                                               'deselected_color': [0.6,0.6,0.8,1.0],
                                               'selected_color': [0.7,0.4,0.6,1.0],
                                               'height': 25}

        selectable_img_list = [GBIAImageListItem(img_idx, img_name, img_path, False) for (img_idx, img_name, img_path) in img_path_list]
        self.img_list_adapter = ListAdapter(data=selectable_img_list,
                                       args_converter=list_item_args_converter,
                                       selection_mode='single', #'multiple',
                                       allow_empty_selection=False,
                                       cls=partial(GBIAListItemButton, self))

        # building an image list
        self.img_list_view = ListView(adapter=self.img_list_adapter, size_hint=(.2,0.5))

        self.img_detailed_view.imglist_view.add_widget(self.img_list_view)

        btn_obj = self.img_list_adapter.get_view(0)
#        if not btn_obj.is_selected:
        btn_obj.is_change_from_code = True
#        self.img_list_adapter.select_item_view(btn_obj)
        btn_obj.select()
        btn_obj.is_change_from_code = False
#        self.img_detailed_view.img_slider.addImage(0, btn_obj.img_path)
        self.addImageToSlider(0)
#        select_idx = self.img_list_adapter.selection[0].index
#        self.img_detailed_view.img_path = self.img_list_adapter.data[select_idx].img_path #selection[0].img_path
        #detailed_view = ImageDetailedView(img_list_adapter.selection[0].img_path, cols=1, size_hint=(.5, 1.0))
        self.img_list_adapter.bind(on_selection_change=self.processChangedSelection)

        # building a rangle tuple
        #range_label_tuple = [ListItemLabel(), ListItemLabel()]
        #range_tuple_adapter = ListAdapter(data=range_label_tuple,
        #                                  args_converter=list_lab)
        #range_tuple_view = ListView(adapter=range_tuple_adapter)


    def doReturn(self):
        print 'executed doReturn'

#class ImageSequenceBrowser(FileBrowserScreen):
#
#    def __init__(self, **kargs):
#        super(ImageSequenceBrowser, self).__init__(**kargs)
#        #self.updateImageFolderPath(self.cur_path)


