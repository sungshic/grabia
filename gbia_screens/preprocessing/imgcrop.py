
from kivy.uix.widget import Widget
from kivy.uix.button import Button

from kivy.uix.scatter import Scatter
from kivy.uix.image import Image, AsyncImage
from kivy.core.image import Image as CoreImage
#from kivy.core.image import ImageData
from kivy.uix.boxlayout import BoxLayout

from kivy.properties import ObjectProperty,  NumericProperty, StringProperty

from kivy.graphics import Line, Rectangle, Color
#from kivy.graphics.texture import Texture
#from kivy.core.image.img_pygame import ImageLoaderPygame
#from kivy.core.image.img_pil import ImageLoaderPIL

from kivy.lang import Builder

from gbia_screens.manager import WFScreen
from kivy.clock import Clock
from functools import partial

Builder.load_string('''
<ImageCropWidget>:
    scatter: _scatter
    rect: _rect
    image: _image
    toolbox: _toolbox
    Scatter:
        id: _scatter
        pos: root.pos
        size: root.size
        Image:
            id: _image
            pos: root.pos
            size: root.size
            source: root.img_src
    BoxLayout:
        id: _toolbox
        size_hint: (1,None)
        orientation: 'horizontal'
        Button:
            text: 'Save'
    Widget:
        id: _rect
        size: (root.width/3, root.height/3)
        background_color: [0.,1.,0.,0.80]
''')



class ImageCropWidget(WFScreen):
    source = StringProperty()
    scatter = ObjectProperty(None)
    image = ObjectProperty(None)
    rect = ObjectProperty(None)
    toolbox = ObjectProperty(None)
    xmin = NumericProperty(20)
    xmax = NumericProperty(40)
    ymin = NumericProperty(20)
    ymax = NumericProperty(40)

    img_src = StringProperty(None)

    cur_selection_area = None
    cur_selection_pos = None

    is_finished = None
    #kwargs = {}

    def __init__(self, is_finished=None, **kwargs):
        self.is_finished = is_finished
        #self.kwargs = kwargs
        Clock.schedule_once(partial(self._do_init, kwargs=kwargs))

    def _do_init(self, **kwargs):
        #print 'debugggggg ', self.kwargs
        #super(ImageCropWidget, self).__init__(self.is_finished, ['next_btn2'], **(self.kwargs))
        super(ImageCropWidget, self).__init__(**kwargs)
        #self.bind(size = self.rescale_rec)
        #self.oldsize = self.size
        print self.size
        print self.pos
        #self.hideLimits()
        #self.defaultLimits()
        self._resizing = []

    def on_touch_down(self, touch):
        # if the touch occurred outside the widget
        if not self.collide_point(*touch.pos):
            # see if the super class knows how to handle this touch
            super(ImageCropWidget,self).on_touch_down(touch)
        elif touch.button in( 'left'): # if left mouse button is clicked inside the widget
            print "debuggggg left click ", touch.pos
            self.cur_selection_pos = touch.pos # keep a record of the first mouse click position
            if self.cur_selection_area:
                self.canvas.remove(self.cur_selection_area)
            self.cur_selection_area = None

        return True

    def on_touch_move(self, touch):
        # if the touch occurred inside the widget
        if self.collide_point(*touch.pos):
            cur_pos = touch.pos
            cur_size = (cur_pos[0] - self.cur_selection_pos[0], cur_pos[1] - self.cur_selection_pos[1])
            if self.cur_selection_area is None:
                with self.canvas:
                    self.cur_selection_area = Line(rectangle=self.cur_selection_pos+cur_size) # initialize
            else:
                with self.canvas:
                    self.cur_selection_area.rectangle = self.cur_selection_pos+cur_size # update size
        else:
            super(ImageCropWidget,self).on_touch_move(touch)

        return True

    def on_touch_up(self, touch):
        if self.cur_selection_area:
            print 'selection: ', self.cur_selection_area.points
            print 'image: ', self.image.pos, self.image.size
            print 'scatter pos: ', self.scatter.pos
            print 'scale: ', self.scatter.scale
        # if the touch occurred outside the widget
        if not self.collide_point(*touch.pos):
            super(ImageCropWidget,self).on_touch_up(touch)
        return True

