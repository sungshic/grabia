#from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button

from kivy.uix.scatter import Scatter
from kivy.uix.image import Image, AsyncImage
from kivy.core.image import Image as CoreImage
#from kivy.core.image import ImageData
from kivy.uix.boxlayout import BoxLayout

from kivy.properties import ObjectProperty,  NumericProperty, StringProperty

from kivy.graphics import Line, Rectangle, Color
#from kivy.graphics.texture import Texture
#from kivy.core.image.img_pygame import ImageLoaderPygame
#from kivy.core.image.img_pil import ImageLoaderPIL

from kivy.lang import Builder

from gbia_screens.manager import WFScreen
from kivy.clock import Clock
from threading import Event as ThreadEvent
from gbia_screens.base.imgcrop import ImageCropWidget
from functools import partial
from os.path import isfile
import logging

Builder.load_string('''
#:kivy 1.9

<GBIAImgCropWidget>:
    cropwidget: _cropwidget
    saveFilepath: _cropwidget.saveFilepath
    next_btn: _next_btn
    canvas.before:
        Color:
            rgba: 0,0,0,1
        Rectangle:
            pos: root.pos
            size: root.size
    Label:
        text: 'Select an area to be used as a landmark, and click Next'
        size_hint: 1, .1
        #pos_hint: {'x': .0, 'y': .0}
    BoxLayout
        orientation: 'horizontal'
        BoxLayout:
            size_hint: .8, 1
            ImageCropWidget:
                id: _cropwidget
                img_src: root.img_src
                root_screen_ref: root
                #pos: root.pos
                #size_hint: .8, .8
                #pos_hint: {'x': .1, 'y': .1}
        FloatLayout:
            size_hint: .2, .6
            Button:
                id: _dashboard_btn
                text: 'Dashboard'
                pos_hint: {'x': .1, 'y': .6}
                size_hint: .6, .1
                on_release: app.wfm.getWorkflow('dashboard_wf').lookupTaskMemory('tid_load_dashboard_scr').executeTask()
            Button:
                id: _next_btn
                text: 'Next'
                disabled: True
                pos_hint: {'x': .1, 'y': .2}
                size_hint: .6, .1
                on_release: if root.isScreenTaskComplete(): app.wfm.getWorkflow('landmarkchooser_wf').lookupTaskMemory('tid_load_next_wf').executeTask()
''')


# this screen sets the following events
# next_btn
class GBIAImgCropWidget(WFScreen):
    source = StringProperty()
    img_src = StringProperty('testimg.jpg')
#    layout_drop = ObjectProperty(None)
#    is_finished = ThreadEvent()
    is_updated = None
    #kwargs = {}
    update_image_path = None
    logger = logging.getLogger('grabia_app.gbia_screens.preprocessing.landmarkchooser.GBIAImgCropWidget')


    def isReady(self):
        return (self.is_finished and self.is_updated)

    def __init__(self, **kwargs):
        self.logger.debug('GBIAIIIIIIIIIIIIIMMMMMMMGGGGGGGGGGGGCROPWIDGET '+str(kwargs))
        img_src = kwargs.pop('img_src')
        #self.saveFilepath = kwargs.pop('landmark_path')
        #self.kwargs = kwargs

        self.update_image_path = img_src
#        Clock.schedule_once(partial(self._do_init, **kwargs))
#        print 'GBIAIIIIIIIIIIIIIMMMMMMMGGGGGGGGGGGGCROPWIDGET 2', self.name

#    def _do_init(self, *largs, **kwargs):
        if self.update_image_path:
            self.img_src = self.update_image_path
        if kwargs.has_key('is_updated'):
            self.is_updated = kwargs.pop('is_updated') #meta(tid_landmarkchooser_screen.getEvent('scr_task_flag'))
#        print 'GGGGGBIAIIIIIIIIIIIIIIIIIIIIIIMGCROPWIDGET ', self.kwargs
        #super(GBIAImgCropWidget, self).__init__(scm_ref, ['next_btn2'], **self.kwargs)
        super(GBIAImgCropWidget, self).__init__(**kwargs)
        #self.cropwidget.saveFilepath = self.saveFilepath
#        if self.is_finished:
#            self.is_finished.set()
#        cw = ImageCropWidget(**self.kwargs)
#        self.layout_drop.add_widget(cw)

    def isScreenTaskComplete(self):
        if isfile(self.cropwidget.saveFilepath):
            self.logger.debug('ScreenTask is completed: '+self.cropwidget.saveFilepath)
            print 'pressed !'
            return True
        else:
            self.logger.debug('ScreenTask is NOT completed')
            print 'pressed not ready'
            return False

    def initializeBtns(self):
        self.next_btn.disabled = True
        self.cropwidget.clearCurAreaSelection()

    def updateImage(self, image_path, landmark_path):
        self.is_updated.clear()
        self.update_image_path = image_path
        self.cropwidget.saveFilepath = landmark_path
        Clock.schedule_once(self._doUpdateImage)

    def _doUpdateImage(self, *largs):
        self.img_src = self.update_image_path
        self.logger.debug('image is updated: '+self.update_image_path)
        self.is_updated.set()




