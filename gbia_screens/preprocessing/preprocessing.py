__author__='spark'

from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty
from gbia_screens.manager import WFScreen
from threading import Event as ThreadEvent
from kivy.clock import Clock, mainthread
from kivy.animation import Animation
from kivy.factory import Factory
from kivy.uix.progressbar import ProgressBar
import subprocess
import time
from functools import partial
import threading

from os.path import expanduser
#from gbimageanalyzer.core.temporalCorrespondences import setClusterNodesByImgUUID, setClusterCorrespondencesByUUID, setSingleCellCorrespondencesByGBIAClusters
from gbimageanalyzer.core import dographdb as gdb
from uuid import uuid4
import os
import datetime
import logging
import json
import pickle
#from shapely.geometry import Polygon, box
##from sympy.geometry import Polygon
from gbimageanalyzer import celery_client_task

Builder.load_string('''
<PreprocessingScreen>:
    next_btn: _next_btn
    go_btn: _go_btn
    anim_box: _anim_box
    cur_status_text: ''
    canvas.before:
        Color:
            rgba: 0.3,0.3,0.4,0.5
        Rectangle:
            pos: self.pos
            size: self.size
    BoxLayout:
        orientation: 'vertical'
        FloatLayout:
            size_hint: 1.0, 0.4
            Image:
                source: 'grabia_icon.png'
                size_hint: 1.0, 1.0
                pos_hint: {'x': -.3, 'y': .0}
            Label:
                size_hint: .5, .1
                pos_hint: {'x': .5, 'y': .05}
                font_size: '48sp'
                text: "[b]image analysis[/b]"
                markup: True
        FloatLayout:
            pos: root.pos
            size: root.size
            Label:
                text: root.cur_status_text
                color: 1,1,0,1
                size_hint: .8, .1
                pos_hint: {'x': .1, 'y': .8}
                halign: 'left'
            AnchorLayout:
                id: _anim_box
                pos_hint: {'x': .1, 'y': .6}
                size_hint: .8, .1

            Button:
                id: _go_btn
                text: 'Start analysis'
                disabled: False
                pos_hint: {'x': .5, 'y': .2}
                size_hint: .2, .1
                on_release: self.disabled=True; root.next_btn.disabled=True; root.runPipeline() #setEvent('next_btn')
            Button:
                id: _next_btn
                text: 'Dashboard'
                disabled: False
                pos_hint: {'x': .75, 'y': .2}
                size_hint: .2, .1
                on_release: app.wfm.getWorkflow('preprocessing_wf').lookupTaskMemory('tid_load_next_wf').executeTask() #root.setEvent('next_btn')
''')

class PreprocessingScreen(WFScreen):
    source = StringProperty()
    is_dependency_complete = ObjectProperty(None)
#    is_finished = ThreadEvent()
    thread_list = []
    image_dir = None
    image_range = None
    landmark_path = None
    is_pipeline_finished = False
    #kwargs = {}
    logger = logging.getLogger('grabia_app.gbia_screens.preprocessing.preprocessing.PreprocessingScreen')
    env_dict = {}
    progress_bar = None

    def initButtonStates(self):
        self.cur_status_text = 'image dir: ' + self.image_dir + '\n' + 'image range: ' + str(self.image_range)
        self.go_btn.disabled = False
        self.next_btn.disabled = False

    def __init__(self, **kwargs):
        self.logger.debug('initializing PreprocessingScreen: '+ str(kwargs))
        self.env_dict = dict(os.environ)
        #self.kwargs = kwargs
        if kwargs.has_key('img_dir'):
            self.image_dir = kwargs.pop('img_dir')

        if kwargs.has_key('img_range'):
            self.image_range = kwargs.pop('img_range')

        if kwargs.has_key('landmark_path'):
            self.landmark_path = kwargs.pop('landmark_path')


        #import pdb; pdb.set_trace()
#        Clock.schedule_once(partial(self._do_init, **kwargs))

#    def _do_init(self, *largs, **kwargs): #, is_finished, **kwargs):
        #super(PreprocessingScreen, self).__init__(scm_ref, ['next_btn'], **self.kwargs)
        super(PreprocessingScreen, self).__init__(**kwargs)
        self.logger.debug('FFFFFFFFFFFFFFFFFFFFFFFF PreprocessingScreen initialized.')
        # if self.is_finished:
        #     self.is_finished.set()

    def getFinishedEvent(self):
        return self.is_finished

    def getRangeStr(self):
        if self.image_range:
            return str(self.image_range[0])+':'+str(self.image_range[1])
        else:
            return '0:0'

    def runPipeline(self):
        self.is_pipeline_finished = False
        pipeline_thread = threading.Thread(target=self._doRunPipeline)
        pipeline_thread.start()
        self.thread_list.append(pipeline_thread)

    # def doProcessGDBDataSingle(self, exp_data, previmg_uuid=None):
    #     self.logger.debug('entering doProcessGDBData()')
    #
    #     # create experiment graph node
    #     metadata_filepath = 'tbd'
    #     landmark_filepath = ''
    #     exp_details = ''
    #     exp_uuid = exp_data['exp_uuid']
    #     #gdb.g = gdb.loadconfig() # need to do this for process being invoked from kivy button
    #     exp_node = gdb.createExperimentNode(exp_uuid, "Timelapse microscopy", metadata_filepath, exp_details)
    #     self.logger.debug('doProcessGDBData: exp node created')
    #
    #     #refimg_uuid = None
    #     #curimg_uuid = None
    #     #previmg_uuid = None
    #     img_stats = exp_data['data']
    #     t = exp_data['seq_idx']
    #
    #     roi_stats = img_stats['roi_stats'] # roi_stats is a list of dict(polygon=roi_coordlist, ch_stats=roi_ch_statslist)
    #     img_info_list = img_stats['img_stats']  # img_info is a list of dict(roi_stats=roi_stats, img_stats=img_stats)
    #     # img_stats is a list of dict(filepath=filepath, mean=stats.mean, chname=chname, img_uuid=img_uuid, timestamp=img_timestamp)
    #     #  dict(filepath=filepath, mean=stats.mean, chname=chname, uuid=img_uuid, timestamp=img_timestamp, landmarkOffset=(xOff, yOff), img_size=(imgwidth, imgheight))
    #     bf_filepath = img_stats['bf_filepath'] # path to the bright field image
    #
    #     # initialize local variables
    #     self.cur_img_idx = t
    #
    #     refimg_uuid = None
    #
    #
    #     for c, img_info in enumerate(img_info_list):
    #         #### create image nodes in graph db
    #         offset_xy = img_info['landmarkOffset']
    #         img_size = img_info['img_size']
    #         curimg_uuid = img_info['img_uuid']
    #         ch_name = img_info['chname']
    #         timestamp = img_info['timestamp']
    #         filepath = img_info['img_filepath']
    #
    #         self.logger.debug('doProcessGDBData: checking for an existing imaging node: '+str(curimg_uuid))
    #         existing_node = gdb.getNodeByUUID(curimg_uuid)
    #         # imgwidth = curImp.getWidth()
    #         # imgheight = curImp.getHeight()
    #         if existing_node != None:
    #             self.logger.debug('doProcessGDBData: existing imaging node found: '+str(curimg_uuid))
    #             gdb.updateImageNode(existing_node, curimg_uuid, ch_name, filepath, img_size[0], img_size[1], offset_xy[0], offset_xy[1], landmark_filepath, metadata_filepath, timestamp)
    #             gdb.linkImageToExp(existing_node, exp_node) #curimg_uuid, exp_uuid)
    #         else:
    #             self.logger.debug('doProcessGDBData: creating an image node (uuid: ' + str(curimg_uuid) + ')')
    #             img_node = gdb.createImageNode(curimg_uuid, ch_name, filepath, img_size[0], img_size[1], offset_xy[0], offset_xy[1], landmark_filepath, metadata_filepath, timestamp)
    #             gdb.linkImageToExp(img_node, exp_node) #curimg_uuid, exp_uuid)
    #
    #         if c == 0:
    #             refimg_uuid = curimg_uuid
    #             bfimg_node = img_node
    #         elif c != 0 and existing_node == None:
    #             gdb.linkBFImageToFluoImage(bfimg_node, img_node) #refimg_uuid, curimg_uuid)
    #             # end of if
    #     # end of for
    #
    #     self.logger.debug('doProcessGDBData: creating image blobs imguuid: ' + str(refimg_uuid))
    #
    #     # catalogue image features in graph db
    #     self.createImageBlobs(refimg_uuid, img_info_list, roi_stats, offset_xy[0], offset_xy[1], timestamp)
    #     self.logger.debug('doProcessGDBData: processing intra frame feature relations imguuid: ' + str(refimg_uuid))
    #     self.processIntraFrameFeatureRelations(refimg_uuid, 30) #maxClusteringDist)
    #
    #     # correspondence analysis between images at time t and t+1
    #     if previmg_uuid != None and refimg_uuid != None:
    #         self.logger.debug('doProcessGDBData: processing inter frame feature relations imguuid: ' + str(refimg_uuid))
    #         self.processInterFrameFeatureRelations(previmg_uuid, refimg_uuid)
    #
    #     return refimg_uuid
    # # end of def
    #
    # def doProcessGDBData(self, exp_data):
    #     self.logger.debug('entering doProcessGDBData()')
    #
    #     # create experiment graph node
    #     metadata_filepath = 'tbd'
    #     landmark_filepath = ''
    #     exp_details = ''
    #     exp_uuid = exp_data['exp_uuid']
    #     #gdb.g = gdb.loadconfig() # need to do this for process being invoked from kivy button
    #     exp_node = gdb.createExperimentNode(exp_uuid, "Timelapse microscopy", metadata_filepath, exp_details)
    #     self.logger.debug('doProcessGDBData: exp node created')
    #
    #     #refimg_uuid = None
    #     #curimg_uuid = None
    #     #previmg_uuid = None
    #     img_stats_list = exp_data['data']
    #     #import pdb; pdb.set_trace()
    #     previmg_uuid = None
    #     for t, img_stats in enumerate(img_stats_list):
    #         roi_stats = img_stats['roi_stats'] # roi_stats is a list of dict(polygon=roi_coordlist, ch_stats=roi_ch_statslist)
    #         img_info_list = img_stats['img_stats']  # img_info is a list of dict(roi_stats=roi_stats, img_stats=img_stats)
    #         # img_stats is a list of dict(filepath=filepath, mean=stats.mean, chname=chname, img_uuid=img_uuid, timestamp=img_timestamp)
    #         #  dict(filepath=filepath, mean=stats.mean, chname=chname, uuid=img_uuid, timestamp=img_timestamp, landmarkOffset=(xOff, yOff), img_size=(imgwidth, imgheight))
    #         bf_filepath = img_stats['bf_filepath'] # path to the bright field image
    #
    #         # initialize local variables
    #         self.cur_img_idx = t
    #
    #         refimg_uuid = None
    #
    #         for c, img_info in enumerate(img_info_list):
    #             #### create image nodes in graph db
    #             offset_xy = img_info['landmarkOffset']
    #             img_size = img_info['img_size']
    #             curimg_uuid = img_info['img_uuid']
    #             ch_name = img_info['chname']
    #             timestamp = img_info['timestamp']
    #             filepath = img_info['img_filepath']
    #
    #             self.logger.debug('doProcessGDBData: checking for an existing imaging node: '+str(curimg_uuid))
    #             existing_node = gdb.getNodeByUUID(curimg_uuid)
    #             # imgwidth = curImp.getWidth()
    #             # imgheight = curImp.getHeight()
    #             if existing_node != None:
    #                 self.logger.debug('doProcessGDBData: existing imaging node found: '+str(curimg_uuid))
    #                 gdb.updateImageNode(existing_node, curimg_uuid, ch_name, filepath, img_size[0], img_size[1], offset_xy[0], offset_xy[1], landmark_filepath, metadata_filepath, timestamp)
    #                 gdb.linkImageToExp(existing_node, exp_node) #curimg_uuid, exp_uuid)
    #             else:
    #                 self.logger.debug('doProcessGDBData: creating an image node (uuid: ' + str(curimg_uuid) + ')')
    #                 img_node = gdb.createImageNode(curimg_uuid, ch_name, filepath, img_size[0], img_size[1], offset_xy[0], offset_xy[1], landmark_filepath, metadata_filepath, timestamp)
    #                 gdb.linkImageToExp(img_node, exp_node) #curimg_uuid, exp_uuid)
    #
    #             if c == 0:
    #                 refimg_uuid = curimg_uuid
    #                 bfimg_node = img_node
    #             elif c != 0 and existing_node == None:
    #                 gdb.linkBFImageToFluoImage(bfimg_node, img_node) #refimg_uuid, curimg_uuid)
    #             # end of if
    #         # end of for
    #
    #         self.logger.debug('doProcessGDBData: creating image blobs imguuid: ' + str(refimg_uuid))
    #
    #         # catalogue image features in graph db
    #         self.createImageBlobs(refimg_uuid, img_info_list, roi_stats, offset_xy[0], offset_xy[1], timestamp)
    #         self.logger.debug('doProcessGDBData: processing intra frame feature relations imguuid: ' + str(refimg_uuid))
    #         self.processIntraFrameFeatureRelations(refimg_uuid, 30) #maxClusteringDist)
    #
    #         # correspondence analysis between images at time t and t+1
    #         if previmg_uuid != None and refimg_uuid != None:
    #             self.logger.debug('doProcessGDBData: processing inter frame feature relations imguuid: ' + str(refimg_uuid))
    #             self.processInterFrameFeatureRelations(previmg_uuid, refimg_uuid)
    #
    #         if refimg_uuid != None:
    #             previmg_uuid = refimg_uuid
    #     # end of for
    # # end of def
    #
    # def processInterFrameFeatureRelations(self, imgt1_uuid, imgt2_uuid):
    #     # set temporal correspondence relationships between cluster at time t1 to those at time t2
    #     gbiaclist1, gbiaclist2 = setClusterCorrespondencesByUUID(gdb, imgt1_uuid, imgt2_uuid) #expId, imgNodeAt_t1.timestamp)
    #     setSingleCellCorrespondencesByGBIAClusters(gbiaclist1, gbiaclist2)
    # # end of def
    #
    #
    # def processIntraFrameFeatureRelations(self, refimg_uuid, max_clustering_distance):
    #
    #     img_node = gdb.getNodeByUUID(refimg_uuid)
    #
    #     blobs = list(img_node.inV('belongsToImage')) # get all blobs that belong to this imgNode
    #
    #     cmpblobs = list(blobs)
    #     self.logger.debug('number of blobs to be processed: '+str(len(cmpblobs)))
    #     for blob1 in blobs:
    #         cmpblobs.remove(blob1)
    #         self.logger.debug('blob to be processed: '+str(blob1))
    #         # deserialize the blob's polygonal coordinate points
    #         #roi1Coords = zip(*geojson.loads(blob1.roi)['coordinates']) # put xCoords in idx 0 yCoords in idx 1
    #         #roi1coordlist = zip(roi1Coords[0],roi1Coords[1])
    #         #import pdb; pdb.set_trace()
    #         #roi1coordlist = json.loads(blob1.roi)
    #         #roi1 = PolygonRoi(Polygon(roi1Coords[0], roi1Coords[1], len(roi1Coords[0])), Roi.POLYGON)
    #         roi1 = Polygon(*(json.loads(blob1.roi))) #roi1coordlist)
    #         for blob2 in cmpblobs: # [blob for blob in blobs if blob != blob1]:
    #             #roi2Coords = zip(*geojson.loads(blob2.roi)['coordinates']) # put xCoords in idx 0 yCoords in idx 1
    #             #roi2coordlist = zip(roi2Coords[0],roi2Coords[1])
    #             #roi2coordlist = json.loads(blob2.roi)
    #             #roi2 = PolygonRoi(Polygon(roi2Coords[0], roi2Coords[1], len(roi2Coords[0])), Roi.POLYGON)
    #             roi2 = Polygon(*(json.loads(blob2.roi))) #roi2coordlist)
    #
    #             #blob1Rect = roi1.getBounds()
    #             #blob2Rect = roi2.getBounds()
    #             #blob1Rect = roi1.bounds
    #             #blob2Rect = roi2.bounds
    #             ###unionRect = roi1.union(roi2).bounds #blob1Rect.createUnion(blob2Rect)
    #
    #             #polygonUnionBounds = roi1.union(roi2).envelope.bounds
    #
    #             # calculate the geometrical proximity measure between two rectangles
    #             #areaProximity = (unionRect.getWidth() * unionRect.getHeight()) - (blob1Rect.getWidth() * blob1Rect.getHeight() + blob2Rect.getWidth() * blob2Rect.getHeight())
    #             #areaProximity = _getAreaFromPolygonBounds(polygonUnionBounds) - ( _getAreaFromPolygonBounds(roi1.bounds) + _getAreaFromPolygonBounds(roi2.bounds) )
    #
    #             #polygon_distance = roi1.distance(roi2) sympy returns error on complex polygons
    #             polygon_distance = roi1.centroid.distance(roi2.centroid)
    #             # print 'Area proximity: ' + str(areaProximity)
    #             #pu = [blob1Rect.getX(), blob1Rect.getY()]
    #             #pv = [blob2Rect.getX(), blob2Rect.getY()]
    #             #dist = float(math.sqrt(sum(((a-b)**2 for a,b in zip(pu,pv))))) # find the distance between points pu & pv
    #
    #             if (polygon_distance <= max_clustering_distance):
    #                 gdb.linkBlob_is_near_Blob(blob1, blob2, float(polygon_distance))
    #                 gdb.linkBlob_is_near_Blob(blob2, blob1, float(polygon_distance))
    #
    #         # end of for
    #     # end of for
    #
    #     # set cluster nodes
    #     setClusterNodesByImgUUID(gdb, refimg_uuid, max_clustering_distance)
    # # end of def
    #
    # # roi_stats is a list of dict(polygon=roi_coordlist, ch_stats=roi_ch_statslist)
    # # img_info_list is a list of dict(filepath=filepath, mean=stats.mean, chname=chname, uuid=img_uuid, timestamp=img_timestamp, landmarkOffset=(xOff, yOff), img_size=(imgwidth, imgheight))
    # def createImageBlobs(self, refimg_uuid, img_info_list, roi_stats, xoff, yoff, timestamp):
    #
    #     #self.gRoiM = roiM
    #
    #     self.logger.debug('image node uuid: ' + str(refimg_uuid))
    #
    #     fluoimg_info_list = []
    #     # gather some general stats on fluorescent images
    #     #img_stats_list = [item['mean'] for item in img_info_list]
    #     #for img_node in fluoimg_nodes:
    #     for img_info in img_info_list: #img_stats_list:
    #         #self.logger.debug('fluoImg Filepath:' + img_node.filepath)
    #         # create a graph node for image level statistics
    #         #stats = imp.getStatistics(Measurements.MEAN)
    #         marshalled_stats = pickle.dumps(img_info['mean'])#pickle.dumps(dict(mean=img_stats))
    #         chname = img_info['chname']
    #
    #         image_node = gdb.getNodeByUUID(refimg_uuid)
    #         stats_uuid = gdb.issueUUID() # generate a unique id for the stat
    #         img_stats_node = gdb.addImageStatsNode(image_node, stats_uuid, marshalled_stats, chname)
    #         fluoimg_info_list.append(dict(img_node=image_node, content_type=image_node.contentType, stats_node=img_stats_node))
    #
    #     # process all ROIs
    #     # roi_stats is a list of dict(polygon=roi_coordlist, ch_stats=roi_ch_statslist)
    #     # ch_stats is a list of dict(ch = chname_list[idx] stats = dict(mean=stats.mean, median=stats.median, area=stats.area, std_dev=stats.stdDev))
    #     for roistat in roi_stats: #roiM.getRoisAsArray():
    #         #print('DEBUG printing roi polygon:')
    #         coordlist = roistat['polygon'] #roi.getPolygon()
    #         #import pdb; pdb.set_trace()
    #         #coordlist = zip(p.xpoints, p.ypoints)
    #         statslist = roistat['ch_stats']
    #
    #         # add to graph db
    #         blob_uuid = gdb.issueUUID() # generate a unique id for the stat
    #         image_node = gdb.getNodeByUUID(refimg_uuid)
    #         #fluoimg_node = gdb.getLinkedImageNodeByUUID(refimg_uuid)
    #
    #         blob_node = gdb.createBlobNode(blob_uuid, xoff, yoff, coordlist, timestamp)
    #         gdb.linkBlobToImage(blob_node, image_node)
    #
    #         # for each channel and associated stats, add blob-stat-node
    #         for idx, chstat in enumerate(statslist):
    #             img_stats_node = fluoimg_info_list[idx]['stats_node']
    #             #import pdb; pdb.set_trace()
    #             gdb.addBlobStatsNode(img_stats_node, blob_node, chstat['stats'], chstat['ch'])
    #     # end for
    # # end of def
    def getParsedImageData(self, fromidx, toidx):
        proc_handle = subprocess.Popen(['java', '-jar', '/grabia/gbimageanalyzer/lib/jython-2.7-b2.jar', '/grabia/gbimageanalyzer/jy_gbia.py', '-r', str(fromidx)+':'+str(toidx), '-d', self.image_dir + '/', '-l', self.landmark_path], stdout=subprocess.PIPE) #, env=self.env_dict)
        while True:
            #(output, err) = proc_handle.communicate()
            poll_ret = proc_handle.poll()
            if poll_ret != None or self.scm_ref.stop.is_set():
                if poll_ret == 0:
                    (retdata, err) = proc_handle.communicate()
                    if retdata:
                        print 'this came in: ', retdata
                        retdata = json.loads(retdata)
                        return retdata
                else:
                    #proc_handle.kill() # killing threads abruptly has adverse effects
                    proc_handle.terminate() # terminate() takes care of signalling child threads to finish
                    self.logger.debug('Preprocessing pipeline prematurely finished! User stopped the application.')
                    return None
                break
            time.sleep(1)

    @mainthread
    def _setupProgressBar(self, max_idx, *args):
        self.anim_box.clear_widgets()
        self.progress_bar = ProgressBar(max=max_idx)
        self.progress_bar.value = 1
        self.anim_box.add_widget(self.progress_bar)

    @mainthread
    def updateProgressVal(self, val):
        self.logger.debug('progress val update: '+str(val))
        if self.progress_bar:
            self.progress_bar.value = val
        else:
            self.logger.debug('progress_bar garbage collected!')



    def _doRunPipeline(self):
        try:
            self.logger.debug('Preprocessing call, jython jy_gbia.py')
            #gdb.g = gdb.loadconfig() # for some reason importing gdb doesn't initialize gdb.g, if a gdb method is invoked from Kivy language
            #gdb.clearGDB() # clear the db
            #celery_client_task.doClearGDBData()

            Clock.schedule_once(partial(self._setupProgressBar, self.image_range[1]+2), 0)

            previmg_uuid = None
            celery_client_task.doClearGDBData()
            exp_uuid = str(uuid4()) # try to believe in the claim that the uuid space is larger than the number of atoms in the universe...

            self.cur_status_text = 'You may go grab a coffee while the analysis is being done...'
            exp_node = celery_client_task.createExperiment(exp_uuid, 'Timelapse microscopy', '', '')
            self.logger.debug('Current exp_node(' + str(exp_uuid) + ') is: '+str(exp_node))
            for idx in range(self.image_range[0], self.image_range[1]+1):
                retdata = self.getParsedImageData(idx, idx+1)

                if retdata.__class__ is dict:
                    self.updateProgressVal(idx+1)
                    previmg_uuid = celery_client_task.doProcessGDBDataSingle(exp_uuid, retdata, previmg_uuid)
                else:
                    print 'something went wrong with retdata: ', retdata
                print 'processed imguuid: ', previmg_uuid

            self.updateProgressVal(self.image_range[1]+2)

            self.is_pipeline_finished = True
            #retdata = json.loads(retdata)
            #self.logger.debug('Preprocessing call to jython jy_gbia.py finished with result: ' + str(retdata))
            self.logger.debug('Start processing gdb data from the single cell ROIs')

            #self.doProcessGDBData(retdata)

            celery_client_task.saveGDBIntoFile(exp_uuid)

            # restore the buttons
            #self.go_btn.disabled = False
            self.next_btn.disabled = False
            self.cur_status_text = 'Congrats! The analysis is now completed.\nYou can go to the dashboard to visualise the results.'

            self.logger.debug('Preprocessing pipeline finished!')
        except Exception as inst:
            self.logger.debug(str(inst))
            self.cur_status_text = 'Ooops, this is embarrassing...\n' + \
                                   'The backend database has gone wrong while serving your analysis request.\n' + \
                                   'Please try again or restart the application.'
            self.next_btn.disabled = False
            self.go_btn.disabled = False

    def checkDependencies(self):
        print 'executed checkDependencies'
        pass

    def isDependencyComplete(self):
        print 'isDependencyComplete?'
        return self.is_dependency_complete

    def setupBackend(self):
        print 'executed setupBackend'
        pass

    def isSetupComplete(self):
        print 'executed isSetupComplete'
        pass

    def doReturn(self):
        print 'executed doReturn'
        pass
