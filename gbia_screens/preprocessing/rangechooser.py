__author__='spark'

from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty

from gbia_screens.base.filebrowser import FileBrowser
from gbia_screens.manager import WFScreen

from kivy.clock import Clock
from threading import Event as ThreadEvent
from functools import partial

Builder.load_string('''
<ImageRangeChooser>:
    clear_list: [popup_demo]
    folder_path_label: _folder_path_label
    cur_path: ''
    BoxLayout:
        orientation: 'vertical'
        canvas.before:
            Color:
                rgba: 1,1,0,1
            Rectangle:
                pos: root.pos
                size: root.size
        FloatLayout:
            size: root.size
            Label:
                id: _folder_path_label
                text: root.cur_path
                color: 0,0,1,1
                size_hint: .8, .1
                pos_hint: {'x': .1, 'y': .8}
                halign: 'left'
            Button:
                text: 'Select image folder'
                pos_hint: {'x': .2, 'y': .2}
                size_hint: .2, .1
                #theme: ('green', 'accent')
                on_release: popup_demo.open()
                popup_demo: popup_demo.__self__
            Button:
                text: 'Next'
                pos_hint: {'x': .6, 'y': .2}
                size_hint: .2, .1
                on_release: root.setEvent('next_btn')

            Popup:
                id: popup_demo
                title: 'Flat popup demo'
                title_color_tuple: ('Gray', '1000')
                size_hint: .9, .9
                on_parent: if self.parent: self.parent.remove_widget(self)

                FileBrowser:
                    select_string: 'Select'
                    on_success: root.updateImageFolderPath(self.filename); popup_demo.dismiss()
                    on_canceled: popup_demo.dismiss()
''')

# this screen sets the following events
# next_btn
class ImageSequenceBrowser(WFScreen):
    source = StringProperty()
    is_dependency_complete = ObjectProperty(None)
    folder_path_label = ObjectProperty(None)
#    cur_path = StringProperty()
    is_finished = None

    def __init__(self, **kwargs):
        print 'FFFFFFFFFFFFFFFFFFFFFFFF ImageSequenceBrowser initializing...'
        self.is_finished = ThreadEvent() #is_finished
        Clock.schedule_once(partial(self._do_init, **kwargs))

    def getFinishedEvent(self):
        return self.is_finished

    def _do_init(self, *largs, **kwargs): #, is_finished, **kwargs):
        #super(ImageSequenceBrowser, self).__init__(scm_ref, ['next_btn'], **self.kwargs)
        super(ImageSequenceBrowser, self).__init__(**kwargs)
        print 'FFFFFFFFFFFFFFFFFFFFFFFF ImageSequenceBrowser initialized.'
        if self.is_finished:
            self.is_finished.set()

    def updateImageFolderPath(self, path_str):
        if path_str:
            self.cur_path = path_str

    def doReturn(self):
        print 'executed doReturn'

#class ImageSequenceBrowser(FileBrowserScreen):
#
#    def __init__(self, **kargs):
#        super(ImageSequenceBrowser, self).__init__(**kargs)
#        #self.updateImageFolderPath(self.cur_path)


