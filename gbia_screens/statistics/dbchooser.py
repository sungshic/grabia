from re import findall
from kivy.uix.widget import Widget
from gbia_screens.manager import WFScreen
from kivy.lang import Builder
from kivy.adapters.models import SelectableDataItem
from kivy.uix.listview import ListView, ListItemButton
from kivy.adapters.listadapter import ListAdapter
from kivy.properties import ObjectProperty, StringProperty
# NumericProperty, ListProperty, BooleanProperty, AliasProperty, ObjectProperty, StringProperty
from kivy.clock import Clock

from functools import partial
import logging
from gbimageanalyzer import celery_client_task

Builder.load_string('''
<GDBChooserScreen>:
    layout_handle: _viewlayout
    gdb_file_handle: _gdb_file_view
    stats_btn: _stats_btn
    loadgdb_btn: _loadgdb_btn
    cur_status_text: 'hello'
    Label:
        id: _status_label
        text: root.cur_status_text
        color: 1,1,0,1
        size_hint: .8, .1
        pos_hint: {'x': .1, 'y': .9}
        halign: 'left'
    BoxLayout:
        id: _viewlayout
        orientation: 'horizontal'
        FloatLayout:
            size_hints: 1, 0.5
            GDBFileListView:
                id: _gdb_file_view
                #pos: root.pos
                #size: root.size
                pos_hint: {'x': .25, 'y': .15}
                size_hint: .5, .7
        FloatLayout:
            size_hints: 1, 0.5
            Button:
                id: _next_btn
                text: 'Dashboard'
                pos_hint: {'x': .6, 'y': .6}
                size_hint: .2, .1
                on_release: app.wfm.getWorkflow('dashboard_wf').lookupTaskMemory('tid_load_dashboard_scr').executeTask()
            Button:
                id: _loadgdb_btn
                text: 'Load GDB'
                pos_hint: {'x': .3, 'y': .6}
                size_hint: .2, .1
                on_release: root.loadGDB()
            Button:
                id: _stats_btn
                disabled: True
                text: 'Get Stats'
                pos_hint: {'x': .3, 'y': .4}
                size_hint: .2, .1
                on_release: app.wfm.getWorkflow('dashboard_wf').lookupTaskMemory('tid_run_stats_wf').executeTask()

<GDBFileListView>:
    filelist_view: _filelist_view
    canvas.before:
        Color:
            rgba: 0.5,0.7,1,0.5
        Rectangle:
            pos: root.pos
            size: root.size
    GridLayout:
        id: _filelist_view
        cols: 1
        size: root.size
        pos: root.pos
        #size_hint: 0.5, 1.0
''')

class GDBFileListItem(SelectableDataItem):
    name = None
    filepath = None
    filename = None
    def __init__(self, exp_uuid, name, filepath, filedate, summary, is_selected, **kwargs):
        #super(GBIAImageListItem, self).__init__(is_selected=is_selected, **kwargs)
        self.exp_uuid = exp_uuid
        self.name = name
        self.filepath = filepath
        self.filedate = filedate
        self.summary = summary
        self.is_selected = is_selected

class GDBListItemButton(ListItemButton):
    root_ref = None
    is_change_from_code = False

    def __init__(self, root_ref, **kwargs):
        super(GDBListItemButton, self).__init__(**kwargs)
        self.root_ref = root_ref

    def select(self, *args):
#        if not self.is_change_from_code:
#            print 'selected 1 ', self.index
#            self.root_ref.cur_idx_pick = self.index
#        else:
#            print 'selected 2 ', self.index
#            self.root_ref.addImageToSlider(self.index)
        self.select_from_composite(*args)

class GDBFileListView(Widget):
    logger = logging.getLogger('grabia_app.gbia_screens.statistics.dbchooser.GDBFileListView')
    filelist_view = ObjectProperty()

    file_list_adapter = None
    last_gdbdir_index = []
    def __init__(self, **kwargs):
        super(GDBFileListView, self).__init__(**kwargs)
#        self.refreshGDBFiles()

    def refreshGDBFiles(self):
        self.loadGDBFiles()
        if self.isDIRChanged():
            self.logger.debug('There are GDB files from previous runs...')


    def isDIRChanged(self):
        annotated_flist = self.getGDBFileList()
        cur_gdbdir_index = zip(*[item.values() for item in annotated_flist])[0] # get the list of mtimes
        index_diff = list(set(cur_gdbdir_index) - set(self.last_gdbdir_index))
        self.last_gdbdir_index = cur_gdbdir_index

        if index_diff:
            return True
        else:
            return False

    def getGDBFileList(self):
#        gdb_file_list = [f for f in listdir(self._GDB_DIR) if isfile(join(self._GDB_DIR, f)) and re.match(r'.*\.json', f)]
#         gdb_filepath_list = filter(isfile, glob(self._GDB_DIR_ + '*.json'))
#         gdb_filepath_list.sort(key=getmtime, reverse=True) # sort the list by modification time
        gdb_fileinfo_list = celery_client_task.getGDBFiles()
        #print gdb_fileinfo_list
        annotated_list = []
        for filepath, filemtime in gdb_fileinfo_list:
            #filedate = ctime(getmtime(filepath)) # get file modification date and time
            #filemtime = getmtime(filepath)
            filedate = ''
            summary = ''
            name = findall('^.*/(.*)_(.*_.*).json$', filepath)
            print filepath, name
            if name:
                annotated_list.append({'exp_uuid':name[0][0], 'name':name[0][1], 'filepath':filepath, 'filemtime':filemtime, 'filedate':filedate, 'summary':summary})

        return annotated_list

    def clearFileListWidgets(self):
        subwidgets = self.filelist_view.walk(restrict=True)
        for w in list(subwidgets)[1:]:
            self.filelist_view.remove_widget(w)

    def loadGDBFiles(self):
        #ilm = ImageListManager(self.cur_path)
        #img_path_list = ilm.getImagePathList(0,0,0)

        gdb_file_list = self.getGDBFileList()

        list_item_args_converter = \
                lambda row_index, selectable: {'text': str(selectable.name),
                                               'exp_uuid': str(selectable.exp_uuid),
                                               'filepath': str(selectable.filepath),
                                               'size_hint_y': None,
                                               'deselected_color': [0.6,0.6,0.8,1.0],
                                               'selected_color': [0.7,0.4,0.6,1.0],
                                               'height': 25}

        print gdb_file_list
        selectable_file_list = [GDBFileListItem(gdbfile['exp_uuid'], gdbfile['name'], gdbfile['filepath'], gdbfile['filedate'], gdbfile['summary'], False) \
                                for gdbfile in gdb_file_list]

        print selectable_file_list
        self.file_list_adapter = ListAdapter(data=selectable_file_list,
                                       args_converter=list_item_args_converter,
                                       selection_mode='single', #'multiple',
                                       allow_empty_selection=False,
                                       cls=partial(GDBListItemButton, self))

        # building an image list
        file_list_view = ListView(adapter=self.file_list_adapter, size_hint=(.2,0.5))

        self.clearFileListWidgets()
        self.filelist_view.add_widget(file_list_view)

        btn_obj = self.file_list_adapter.get_view(0)
#        if not btn_obj.is_selected:
        #btn_obj.is_change_from_code = True
#        self.img_list_adapter.select_item_view(btn_obj)
        btn_obj.select()
        #btn_obj.is_change_from_code = False
#        self.img_detailed_view.img_slider.addImage(0, btn_obj.img_path)
        #self.addImageToSlider(0)
#        select_idx = self.img_list_adapter.selection[0].index
#        self.img_detailed_view.img_path = self.img_list_adapter.data[select_idx].img_path #selection[0].img_path
        #detailed_view = ImageDetailedView(img_list_adapter.selection[0].img_path, cols=1, size_hint=(.5, 1.0))
        self.file_list_adapter.bind(on_selection_change=self.processChangedSelection)

        # building a rangle tuple
        #range_label_tuple = [ListItemLabel(), ListItemLabel()]
        #range_tuple_adapter = ListAdapter(data=range_label_tuple,
        #                                  args_converter=list_lab)
        #range_tuple_view = ListView(adapter=range_tuple_adapter)

    def processChangedSelection(self, list_adapter, *args):
        ##self.img_detailed_view.img_changed(list_adapter, *args)
        Clock.schedule_once(partial(self._processChangedSelection, list_adapter, *args))

    def _processChangedSelection(self, list_adapter, *args):
        print 'hello world'


class GDBChooserScreen(WFScreen):
    logger = logging.getLogger('grabia_app.gbia_screens.statistics.dbchooser.GDBChooserScreen')
    gdb_file_handle = ObjectProperty(None)
    cur_status_text = StringProperty()
    gdb_graph = None
    exp_uuid = None

    def __init__(self, **kwargs):
        self.logger.debug('GDBChooserScreen initializing...')
        #import pdb; pdb.set_trace()
        #Clock.schedule_once(partial(self._do_init, **kwargs))

        #self.gdb_graph = gdb.loadconfig()
#    def _do_init(self, *largs, **kwargs): #, is_finished, **kwargs):
        #super(ImageSequenceBrowser, self).__init__(scm_ref, ['next_btn'], **self.kwargs)
        super(GDBChooserScreen, self).__init__(**kwargs)
        if self.is_finished:
            # self.is_finished.set()
            if self.is_finished.is_set():
                self.logger.debug('GDBChooserScreen initialized.'+str(self.gdb_file_handle))
            else:
                self.logger.debug('GDBChooserScreen hasn\'t been initialized yet')
        else:
            self.logger.debug('GDBChooserScreen cannot be initialized')

    def doRefreshGDBFiles(self):
        if self.isGDBFile():
            self.cur_status_text = 'Please pick an analysis result, load its data, and get stats.'
            self.gdb_file_handle.refreshGDBFiles()
            self.loadgdb_btn.disabled = False
        else:
            self.cur_status_text = 'There is currently no result to be visualised.\nPlease go back and run some analysis first.'
            self.loadgdb_btn.disabled = True

    def isGDBFile(self):
        gdb_files = celery_client_task.getGDBFiles()
        if gdb_files:
            return True
        else:
            return False

    def loadGDB(self):
        cur_selection = self.gdb_file_handle.file_list_adapter.selection[0]
        cur_file = self.gdb_file_handle.file_list_adapter.data[cur_selection.index]
        self.logger.debug('Selected GDB file: ' + cur_file.filepath)
        celery_client_task.loadFileToGDB(cur_file.filepath)
        self.exp_uuid = cur_file.exp_uuid
        self.stats_btn.disabled = False

    def getExpUUID(self):
        return self.exp_uuid


