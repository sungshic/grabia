from kivy.app import App

from os import listdir
from os.path import expanduser, isfile, getmtime
from time import ctime
from re import findall
from glob import glob
from kivy.uix.scrollview import ScrollView
from kivy.uix.widget import Widget
from gbia_screens.manager import WFScreen
from kivy.lang import Builder
from kivy.adapters.models import SelectableDataItem
from kivy.uix.listview import ListView, ListItemButton, CompositeListItem
from kivy.adapters.listadapter import ListAdapter
from kivy.adapters.dictadapter import DictAdapter
from kivy.uix.gridlayout import GridLayout
from kivy.properties import NumericProperty, ListProperty, \
        BooleanProperty, AliasProperty, ObjectProperty, StringProperty
from kivy.uix.image import Image, AsyncImage

#from gbia_screens.base.filebrowser import FileBrowser
#from gbia_screens.manager import WFScreen

from kivy.clock import Clock
from threading import Event as ThreadEvent
from functools import partial

#from gbimageanalyzer.core.gbia import GBIASingleCellStats
from gbia_screens.base.mem_mpl_backend import GBIAMemoryImage

from functools import partial
import logging
from matplotlib.ticker import NullFormatter
import matplotlib.pyplot as plt
import numpy as np
import pickle
from gbimageanalyzer import celery_client_task

Builder.load_string('''
<GDBGenChooserScreen>:
    layout_handle: _viewlayout
    gdb_gen_handle: _gdb_gen_view
    scatterplot: _popup_scatterplot
    mpl_fig: _mpl_fig
    BoxLayout:
        id: _viewlayout
        orientation: 'horizontal'
        FloatLayout:
            size_hints: 0.5, 1
            Label:
                text: root.exp_uuid
                pos_hint: {'x': .25, 'y': .90}
                size_hint: .7, .1
            GDBGenListView:
                id: _gdb_gen_view
                #pos: root.pos
                #size: root.size
                pos_hint: {'x': .25, 'y': .15}
                size_hint: .5, .7
        FloatLayout:
            size_hints: 0.5, 1
            Button:
                id: _next_btn
                text: 'Go back'
                pos_hint: {'x': .6, 'y': .6}
                size_hint: .2, .1
                on_release: app.wfm.getWorkflow('stats_wf').lookupTaskMemory('tid_load_statsmain_scr').executeTask()
            Button:
                id: _loadgdb_btn
                text: 'Scatter plot'
                pos_hint: {'x': .3, 'y': .6}
                size_hint: .2, .1
                on_release: root.loadGDBGenScatter()
                popup_ref: _popup_scatterplot.__self__
            Popup:
                id: _popup_scatterplot
                title: 'Scatter plot: '
                title_color_tuple: ('Gray', '1000')
                size_hint: .8, .8
                on_parent: if self.parent: self.parent.remove_widget(self)
                GBIAMemoryImage:
                    id: _mpl_fig
                    pos: root.pos
                    size: root.size

<GDBGenListView>:
    genlist_view: _genlist_view
    canvas.before:
        Color:
            rgba: 0.5,0.7,1,0.5
        Rectangle:
            pos: root.pos
            size: root.size
    GridLayout:
        id: _genlist_view
        cols: 1
        pos: root.pos
        size: root.size
''')

class GDBGenListItem(SelectableDataItem):
    name = None
    filepath = None
    filename = None
    def __init__(self, gen_no, sample_size, summary, is_selected, **kwargs):
        #super(GBIAImageListItem, self).__init__(is_selected=is_selected, **kwargs)
        self.gen_no = gen_no
        self.sample_size = sample_size
        self.summary = summary
        self.is_selected = is_selected

class GDBListItemButton(ListItemButton):
    root_ref = None
    is_change_from_code = False

    def __init__(self, root_ref, **kwargs):
        super(GDBListItemButton, self).__init__(**kwargs)
        self.root_ref = root_ref

    def select(self, *args):
#        if not self.is_change_from_code:
#            print 'selected 1 ', self.index
#            self.root_ref.cur_idx_pick = self.index
#        else:
#            print 'selected 2 ', self.index
#            self.root_ref.addImageToSlider(self.index)
        self.select_from_composite(*args)

class GDBGenListView(Widget):
    #_GDB_DIR_ = expanduser("~") + '/.grabia/gdb/'
    logger = logging.getLogger('grabia_app.gbia_screens.statistics.genchooser.GDBGenListView')
    genlist_view = ObjectProperty()
    mpl_fig = ObjectProperty(None)

    gen_list_adapter = None
    last_gen_index = []
    sc_stat_handle = None

    def __init__(self, **kwargs):
        super(GDBGenListView, self).__init__(**kwargs)
#        self.refreshGDBFiles()

    def refreshSingleCellStats(self):
        self.sc_stat_handle = GBIASingleCellStats()
        self.loadGDBGenerations()
        #self.loadGDBFiles()
        #if self.isDIRChanged():
        #    self.logger.debug('There are GDB files from previous runs...')


#    def isDIRChanged(self):
#        annotated_flist = self.getGDBGenList()
#        cur_gen_index = zip(*[item.values() for item in annotated_flist])[0] # get the list of mtimes
#        #index_diff = list(set(cur_gdbdir_index) - set(self.last_gdbdir_index))
#        self.last_gen_index = cur_gen_index
#
#        if index_diff:
#            return True
#        else:
#            return False

##    def getGDBFileList(self):
       # gdb_file_list = [f for f in listdir(self._GDB_DIR) if isfile(join(self._GDB_DIR, f)) and re.match(r'.*\.json', f)]
       #  gdb_filepath_list = filter(isfile, glob(self._GDB_DIR_ + '*.json'))
       #  gdb_filepath_list.sort(key=getmtime, reverse=True) # sort the list by modification time
       #  print gdb_filepath_list
       #  annotated_list = []
       #  for filepath in gdb_filepath_list:
       #      #filedate = ctime(getmtime(filepath)) # get file modification date and time
       #      filemtime = getmtime(filepath)
       #      filedate = ''
       #      summary = ''
       #      name = findall('^.*_(.*).json$', filepath)
       #      print filepath, name
       #      if name:
       #          annotated_list.append({'name':name[0], 'filepath':filepath, 'filemtime':filemtime, 'filedate':filedate, 'summary':summary})
       #
       #  return annotated_list

    def clearGenListWidgets(self):
        subwidgets = self.genlist_view.walk(restrict=True)
        for w in list(subwidgets)[1:]:
            self.genlist_view.remove_widget(w)

    def loadGDBGenerations(self):
        #ilm = ImageListManager(self.cur_path)
        #img_path_list = ilm.getImagePathList(0,0,0)

        gdb_gen_list = self.sc_stat_handle._filtered_gens #getGDBFileList()

        list_item_args_converter = \
                lambda row_index, selectable: {'text': str(selectable.gen_no),
                                               'sample_size': str(selectable.sample_size),
                                               'size_hint_y': None,
                                               'deselected_color': [0.6,0.6,0.8,1.0],
                                               'selected_color': [0.7,0.4,0.6,1.0],
                                               'height': 25}

#        print gdb_gen_list
        selectable_gen_list = [GDBGenListItem(gen_no, len(gdbgen), '', False) \
                                for gen_no, gdbgen in enumerate(gdb_gen_list) if len(gdbgen) > 0]

#        print selectable_gen_list
        self.gen_list_adapter = ListAdapter(data=selectable_gen_list,
                                       args_converter=list_item_args_converter,
                                       selection_mode='single', #'multiple',
                                       allow_empty_selection=False,
                                       cls=partial(GDBListItemButton, self))

        # building an image list
        gen_list_view = ListView(adapter=self.gen_list_adapter, size_hint=(.2,0.5))

        self.clearGenListWidgets()
        self.genlist_view.add_widget(gen_list_view)

        ##btn_obj = self.gen_list_adapter.get_view(0)
#        if not btn_obj.is_selected:
        #btn_obj.is_change_from_code = True
#        self.img_list_adapter.select_item_view(btn_obj)
        ##btn_obj.select()
        #btn_obj.is_change_from_code = False
#        self.img_detailed_view.img_slider.addImage(0, btn_obj.img_path)
        #self.addImageToSlider(0)
#        select_idx = self.img_list_adapter.selection[0].index
#        self.img_detailed_view.img_path = self.img_list_adapter.data[select_idx].img_path #selection[0].img_path
        #detailed_view = ImageDetailedView(img_list_adapter.selection[0].img_path, cols=1, size_hint=(.5, 1.0))
        self.gen_list_adapter.bind(on_selection_change=self.processChangedSelection)

        # building a rangle tuple
        #range_label_tuple = [ListItemLabel(), ListItemLabel()]
        #range_tuple_adapter = ListAdapter(data=range_label_tuple,
        #                                  args_converter=list_lab)
        #range_tuple_view = ListView(adapter=range_tuple_adapter)

    def processChangedSelection(self, list_adapter, *args):
        ##self.img_detailed_view.img_changed(list_adapter, *args)
        Clock.schedule_once(partial(self._processChangedSelection, list_adapter, *args))

    def _processChangedSelection(self, list_adapter, *args):
        print 'hello world'


class GDBGenChooserScreen(WFScreen):
    logger = logging.getLogger('grabia_app.grabia_screen.statistics.genchooser.GDBGenChooserScreen')
    #gdb_file_handle = ObjectProperty()
    gdb_gen_handle = ObjectProperty(None)
    exp_uuid = StringProperty('')

    def __init__(self, **kwargs):
        self.logger.debug('GDBGenChooserScreen initializing...')
        #import pdb; pdb.set_trace()
        #Clock.schedule_once(partial(self._do_init, **kwargs))
        # if kwargs.has_key('exp_uuid'):
        #     self.exp_uuid = kwargs.pop('exp_uuid')

#    def _do_init(self, *largs, **kwargs): #, is_finished, **kwargs):
        #super(ImageSequenceBrowser, self).__init__(scm_ref, ['next_btn'], **self.kwargs)
        super(GDBGenChooserScreen, self).__init__(**kwargs)
        self.logger.debug('GDBGenChooserScreen initialized.'+str(self.gdb_gen_handle))

    def do_init(self):
        pass

    def getExpUUID(self):
        return self.exp_uuid

    def updateExpUUID(self, exp_uuid):
        if exp_uuid:
            self.exp_uuid = exp_uuid

    def doRefreshSingleCellStats(self):
        self.gdb_gen_handle.refreshSingleCellStats()

    def loadGDBGenScatter(self):
        cur_selection = self.gdb_gen_handle.gen_list_adapter.selection[0]
        cur_gen = self.gdb_gen_handle.gen_list_adapter.data[cur_selection.index]
        print 'Selected GDB generation: ', cur_gen.gen_no

        ## do something to load generation scatter
        # need to make the following constants inputtable from GUI or detect from metadata
        pix_width = 7.4
        pix_height = 7.4
        obj_mag = 100
        lens_mag = 1
        cmount_mag = 1
        binning = 1

#        self.mpl_fig.memory_data = self.fig_disp_data #root.disp_dataval
        self.mpl_fig.mpl_fig_ref = self.getAGenerationScatter(cur_gen.gen_no, pix_width, pix_height, obj_mag, lens_mag, cmount_mag, binning) #self.mpl_fig_ref
#        self.memimage = mpl_fig
#        self.layout_handle.add_widget(mpl_fig)

        #if self.memimage:
        #    self.memimage.memory_data = self.fig_disp_data##.getvalue()
        #    print 'debug: ', self.memimage._filename
        self.mpl_fig.updateMPLScreen()
        self.scatterplot.title = 'Scatter plot: ' + 'generation #' + str(cur_gen.gen_no)
        self.scatterplot.open() # this will popup open a kivy windown in window

    # a method for calculating the area of a pixel in square microns.
    # Its use is a part of microscopy image analysis.
    # Might need to be merged into a separate class if a frequent use is seen
    def calcPixelAreaInSqMicrons(self, ccd_pix_width, ccd_pix_height, obj_mag, lens_mag, cmount_mag, binning):
        pixel_width = (float(ccd_pix_width) * binning)/(obj_mag * lens_mag * cmount_mag)
        if ccd_pix_width == ccd_pix_height:
            pixel_height = pixel_width
        else:
            pixel_height = (float(ccd_pix_height) * binning)/(obj_mag * lens_mag * cmount_mag)

        pixel_area = pixel_width * pixel_height

        return pixel_area

    def getAGenerationScatter(self, gen_no, pix_width, pix_height, obj_mag, lens_mag, cmount_mag, binning):
        pixel_area = self.calcPixelAreaInSqMicrons(pix_width, pix_height, obj_mag, lens_mag, cmount_mag, binning)

        gen_dict = self.gdb_gen_handle.sc_stat_handle._filtered_gens

        x = []
        y = []
        for c in gen_dict[gen_no]:
            c_stat_v = self.gdb_gen_handle.sc_stat_handle._stats_lt[c]
            c_stat = pickle.loads(c_stat_v['stats'])
            c_size = float("{0:.2f}".format(c_stat['area']))*pixel_area # size in square microns
            c_fluo = float("{0:.2f}".format(c_stat['mean']))
            x.append(c_size)
            y.append(c_fluo)
            self.logger.debug("plot coord: size: "+str(pixel_area)+' '+str((c_size, c_fluo)))


        nullfmt = NullFormatter()       # no labels

        # definitions for the axes
        left, width = 0.1, 0.65
        bottom, height = 0.1, 0.65
        bottom_h = left_h = left + width + 0.02

        rect_scatter = [left, bottom, width, height]
        rect_histx = [left, bottom_h, width, 0.2]
        rect_histy = [left_h, bottom, 0.2, height]

        fig = plt.figure()

        axScatter = fig.add_axes(rect_scatter)
        axScatter.set_xlabel("cell size (square microns)")
        axScatter.set_ylabel("fluorescence (AU)")
        axHistx = fig.add_axes(rect_histx)
        axHisty = fig.add_axes(rect_histy)

        # no labels
        axHistx.xaxis.set_major_formatter(nullfmt)
        axHisty.yaxis.set_major_formatter(nullfmt)

        # the scatter plot:
        axScatter.scatter(x, y)

        # now determine nice limits by hand:
        xbinwidth = 1.5
        ybinwidth = 10
        #xymax = np.max([np.max(np.fabs(x)), np.max(np.fabs(y))])
        xmax = np.max(np.fabs(x))
        ymax = np.max(np.fabs(y))
        xlim = (int(xmax/xbinwidth) + 1) * xbinwidth
        ylim = (int(ymax/ybinwidth) + 1) * ybinwidth
        xlim = 40
        ylim = 255

        axScatter.set_xlim((0, xlim))
        axScatter.set_ylim((0, ylim))

        xbins = np.arange(0, xlim + xbinwidth, xbinwidth)
        ybins = np.arange(0, ylim + ybinwidth, ybinwidth)
        axHistx.hist(x, bins=xbins)
        axHisty.hist(y, bins=ybins, orientation='horizontal')

        axHistx.set_xlim(axScatter.get_xlim())
        axHisty.set_ylim(axScatter.get_ylim())

        return fig



class GBIASingleCellStats():
    _generations = None
    _cuuids_with_stats = None
    _mother_lt = None
    _lineage_lt = None
    _filtered_gens = None
    _stats_lt = None

    def __init__(self, gen_limit=0):
        try:
            self._lineage_lt = {} # initialize
            self._mother_lt = {} # initialize
            self._stats_lt = {} # initialize

            self._generations = []
            self._filtered_gens = [] # filtered generations, or a generations list formed out of cells with statistics
            gen0 = celery_client_task.getMotherGenerationCells()
            self._generations.append(gen0)

            for lineage_list in gen0:
                cuuid_list = [c['uuid'] for c in lineage_list]
                mother_cell_uuid = lineage_list[0]['uuid']
                self.initLineageLTMother(mother_cell_uuid)
                self.buildLookupTables(cuuid_list, mother_cell_uuid, 0)

            # obtain a list of uuids of cells with statistics
            self._cuuids_with_stats = [v['uuid'] for v in celery_client_task.getCellsWithStats()]

            mcells = gen0
            gen_no = 1
            while len(mcells) > 0:
                nth_gen_clist = self.getNthGenCellList(mcells)
                nth_gen_clist_clipped = []
                for lineage_list in nth_gen_clist: # for each cell group belonging to the same lineage and generation
                    cuuid_list = [c['uuid'] for c in lineage_list[1:]]  # get corresponding uuids
                    mother_cell_uuid = self._mother_lt[lineage_list[0]['uuid']] # retrieve the mother cell of the lineage
                    self.buildLookupTables(cuuid_list, mother_cell_uuid, gen_no) # build a lookup table, without the first cell
                    nth_gen_clist_clipped.append(lineage_list[1:]) # remove the first cell, belonging to the previous generation of cells
                if len(nth_gen_clist_clipped) > 0:
                    self._generations.append(nth_gen_clist_clipped)
                mcells = nth_gen_clist_clipped
                gen_no += 1 # increment generation no

            # initialize filtered generation
            self.filterGenerationsList()
            # build a lookup table for stats
            self.buildStatsLookupTable()

        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def initLineageLTMother(self, mcell_uuid):
        self._lineage_lt[mcell_uuid] = {}

    def buildLookupTables(self, cuuid_list, mcell_uuid, gen_no):
        if mcell_uuid not in self._lineage_lt:
            print 'buildMotherLT 1: this should not have happened.'
        elif str(gen_no) in self._lineage_lt[mcell_uuid]:
            #print 'buildMotherLT 2: this should not have happened.'
            self._lineage_lt[mcell_uuid][str(gen_no)].append(cuuid_list)
        else:
            # build lineage lookup table based on the current mother cell and a generation of cells in the same lineage
            self._lineage_lt[mcell_uuid][str(gen_no)] = []
            self._lineage_lt[mcell_uuid][str(gen_no)].append(cuuid_list)
        # have each constituent point to the mother cell
        for cuuid in cuuid_list:
            self._mother_lt[cuuid] = mcell_uuid

    def buildStatsLookupTable(self):
        for cur_gen in self._filtered_gens:
            cur_dict = celery_client_task.getCellStats(cur_gen)
            self._stats_lt.update(cur_dict) # merge new dict

    def filterGenerationsList(self):
        self._filtered_gens = []
        for cur_gen in self._generations:
            cur_uuid_list = [c['uuid'] for clist in cur_gen for c in clist] #self.getUUIDListFromCellList(cur_gen)
            filtered_list = list(set([c for c in cur_uuid_list if c in self._cuuids_with_stats])) # a conversion to set then back to list removes duplicate entries
            self._filtered_gens.append(filtered_list)


    def getUUIDListFromCellList(self, gen_clist):
        cuuidlist = [p[-1:][0]['uuid'] for p in gen_clist]
        return cuuidlist

    def filterNthGenWithStats(self, gen_no):
        gen_clist = self._generations[gen_no]
        nth_gen_with_stats = []
        for alist in gen_clist:
            filtered_list = [c for c in alist if c['uuid'] in self._cuuids_with_stats]
            if len(filtered_list) > 0:
                nth_gen_with_stats.append(filtered_list)
        #nth_gen_with_stats = [c for alist in gen_clist for c in alist if c['uuid'] in self._cuuids_with_stats]
        return nth_gen_with_stats

    def getNthGenCellList(self, mother_clist):
        nth_gen_clist = celery_client_task.getNextGenerationCells(self.getUUIDListFromCellList(mother_clist))
        return nth_gen_clist




