__author__='spark'

from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty
from kivy.uix.image import Image, AsyncImage
from kivy.core.image.img_pygame import ImageLoaderPygame
from gbia_screens.manager import WFScreen
from threading import Event as ThreadEvent
from kivy.clock import Clock
import subprocess
import time
from functools import partial
import threading

from gbia_screens.base.mplstat import SingleCellStatBrowser
from gbia_screens.base.mem_mpl_backend import GBIAMemoryImage
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.backend_bases import MouseEvent
import StringIO
import logging

MPL_DPI=100

Builder.load_string('''
<GBIALineagesScreen>:
    layout_handle: _layout_handle
    BoxLayout:
        orientation: 'vertical'
        BoxLayout:
            orientation: 'vertical'
            id: _layout_handle
            pos: self.pos
            size: root.size
            size_hint: 1.0, 0.9
            # GBIAMemoryImage:
            #     id: _memimage
            #     memory_data: root.fig_disp_data #root.disp_dataval
            #     mpl_fig_ref: root.mpl_fig_ref
            #     pos: self.pos
            #     size: self.size
            #     allow_stretch: True
            #     source: root.img_src
        FloatLayout:
            size_hint: 1.0, 0.1
            Label:
                pos_hint: {'x': .1, 'y': .5}
                size_hint: .5, .1
                text: root.exp_uuid
            Button:
                text: 'Return to stats menu'
                pos_hint: {'x': .55, 'y': .5}
                size_hint: .4, .1
                on_release: app.wfm.getWorkflow('stats_wf').lookupTaskMemory('tid_load_statsmain_scr').executeTask()

''')

class GBIALineagesScreen(WFScreen):
    source = StringProperty()
    mplcanvas = ObjectProperty(None)
    is_dependency_complete = ObjectProperty(None)
    img_src = StringProperty(None)
    # is_finished = ThreadEvent()
    thread_list = []
    fig_disp_data = ObjectProperty(None)
    disp_dataval = None
    cellbrowser = ObjectProperty(None)
    mpl_fig_ref = ObjectProperty(None)
    memimage = ObjectProperty(None)
    #layout_handle = ObjectProperty(None)
    is_screen_initialized = None
    exp_uuid = StringProperty('')

    logger = logging.getLogger('grabia_app.gbia_screens.statistics.lineages.GBIALineagesScreen')
    kwargs = {}

    def updateExpUUID(self, exp_uuid):
        if exp_uuid:
            self.exp_uuid = exp_uuid

    def __init__(self, **kwargs):
        self.logger.debug('GBIALineagesScreen stanby...')

        # store kwargs intended for this class for deferred do_init()
        if kwargs.has_key('is_finished'):
            self.kwargs['is_finished'] = kwargs.pop('is_finished')

        super(GBIALineagesScreen, self).__init__(**kwargs)

#        Clock.schedule_once(partial(self._do_init, **kwargs))
    def do_init(self): #, is_finished, **kwargs):
        self.logger.debug('GBIALineagesScreen do_init() called: initializing on the run...')
        #super(PreprocessingScreen, self).__init__(scm_ref, ['next_btn'], **self.kwargs)

        # if kwargs.has_key('exp_uuid'):
        #     self.exp_uuid = kwargs.pop('exp_uuid')

        self.cellbrowser = SingleCellStatBrowser(self.exp_uuid)
        self.mplcanvas = self.cellbrowser.mpl_fig_ref.canvas #FigureCanvas(self.cellbrowser.mpl_fig_ref)
        self.mpl_fig_ref = self.cellbrowser.mpl_fig_ref

        print 'STATS_MAIN SCREEEEEEEEEN INIT fig ', self.cellbrowser.mpl_fig_ref, self.cellbrowser.mpl_fig_ref.canvas._key
        # setup mpl callbacks
        #self.cellbrowser.mpl_fig_ref.canvas.mpl_connect('button_press_event', self.mpl_on_click)
        #self.cellbrowser.mpl_fig_ref.canvas.mpl_connect('button_release_event', self.mpl_on_release)
        #self.cellbrowser.mpl_fig_ref.canvas.mpl_connect('motion_notify_event', self.mpl_on_drag)
        ##self.fig_disp_data = StringIO.StringIO()
        ##self.mplcanvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
        ##print 'disp mem class: ', str(self.fig_disp_data.__class__)
        ##self.disp_dataval = self.fig_disp_data.getvalue()

        mpl_memory_img = GBIAMemoryImage()
        mpl_memory_img.id = '_memimage'
        mpl_memory_img.memory_data = self.fig_disp_data #root.disp_dataval
        mpl_memory_img.mpl_fig_ref = self.mpl_fig_ref
        self.memimage = mpl_memory_img

        #if self.memimage:
        #    self.memimage.memory_data = self.fig_disp_data##.getvalue()
        #    print 'debug: ', self.memimage._filename
        self.memimage.updateMPLScreen()

        if self.kwargs.has_key('is_finished'):
            self.is_screen_initialized = self.kwargs.pop('is_finished')
        else:
            self.is_screen_initialized = ThreadEvent()

        #super(GBIALineagesScreen, self).__init__(**(self.kwargs))

        #self.logger.debug('waiting for GBIALineagsScreen\'s super class to initiialize')
        #self.updateMPLMemImg(mpl_memory_img)
        screen_update_thread = threading.Thread(target=partial(self.updateMPLMemImg, mpl_memory_img))
        screen_update_thread.start()

        self.logger.debug('GBIALineagesScreen initialized.')

    def clearCurrentMemImg(self):
        self.layout_handle.clear_widgets()

    def updateMPLMemImg(self, mpl_memory_img):
        while not self.is_finished.wait(1):
            self.logger.debug('waiting for GBIALineagsScreen\'s super class to initiialize')
        self.clearCurrentMemImg()
        self.layout_handle.add_widget(mpl_memory_img)
        self.is_screen_initialized.set()
        #self.is_finished.set() # set the is_finished flag
        #Clock.schedule_once(partial(self._do_local_init, mpl_memory_img))

    def _do_local_init(self, mpl_memory_img, *largs):
        self.layout_handle.add_widget(mpl_memory_img)

    def mpl_on_click(self, event):
        # 'button_press_callback': whenever a mouse button is pressed
        if event.dblclick:
            print("DBLCLICK", event.x, event.y)
        else:
            print("DOWN    ", event.x, event.y)

    def mpl_on_release(self, event):
        # 'button_release_callback': whenever a mouse button is released
        print("MPL RELEASE EVENT FIRED    ", event.x, event.y)

    def mpl_on_drag(self, event):
        #pass
        # 'motion_notify_event': whenever mouse is moved
        print("MOUSE moved   ", event.x, event.y)


    def doSomething(self, task_idx):
        print 'DO SOMETHING!!!!!!!'
        if task_idx == 0:
            self.cellbrowser.doOnClickedPerspective0()
        elif task_idx == 1:
            self.cellbrowser.doOnClickedPerspective1()

        self.updateMPLScreen()


    def runPipeline(self):
        pipeline_thread = threading.Thread(target=self._doRunPipeline)
        pipeline_thread.start()
        self.thread_list.append(pipeline_thread)


    def _doRunPipeline(self):
        print 'hello world'
        proc_handle = subprocess.Popen(['java', '-jar', '/grabia/gbimageanalyzer/lib/jython-2.7-b2.jar', '/grabia/gbimageanalyzer/jy_gbia.py'], shell=False)
        while True:
            if proc_handle.poll() == 0 or self.scm_ref.stop.is_set():
                if proc_handle.poll() != 0:
                    #proc_handle.kill() # killing threads abruptly has adverse effects
                    proc_handle.terminate()
                break
            time.sleep(1)
        print 'pipeline finished!'

    # def getFinishedEvent(self):
    #     return self.is_finished

    def checkDependencies(self):
        print 'executed checkDependencies'
        pass

    def isDependencyComplete(self):
        print 'isDependencyComplete?'
        return self.is_dependency_complete

    def setupBackend(self):
        print 'executed setupBackend'
        pass

    def isSetupComplete(self):
        print 'executed isSetupComplete'
        pass

    def doReturn(self):
        print 'executed doReturn'
        pass

#    def on_motion(self, etype, motionevent):
#        super_ret = super(GBIAStatsScreen, self).on_motion(etype, motionevent)
#
#        print 'kivy on motion...'
#        self.fig_disp_data = StringIO.StringIO()
#        self.mplcanvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
#        self.memimage.memory_data = self.fig_disp_data##.getvalue()
#        mpl_event_name = 'motion_notify_event'
#        (mpl_x, mpl_y) = self.convertKivyPosToMPLPos(motionevent.pos, self.memimage)
#        mpl_mouseevent = MouseEvent(mpl_event_name, self.cellbrowser.mpl_fig_ref.canvas, x=mpl_x, y=mpl_y, key=self.cellbrowser.mpl_fig_ref.canvas._key, step=0, guiEvent=None)
#        self.cellbrowser.mpl_fig_ref.canvas.callbacks.process(mpl_event_name, mpl_mouseevent) # trigger mpl mouse event
#        self.cellbrowser.mpl_fig_ref.canvas.draw()
#        return super_ret


