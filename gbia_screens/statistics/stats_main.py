__author__='spark'

from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty
from kivy.uix.image import Image, AsyncImage
from kivy.core.image.img_pygame import ImageLoaderPygame
from gbia_screens.manager import WFScreen
from threading import Event as ThreadEvent
from kivy.clock import Clock
import subprocess
import time
from functools import partial
import threading

from gbia_screens.base.mplstat import SingleCellStatBrowser
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.backend_bases import MouseEvent
import StringIO
import logging

MPL_DPI=100

Builder.load_string('''
<GBIAStatsScreen>:
    GridLayout:
        cols: 1
        canvas.before:
            Color:
                rgba: 0.0,0.0,0.0,1.0
            Rectangle:
                pos: self.pos
                size: self.size
        FloatLayout:
            size_hint: 1.0, 0.4
            Image:
                source: 'grabia_icon.png'
                size_hint: 1.0, 1.0
                pos_hint: {'x': -.3, 'y': .0}
            Label:
                size_hint: .5, .1
                pos_hint: {'x': .5, 'y': .05}
                font_size: '48sp'
                text: "[b]stats main[/b]"
                markup: True
            Label:
                size_hint: .5, .1
                pos_hint: {'x': .5, 'y': .9}
                text: root.exp_uuid
        FloatLayout:
            size_hint: 1.0, 0.6
            canvas.before:
                Color:
                    rgba: 0.3,0.3,0.4,0.8
                Rectangle:
                    pos: self.pos
                    size: self.size
            Button:
                id: _genscatter_btn
                text: 'Generation scatter plot'
                pos_hint: {'x': .075, 'y': .2}
                size_hint: .4, .1
                on_release: app.wfm.getWorkflow('stats_wf').lookupTaskMemory('tid_load_genscatter_scr').executeTask()
            Button:
                id: _lineage_btn
                text: 'Lineage plot'
                pos_hint: {'x': .525, 'y': .2}
                size_hint: .4, .1
                on_release: app.wfm.getWorkflow('stats_wf').lookupTaskMemory('tid_load_lineages_scr').executeTask() #popup_demo.open()
                #root.setEvent('btn1') #app.wfm.getWorkflow('test_wf').executeTask()
            Button:
                id: _dashboard_btn
                text: 'Dashboard'
                pos_hint: {'x': .525, 'y': .4}
                size_hint: .4, .1
                on_release: app.wfm.getWorkflow('dashboard_wf').lookupTaskMemory('tid_load_dashboard_scr').executeTask()
                #app.wfm.getWorkflow('stats_wf').lookupTaskMemory('tid_load_lineages_scr').executeTask() #popup_demo.open()
                #root.setEvent('btn1') #app.wfm.getWorkflow('test_wf').executeTask()

''')

class GBIAStatsScreen(WFScreen):
    is_dependency_complete = ObjectProperty(None)
    thread_list = []
    exp_uuid = StringProperty('')

    logger = logging.getLogger('grabia_app.gbia_screens.statistics.stats_main.GBIAStatsScreen')
    #kwargs = {}

    def updateExpUUID(self, exp_uuid):
        if exp_uuid:
            self.exp_uuid = exp_uuid

    def __init__(self, **kwargs):
        self.logger.debug('GBIAIIIIIIIIIIIIIMMMMMMMGGGGGGGGGGGGCROPWIDGET ')
        #self.kwargs = kwargs

#        Clock.schedule_once(partial(self._do_init, **kwargs))
#    def _do_init(self, *largs, **kwargs): #, is_finished, **kwargs):
        #super(PreprocessingScreen, self).__init__(scm_ref, ['next_btn'], **self.kwargs)

        super(GBIAStatsScreen, self).__init__(**kwargs)

        self.logger.debug('GBIAStatsScreen initialized.')

    # def getFinishedEvent(self):
    #     return self.is_finished

    def do_init(self):
        pass

    def checkDependencies(self):
        print 'executed checkDependencies'
        pass

    def isDependencyComplete(self):
        print 'isDependencyComplete?'
        return self.is_dependency_complete

    def setupBackend(self):
        print 'executed setupBackend'
        pass

    def isSetupComplete(self):
        print 'executed isSetupComplete'
        pass

    def doReturn(self):
        print 'executed doReturn'
        pass

#    def on_motion(self, etype, motionevent):
#        super_ret = super(GBIAStatsScreen, self).on_motion(etype, motionevent)
#
#        print 'kivy on motion...'
#        self.fig_disp_data = StringIO.StringIO()
#        self.mplcanvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
#        self.memimage.memory_data = self.fig_disp_data##.getvalue()
#        mpl_event_name = 'motion_notify_event'
#        (mpl_x, mpl_y) = self.convertKivyPosToMPLPos(motionevent.pos, self.memimage)
#        mpl_mouseevent = MouseEvent(mpl_event_name, self.cellbrowser.mpl_fig_ref.canvas, x=mpl_x, y=mpl_y, key=self.cellbrowser.mpl_fig_ref.canvas._key, step=0, guiEvent=None)
#        self.cellbrowser.mpl_fig_ref.canvas.callbacks.process(mpl_event_name, mpl_mouseevent) # trigger mpl mouse event
#        self.cellbrowser.mpl_fig_ref.canvas.draw()
#        return super_ret



