__author__ = 'spark'

import yaml
import importlib
import string
from threading import Thread
from threading import Event as ThreadEvent
import threading
import re
import time
import logging
import functools
from kivy.event import EventDispatcher

class ThreadWithReturnValue(Thread):
    close = ThreadEvent()
    def __init__(self, group=None, target=None, name=None, args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs, Verbose)
        self._return = None

    # override
    def run(self):
        if self._Thread__target is not None:
            self._return = self._Thread__target(*self._Thread__args, **self._Thread__kwargs)
    # override
    def join(self):
        Thread.join(self)
        return self._return

class WFTask(object):
    stop = ThreadEvent()

    # constants
    SEQUENCE = 'SEQUENCE'
    THREAD_SPLIT = 'THREAD_SPLIT'
    SYNCHRONIZATION = 'SYNCHRONIZATION'
    SWITCH = 'SWITCH'

    id = None
    flow_control = None
    import_path = None
    module_ref = None
    class_name = None
    class_args = {}
    class_ref = None
    method = None
    method_args = {}
    task_list = [] # a list of WFTask instances
    task_results = []
    wf_ref = None
    task_def = None

    _wfm_ref = None
#	_wfm_lut = {}
    _super_task = None
    _local_lut = {}
    _runtime_kwargs = {}
    event_lut = {}
    logger = logging.getLogger('grabia_app.gbia_wf.wf_manager.WFTask')

    def getEvent(self, event_name):
        event_handle = self.event_lut.get(event_name)
        if event_handle:
            self.logger.debug('getEvent ' + event_name + ' ' + str(event_handle.is_set()))
        else:
            self.logger.debug('getEvent ' + event_name + ' event NOT FOUND')
        return event_handle

    def setEvent(self, event_name):
        event_handle = self.getEvent(event_name)
        if event_handle:
            self.logger.debug('setEvent ' + event_name + ' ' + str(event_handle.is_set()))
            event_handle.set()

    def clearAllEvents(self):
        for event_name, event_handle in self.event_lut.iteritems():
            self.logger.debug('clearAllEvent:' + event_name + ' ' + str(event_handle.is_set()))
            event_handle.clear()

    def clearEvent(self, event_name):
        event_handle = self.getEvent(event_name)
        if event_handle:
            self.logger.debug('clearEvent ' + event_name + ' ' + str(event_handle.is_set()))
            event_handle.clear()

    def isEventSet(self, event_name):
        event_handle = self.getEvent(event_name)
        if event_handle:
            return event_handle.is_set()
        else:
            return False

    def addEvent(self, event_name):
        if not self.event_lut.has_key(event_name):
            event_handle = ThreadEvent()
            event_handle.clear()
            self.event_lut[event_name] = event_handle
            return True
        else:
            event_handle = self.event_lut[event_name]
            self.logger.debug('addEvent ' + event_name + ' event already exists ' + str(event_handle.is_set()))
            event_handle.clear()
            return False

    def removeEvent(self, event_name):
        if self.event_lut.has_key(event_name):
            self.event_lut.pop(event_name)
            return True
        else:
            return False

    def taskLog(self, log_message):
        self.logger.debug(log_message)

    def storeRuntimeTaskArg(self, arg_name, arg_val):
        self._runtime_kwargs[arg_name] = arg_val

    def getRuntimeTaskArg(self, arg_name):
        print 'GRRRRRRRRRRRUUUUUUUUUUUUUUUUUUUUUUNTIMETASKARG ', arg_name, self._runtime_kwargs, self
        return self._runtime_kwargs.get(arg_name)

    @staticmethod
    def instantiateClass(class_name, import_path=None, class_args={}):
        try:
            print 'instantiating ', class_name, import_path, class_args
            obj_ref = None
            if import_path: # if import_path definition is valid
                cur_module_ref = importlib.import_module(import_path)
                class_import = getattr(cur_module_ref, class_name) # import the class from the namespace given by import_path
            else:
                raise Exception()
                ###class_import = globals().get(class_name) # import the class from the global namespace

            if class_import:  # if class import is valid
                obj_ref = class_import(**class_args) # instantiate the class with class arguments

            return obj_ref
        except Exception as inst:
            print 'class name: ', class_name
            print type(inst)
            print inst.args
            print inst

            raise # re-raise Exception

    # a static method to search for a memory instance with precedence
    @staticmethod
    def lookupMemory(task_ref, memory_key):
        mem_ref = None
        # first precedence to lookup for the key in the local_lut
        mem_ref = task_ref._local_lut.get(memory_key)
        #if mem_ref:
        #print 'MMMMMMMMMMMMMMMMMMMMMM local cache ', task_ref._local_lut #memory_key #, ' found in the local cache ', task_ref.id, task_ref._local_lut
        if not mem_ref and task_ref._super_task and task_ref is not task_ref.wf_ref: # not in local_lut and super_task is available
            #print 'AAAAAAAAAAAAAAAAAAAAAA still could not find, looking at the workflow namespace, ', memory_key, task_ref._super_task._local_lut
            mem_ref = task_ref._super_task.lookupTaskMemory(memory_key) # lookup in the super_task's memory
#			mem_ref = task_ref._super_task._local_lut.get(memory_key) # lookup for the key in the super_task's lut
        if not mem_ref and task_ref.wf_ref is not task_ref: # still couldn't find
        #    print 'BBBBBBBBBBBBBBBBBBBBBB still could not find, looking at the workflow namespace, ', memory_key, task_ref.wf_ref._local_lut
            mem_ref = task_ref.wf_ref.lookupTaskMemory(memory_key) # look up in the workflow's memory
        #if not mem_ref:
            #print 'CCCCCCCCCCCCCCCCCCCCCC still could not find, giving up..., ', memory_key

##		if not mem_ref: # still couldn't find
##			print 'CCCCCCCCCCCCCCCCCCCCCC still could not find, looking at the global namespace, ', memory_key
##			mem_ref = globals().get(memory_key) # look up in the global namespace

        return mem_ref

    def __init__(self, wfm, wf_ref, task_def, super_task=None, **kwargs):
        try:
            self._wfm_ref = wfm
            self.wf_ref = wf_ref
            self.task_def = task_def
            self._local_lut = {}
            self._runtime_kwargs = {}
#			self._wfm_lut = wfm._local_lut
            self._super_task = super_task

            self.id = task_def.get('id')
            self.flow_control = task_def.get('flow_control')
            self.import_path = task_def.get('import_path')
            self.class_name = task_def.get('class_name')
            self.class_ref = task_def.get('class_ref')
            self.method = task_def.get('method')


            if self.id:
                self.logger.debug('class instance initializing...: ' + str(self.id) + ' ' + str(self))
            else:
                self.logger.debug('class instance initializing w/o id...: ' + str(self))

            #if kwargs:
            #    self._runtime_kwargs = kwargs
            #if kwargs.has_key('event_list'):
            event_list = task_def.get('event_list')
            if not event_list:
                event_list = []

            self._local_lut['self'] = self
            if self.id:
                self._local_lut[self.id] = self # add self to the local lut memory, so any subtasks can refer to it
                if self.id not in self.wf_ref.restricted_tasks: # if the current id is not declared to have restricted access in the workflow
                    self.wf_ref.addToLookupTable(self.id, self) # add it to the workflow-wide access memory

            self.task_list = []

            #print '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'
            #print task_def.keys()
            self.initializeWFTask(wfm, task_def, super_task, event_list)
            self.logger.debug('class instance initialized: ' + str(self.id) + ' ' + str(self))

        except Exception as inst:
            print type(inst)
            print inst.args
            print inst

            raise # re-raise Exception

#	def parseFuncString(self, obj_ref, func_string):
#		parsed_str = re.findall('^(.*)\((.*)\)(.*)$')
#
#		if parsed_str:
#			func_name = parsed_str[0][0]
#			func_args = parsed_str[0][1]
#			result_op = parsed_str[0][2] # string for an operator on the result of the function call
#			func_handle = getattr(obj_ref, func_name)
#			functools.partial(func_handle, )

    # a dynamic version of the above static method
#	def lookupTaskMemory(self, memory_key):
#		parsed_keys = string.split(memory_key, '.')

#		print 'keys: ', parsed_keys
#		memory_val = WFTask.lookupMemory(self, parsed_keys[0])
#		for key in parsed_keys[1:]: # for any subsequent keys
            #print 'memory_val: ', memory_val
            #print 'memory_key: ', memory_key
            #print 'parsed keys: ', parsed_keys
#			print 'memory_val: ', memory_val
#			print 'key: ', key
#			memory_val = getattr(memory_val, key) # traverse down the memory hierarchy
            #print 'return val: ', return_val

#		return_val = memory_val

#		return return_val

    # make a namespace dict
    def formatNamespaceDict(self, var_dict_list):
        #namespace_dict = dict([(k.keys()[0], locals().get(k.values()[0], None)) for k in var_dict_list])
        namespace_dict = dict([k.popitem() for k in var_dict_list])

        return namespace_dict

    # eval function with limited namespace
    def evalPythonString(self, namespace_var, namespace_name, python_code):
        #print 'namespace var: ', namespace_var
        namespace_dict = self.formatNamespaceDict([{namespace_name:namespace_var}])
        #print 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZ eval string: ', python_code, ' namespace: ', namespace_dict
        #if namespace_name == 'tid_workflow_main':
        #	print '########$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ', namespace_var.wfm_root
        value = eval(python_code, {"__builtins__": None}, namespace_dict)

#        if namespace_name == 'tid_imagechooser_screen':
#            import pdb
#            pdb.set_trace()
        #print 'EVALLLLLLLLLLLLLLLLL_PYTHON_STRING: ', value, namespace_dict, python_code
        return value


    # a dynamic version of the above static method
    def lookupTaskMemory(self, memory_key):
        self.logger.debug('lookupTaskMemory: (' + str(memory_key) + ')')
        parsed_keys = string.split(memory_key, '.', 1)

        #print 'keys: ', parsed_keys
        memory_val = WFTask.lookupMemory(self, parsed_keys[0])

        #if parsed_keys[0] == 'tid_imagechooser_screen':
        #    print 'LLLLLLLLLLLLLLLLLLLLLLLL_LOOKUPTASKMEMORY', memory_key, memory_val

        tail_str = parsed_keys[1:]
        if tail_str: # if trailing string exists
            #print 'TAILLLED'
            return_val = self.evalPythonString(memory_val, parsed_keys[0], memory_key)
        else:
            #print 'NOTAILLLL'
            return_val = memory_val

        #if parsed_keys[0] == 'tid_imagechooser_screen':
        #    print 'LLLLLLLLLLLLLLLLLLLLLLLL_LOOKUPTASKMEMORY', memory_key, memory_val, return_val

        self.logger.debug('lookupTaskMemory: (' + memory_key + ',' + str(memory_val) + ') resolved to ' +
                          str(return_val)) #self.__class__) + str(self))
        if not return_val:
            self.logger.debug('lookupTaskMemory: (' + str(memory_key) + ') not found in the cache!')
        return return_val

    def doParseArgStr(self, arg_str):
        meta_arg = re.findall('^(?:meta|META)\((.*)\)(.*)$', arg_str)
        ret_val = None
        if meta_arg:
            arg_str = meta_arg[0][0]
            ret_val = self.lookupTaskMemory(arg_str)
            if meta_arg[0][1]:
                ret_val += meta_arg[0][1] # add any trailing string outside meta()
        if not ret_val:
            ret_val = arg_str
        return ret_val

    def doParseArgVal(self, arg_val):
        ret_val = None
        if arg_val.__class__ ==  list:
            parsed_list = []
            for item in arg_val:
                if item.__class__ == list:
                    parsed_item = self.doParseArgVal(item)
                elif item.__class__ == str:
                    parsed_item = self.doParseArgStr(item)
                parsed_list.append(parsed_item)
            ret_val = parsed_list
        elif arg_val.__class__ == str:
            ret_val = self.doParseArgStr(arg_val)
        return ret_val


    def parseClassArgs(self, args):

        ret_args_list = {}
        arg_key_list = args.keys()
        arg_val_list = args.values()
        print 'PPPPPPPPPPPPPPPPPPPPPPPARSECLASSARGS: ', args
        for arg_idx, arg_val in enumerate(arg_val_list):
            print 'PPPPPPPPPPPPPPPPPPPPPPPARSECLASSARGS VAL: ', arg_val
            ret_val = self.doParseArgVal(arg_val)
            ret_args_list[arg_key_list[arg_idx]] = ret_val

        return ret_args_list

    def parseRuntimeMethodArgs(self, args):

        ret_args_list = {}
        arg_key_list = args.keys()
        arg_val_list = args.values()
        #print arg_key_list
        #print arg_val_list
        for arg_idx, arg_val in enumerate(arg_val_list):
            ret_val = None
            if arg_val.__class__ == str:
                print 'RUUUUUUUUUUNNTIMEMETHODARRRRRRRRRRRRRRG... ', arg_val
                meta_arg = re.findall('^(?:runtime|RUNTIME)\((.*)\)(.*)$', arg_val)
                if meta_arg:
                    arg_val = meta_arg[0][0]
                    ret_val = self.lookupTaskMemory(arg_val)
                    if meta_arg[0][1]:
                        ret_val += meta_arg[0][1]
                    print 'RUUUUUUUUUUNNTIMEMETHODARRRRRRRRRRRRRRG... ', ret_val
                if not ret_val:
                    ret_val = arg_val
            else:
                ret_val = arg_val
            ret_args_list[arg_key_list[arg_idx]] = ret_val

        return ret_args_list


    def parseMethodArgs(self, args):

        ret_args_list = {}
        arg_key_list = args.keys()
        arg_val_list = args.values()
        #print arg_key_list
        #print arg_val_list
        for arg_idx, arg_val in enumerate(arg_val_list):
            ret_val = None
            ret_val = self.doParseArgVal(arg_val)
#            if arg_val.__class__ == str:
#                meta_arg = re.findall('^(?:meta|META)\((.*)\)$', arg_val)
#                if meta_arg:
#                    arg_val = meta_arg[0]
#                    ret_val = self.lookupTaskMemory(arg_val)
#                if not ret_val:
#                    ret_val = arg_val
#            else:
#                ret_val = arg_val
            ret_args_list[arg_key_list[arg_idx]] = ret_val

        return ret_args_list


    def initializeWFTask(self, wfm, task_def, super_task=None, event_list=[]):
        #cref = cls(wfm, task_def, super_task)
        self._wfm_ref = wfm
#		self._wfm_lut = wfm._local_lut
        self.id = task_def.get('id')

        self.event_lut = {} # initialize events
        for e_name in event_list:
            self.addEvent(e_name)

        self.flow_control = task_def.get('flow_control')
        self.import_path = task_def.get('import_path')
        if self.import_path:
            self.module_ref = importlib.import_module(self.import_path)

        self.class_name = task_def.get('class')
        self.class_args = task_def.get('class_args')
        if self.class_args:
            parsed_class_args = self.parseClassArgs(self.class_args)
        else:
            parsed_class_args = {}

        method_name = task_def.get('method')
        class_ref_id = task_def.get('class_ref')

        #if self.id:
        #    self.wf_ref._local_lut[self.id] = self # add self to the workflow's lut memory, so any tasks can refer to it by task id

        # conditional block to handle different config scenarios
        if self.class_name and not class_ref_id and not method_name:
            #print '################# initializing class: ', self.class_name
            #print '################# class args: ', self.class_args
            #print 'class instantiation: ', self.class_name
            #self.class_name = task_def.get('class')
            self.logger.debug('instantiating a class: (' + self.class_name + ',' + str(self.class_args) + ')' + str(self.__class__) + str(self))
            print 'CCCCCCCCCCCCCCCCCCCCCCCCLASSSSSSSSSSSS 0', self.class_name #, self.module_ref
            self.class_ref = WFTask.instantiateClass(self.class_name, import_path=self.import_path, class_args=parsed_class_args)
#            parsed_class_args = self.parseRuntimeMethodArgs(self.class_args)
#            self.class_ref = WFTask.instantiateClass(self.class_name, import_path=self.import_path, class_args=parsed_class_args)
            #print 'class instance ref: ', self.class_ref
            method_name = task_def.get('method')
            if method_name:
                self.method = getattr(self.class_ref, method_name) # store the reference to the method of the class instance
                method_args = task_def.get('method_args')
                if method_args:
                    self.method_args = self.parseMethodArgs(method_args)
                else:
                    self.method_args = {}
        elif class_ref_id: # no new class to be instantiated anew, and there is an instance ref available
            print 'class reference: ', class_ref_id, self.wf_ref.wf_name
            self.class_ref = self.lookupTaskMemory(class_ref_id) # look up for and store the instance ref from the available memory spaces
            #print 'class instance ref: ', self.class_ref
            if method_name:
                self.method = getattr(self.class_ref, method_name) # store the instance's method reference
                method_args = task_def.get('method_args')
                self.logger.debug('an existing class inst and method referenced: (' + str(class_ref_id) + ',' + str(method_name) + str(method_args) + ')' + str(self.__class__) + str(self))
                if method_args:
                    self.method_args = self.parseMethodArgs(method_args)
                else:
                    self.method_args = {}
        elif self.class_name and method_name: # if a static method is given (i.e. no class instantiation needed)
            #print 'static class method reference: ', self.class_name, '.', method_name
            if self.module_ref: # if class_path is valid
                print 'CCCCCCCCCCCCCCCCCCCCCCCCLASSSSSSSSSSSS 1', self.class_name, self.module_ref
                self.logger.debug('a static class method referenced: (' + str(self.class_name) + ',' + str(method_name) + str(method_args) + ')' + str(self.__class__) + str(self))

                self.class_ref = getattr(self.module_ref, self.class_name) # class reference, not class instance reference, is stored
                self.method = getattr(self.class_ref, method_name) # together with the reference to its static method
            else:
                print 'CCCCCCCCCCCCCCCCCCCCCCCCLASSSSSSSSSSSS 2', self.class_name
                raise Exception()
                ##self.method = getattr(globals().get(self.class_name), method_name) # refer to a class reference from the global namespace

            method_args = task_def.get('method_args')
            if method_args:
                self.method_args = self.parseMethodArgs(method_args)
            else:
                self.method_args = {}
        else:
            if self.id:
                print 'defaulting to WFTask class: ' + str(self.id)
            else:
                print 'defaulting to WFTask class: ' + str(self.task_def)

            self.class_ref = None
            ####self.class_ref = self

        task_defs = task_def.get('task_defs')
        if task_defs:
            #print 'HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH 1', self.wf_ref.wf_name
            self.task_defs = self.loadSubtasks(task_defs)
            #print 'HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH 2', self.wf_ref.wf_name

        subtask_def_list = task_def.get('task_list') # get the list of subtask definitions
        if subtask_def_list:
            self.task_list = self.loadSubtasks(subtask_def_list)

        #debug
        print class_ref_id
        print 'wfm memory: ', self._wfm_ref._wfm_mem
        print 'WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW wf memory: ', self.wf_ref._local_lut


    def loadSubtasks(self, subtask_def_list):
        task_list = [] # initialize
        for idx, subtask_def in enumerate(subtask_def_list):
            #print '***************************', subtask_def
            subtask_id = subtask_def['task'].get('id')
            subtask_ref = None # initialize
            if subtask_id: # task definition has an id
                ## the following memory lookup may have a namespace conflict if workflow tasks use the same id
                ## gotta fix this
                ##
                ##
                subtask_ref = self.lookupTaskMemory(subtask_id) # use the cached object ref if available
                #if subtask_id == 'tid_init_task_screen':
                #    print 'DDDDDDDDDDDDDDDDDEEEEEEEEEEEEBUUUUUUUUUUUUUUUGGGGGGG 1', subtask_ref, self.wf_ref.wf_name
            if not subtask_ref:
                subtask_ref = WFTask(self._wfm_ref, self.wf_ref, subtask_def['task'], super_task=self) # instantiate anew
                ##if subtask_ref.id: # subtask has a valid id
                ##	self._local_lut[subtask_ref.id] = subtask_ref # store the instance ref in the local lut
                #if subtask_id == 'tid_test_and_branch_wf':
                #	print 'DDDDDDDDDDDDDDDDDEEEEEEEEEEEEBUUUUUUUUUUUUUUUGGGGGGG 2', subtask_ref, self.wf_ref.wf_name
            print subtask_ref
            task_list.append(subtask_ref)

        return task_list

    # static function
    @staticmethod
    def synchronizeTask(task_ref, task_idxs, event=None, loop_limit=0): #**kwargs):
        # loop until predicate turns True
        #while True:
            #ret_val = sync_method(**kwargs)
            #if self.stop.is_set():
        ret_val_list = []
        #print '################## entering: synchronizeTask by', task_ref
        for cur_idx in task_idxs:
            subtask_result = task_ref.retrieveSubtaskResult(cur_idx)
            if subtask_result.__class__ == ThreadWithReturnValue:
                ret_val = subtask_result.join()
                task_ref.storeSubtaskResult(cur_idx, ret_val)
            elif subtask_result.__class__ == list and subtask_result[:1]: # in case subtask_result is a list of results including subsubtasks
                if subtask_result[0].__class__ == ThreadWithReturnValue: # the first entry, if any, belongs to the immediate subtask
                    ret_val = subtask_result[0].join()
                    task_ref.storeSubtaskResult(cur_idx, ret_val)
                else:
                    ret_val = subtask_result
            else:
                ret_val = subtask_result
            ret_val_list.append(ret_val)

        task_ref.idleLoop(event, loop_limit)

        return ret_val_list
                #return ret_val # Stop running this thread so the main Python process can exit.
        #	time.sleep(1)

    def idleLoop(self, event=None, event_name='', loop_limit=0): #iter_limit=0):
        iteration = 0
        #print 'entering idle loop... task: ', self
        self.logger.debug('idling on event: ' + str(event) + ' ' + event_name + str(self.__class__) + str(self))
        while True:
            if self.wf_ref.stop.is_set() or (event != None and event.is_set()) or (loop_limit > 0 and iteration >= loop_limit):
                # Stop running this thread so the main Python process can exit.
                self.logger.debug('idling on event: ' + str(event) + ' event is set! ' + str(self.__class__) + str(self))
                return
            iteration += 1
            #print('Infinite loop, iteration {}.'.format(iteration))
            time.sleep(1)

    def idleLoopNot(self, event=None, loop_limit=0): #iter_limit=0):
        iteration = 0
        #print 'entering idle loop... task: ', self
        self.logger.debug('idling on anti-event: ' + str(event) + ' ' + str(self.__class__) + str(self))
        while True:
            if self.wf_ref.stop.is_set() or (event != None and not(event.is_set())) or (loop_limit > 0 and iteration >= loop_limit):
                # Stop running this thread so the main Python process can exit.
                self.logger.debug('idling on anti-event: ' + str(event) + ' event is unset! ' + str(self.__class__) + str(self))
                return
            iteration += 1
            #print('Infinite loop, iteration {}.'.format(iteration))
            time.sleep(1)

    def retrieveSubtaskResult(self, subtask_idx):
        return self._local_lut.get('_subtask_ret_'+ str(subtask_idx))

    def storeSubtaskResult(self, subtask_idx, ret_val):
        self._local_lut['_subtask_ret_'+ str(subtask_idx)] = ret_val # store subtask results

    def getFlowCtrl(self, default_fc=None):
        if self.flow_control:
            return self.flow_control
        elif default_fc:
            return default_fc # user supplied default
        else:
            return WFTask.SEQUENCE # default flow control

    def executeThreadTask(self, fc_default=None, **kwargs):
        flowcontrol = self.getFlowCtrl(fc_default)
        #print 'ZZZZZZZZZ entering task to thread: ', self.id, flowcontrol, self.wf_ref.wf_name
        self.logger.debug('threaded task execution: ' + str(self.__class__) + str(self) + ' ' + str(kwargs))
        ret_val = ThreadWithReturnValue(target=self.executeTask, kwargs={'fc_default': flowcontrol, 'kwargs':kwargs})
        ret_val.start()
        return ret_val

    def executeTask(self, fc_default=None, **kwargs):
        try:
            #print 'EXCUTTTTTTTTTTTTTTTTTTTTTTTING___TASKKKKKKKKKKK kwargs: ', self.id, self, kwargs
            self.logger.debug('task execution: ' + str(self.id) + str(self.class_name) + str(self.method) + str(self.method_args) + ' ' + str(kwargs))
            ##if kwargs:
            ##    self._runtime_kwargs = kwargs
            ret_val = None
            #task_list = self.task_list
            flowcontrol = self.getFlowCtrl(fc_default)
            #print 'entering task: ', self, flowcontrol, self.id, self.wf_ref.wf_name
            #if self.wf_ref.wf_name == 'init_wf':
                #print 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ 1', self.id, threading.current_thread()
                #print 'TDTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT ', self.wf_ref.task_defs
                #print 'TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT ', self.wf_ref.task_list
            #if self.wf_ref.wf_name == 'imagechooser_wf':
            #	print 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ 2', self.id

            #if self.id == 'tid_load_next_wf':
            #	print 'SSSSSSSSSSSSSSSSSSSSSSSSSSSSS ', self

            # finally execute the current task's method if any
            #print 'executing task... current class: ', self.class_ref, 'fc: ', flowcontrol, self.id, self.wf_ref.wf_name, threading.current_thread()
            #if self.method:
            #print 'executing method: ', self.method, self.method_args, self.id
            # execute exception cases first
            # if the current task has a circular method call
            if self.method and self.method.func_name in ['executeTask', 'executeThreadTask'] and self.method.im_self is self:
                ret_val = 'a circular method call '
                if self.id:
                    ret_val += str(self.id)
                self.storeSubtaskResult(0, ret_val)
                raise Exception()
            ret_val = None
            if flowcontrol == WFTask.SWITCH:
                ret_val = self.handleSwitchFlow()
            elif self.method:
                if flowcontrol == WFTask.SEQUENCE:
                    ret_val = self.method(**(self.parseRuntimeMethodArgs(self.method_args)))
                elif flowcontrol == WFTask.THREAD_SPLIT:
                    ret_val = ThreadWithReturnValue(target=self.method, kwargs=self.parseRuntimeMethodArgs(self.method_args))
                    ret_val.start()
                elif flowcontrol == WFTask.SYNCHRONIZATION:
                    ret_val = self.method(**(self.parseRuntimeMethodArgs(self.method_args))) #self.method, **(self.method_args))
                    if self._super_task:
                        ret_val_sub = WFTask.synchronizeTask(self._super_task, range(1, len(self._super_task.task_list)+1))
                        sup_ret_val = ret_val_sub.append(ret_val)
                        self._super_task.storeSubtaskResult(0, sup_ret_val) # store task result
                        if not ret_val:
                            ret_val = ret_val_sub
            elif self.class_ref and self.class_ref.__class__ == WFTask and self.class_ref is not self:
                ret_val = self.class_ref.executeTask(fc_default)
            self.storeSubtaskResult(0, ret_val) # store task result

            # do something to run task according to flow_control type
            # execute subtasks if available
            for tidx, cur_task in enumerate(self.task_list):
                ret_val = None
                cur_task_fc = cur_task.getFlowCtrl(flowcontrol) # use parent task's fc as default
                #print 'current subtask fc: ', cur_task_fc, cur_task_fc.__class__, cur_task
                if cur_task_fc in [WFTask.SEQUENCE, WFTask.SYNCHRONIZATION, WFTask.SWITCH]:
                    if cur_task is not self:
                        #print '@@@@@@@@########@@@@@@@@FFFFFF'
                        ret_val = cur_task.executeTask(flowcontrol)
                elif cur_task_fc == WFTask.THREAD_SPLIT:
                    if cur_task is not self:
                        #print '########@@@@@@@@@#######$$$$$$$'
                        ret_val = cur_task.executeThreadTask(flowcontrol)
                #ret_val = cur_task.executeTask(flowcontrol)
                #elif flowcontrol == WFTask.SYNCHRONIZATION:
                #	if self.method:
                #		cur_task.method(**(self.method_args)) #self.method, **(self.method_args))
                self.storeSubtaskResult(tidx+1, ret_val)


    #		if flowcontrol == WFTask.SEQUENCE:
    #			if self.method and self.method.func_name not in ['executeTask']:
    #				ret_val = self.method(**(self.method_args))
    #			self.storeSubtaskResult(0, ret_val) # store task result
    #		elif flowcontrol == WFTask.THREAD_SPLIT:
    #			if self.method and self.method.func_name not in ['executeTask']:
    #				ret_val = ThreadWithReturnValue(target=self.method, kwargs=self.method_args)
    #				ret_val.start()
    #			self.storeSubtaskResult(0, ret_val) # store task result
    #		elif flowcontrol == WFTask.SYNCHRONIZATION:
    #			if self.method and self.method.func_name not in ['executeTask']:
    #				ret_val = self.method(**(self.method_args)) #self.method, **(self.method_args))
    #				self.storeSubtaskResult(0, ret_val) # store task result
    #			if self._super_task:
    #				ret_val_sub = WFTask.synchronizeTask(self._super_task, range(1, len(self._super_task.task_list)+1))
    #				sup_ret_val = ret_val_sub.append(ret_val)
    #				self._super_task.storeSubtaskResult(0, sup_ret_val) # store task result
    #				if not ret_val:
    #					ret_val = ret_val_sub
    #			print ret_val
    #		elif flowcontrol == WFTask.SWITCH:
    #			self.handleSwitchFlow()
    #			print 'XXXXXXXXXXXXXXXXXXXXX exiting switch loop ', self.id, self.wf_ref.wf_name

            #else:
            #	if flowcontrol == WFTask.SYNCHRONIZATION: #and self._super_task:
            #		print 'executing synchronizeTask by default procedure'
            #		if self._super_task:
            #			ret_val = WFTask.synchronizeTask(self._super_task, range(1, len(self._super_task.task_list)+1))
            #			self.storeSubtaskResult(0, ret_val) # store task result
                #if flowcontrol == WFTask.SEQUENCE:
                #elif flowcontrol == WFTask.THREADING:
                #	threading.Thread(target=self.method)
                #else:
                #	pass

            self.task_results = ret_val
            return ret_val
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst

            raise # re-raise Exception

    def handleSwitchFlow(self):
        switch_cond = self.task_def.get('switch_cond')
        switch_idx = 1
        self.logger.debug('entering switch idle loop: ' + str(switch_cond) + ' ' + str(self))
        while True:
            for cond in switch_cond:
                #print '++++++++++++++++++++++++++ ######################: ', cond, self.wf_ref.wf_name
                switch_case = cond.get('case')
                python_predicate = switch_case.get('predicate')
                if self.lookupTaskMemory(python_predicate):
                    #print 'PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP ', python_predicate, self.wf_ref.wf_name
                    trigger = self.lookupTaskMemory(switch_case.get('trigger'))
                    trigger_args = switch_case.get('trigger_args')
                    parsed_trigger_args = {}
                    if trigger_args:
                        parsed_trigger_args = self.parseMethodArgs(trigger_args)
                    trigger(**parsed_trigger_args) # invoke the method with optional arguments
                    self.logger.debug('exiting switch idle loop: satisfied predicate ' + str(python_predicate) + ' ' + str(self))
                    return switch_idx # exit the case loop at the first instance of a condition being satisfied and its associated action being triggered.
                switch_idx += 1 # increment the idx
            if self.wf_ref.stop.is_set():
                return 0
            #print 'idle loop in the switch...'
            time.sleep(1)
        return -1




class WFWorkflow(WFTask):

    #constants
    WF_INIT = 0
    WF_READY = 1
    WF_LOADING_CONFIG = 2

    wfm_root = None
    wf_name = None
    _wf_def = {} # a placeholder for parsed workflow definition (e.g. a yaml file)
    global_vars = []
    restricted_tasks = []

    logger = logging.getLogger('grabia_app.gbia_wf.wf_manager.WFWorkflow')
    def __init__(self, wf_def, wfm_root=None, super_task=None, **kwargs):
        try:
            self.wf_name = wf_def.get('wf_name')
            self.logger.debug('$$$$$$$$$$$$$$$$$$$$$$$ initializing WFWorkflow...'+self.wf_name)
            self.wfm_root = wfm_root
            #self._local_lut = {} # initialize
            self.global_vars = wf_def.get('global_vars')
            if not self.global_vars:
                self.global_vars = []
            self.restricted_tasks = wf_def.get('restricted_tasks')
            if not self.restricted_tasks:
                self.restricted_tasks = []

            super(WFWorkflow, self).__init__(wfm=wfm_root, wf_ref=self, task_def=wf_def, super_task=super_task, **kwargs)
            self._local_lut['self'] = self
            self._wf_def = wf_def
            self.__status = WFWorkflow.WF_INIT
            self.logger.debug('$$$$$$$$$$$$$$$$$$$$$$$ initialized WFWorkflow'+self.wf_name)
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst

            raise # re-raise Exception

    # def checkConfigSyntax(self)

    @classmethod
    def fromYAML(cls, yaml_config, wfm_root=None):
        try:
            with open(yaml_config, 'r') as cfile:
                wf_task_def = yaml.load(cfile.read())

            #print '################### ', wfm_root
            cref = cls(wf_task_def.get('workflow'), wfm_root, None)
            #print '################### ', wfm_root
            #print '################### ', wfm_root
            #print '################### ', wfm_root
            #print '################### ', wfm_root
            #print '################### ', wfm_root
            wf_def = cref._wf_def.get('task_list')
            ##cref.loadTaskDefs(wf_def)
            return cref

        except Exception as inst:
            print type(inst)
            print inst.args
            print inst

            raise # re-raise Exception

    def loadTaskDefs(self, task_defs):
        #self.task_list = [] # initialize
        for idx, task_def in enumerate(task_defs):
            #print '>>>>>>>>>>>>>>>>>>>>> loading taskdef: ', task_def
            task_ref = WFTask(self._wfm_ref, self, task_def['task'], super_task=self)
            if task_ref.id: # task has a valid id
                self.addToLookupTable(task_ref.id, task_ref) # add to workflow manager's top-level lookup table

    # override super's method to support lazy instantiation of referenced classes
    # def lookupTaskMemory(self, memory_key):


    def addToLookupTable(self, key, value):
        self._local_lut[key] = value


    def removeFromLookupTable(self, key):
        self._local_lut[key] = None
        self._local_lut.pop(key)


class WorkflowManager():
    stop = ThreadEvent()
    #constants

    _wf_def_lut = {} # a placeholder for parsed workflow definitions (e.g. from yaml files)
    _wf_import_paths = {}
    _wfm_mem = {}
    logger = logging.getLogger('grabia_app.gbia_wf.wf_manager.WorfklowManager')

    def onStop(self, *args):
        for wf in self._wf_def_lut.values():
            wf.stop.set()

    @staticmethod
    def onStopListener(dispatcher_ref):
        #print 'waiting for stop trigger: ', dispatcher_ref
        dispatcher_ref.stop.wait()
        print '*********************** stop set!'
        dispatcher_ref.onStop()

    def dispatchListner(self):
        Thread(target=WorkflowManager.onStopListener, args=(self,)).start()

    def __init__(self, wf_import, init_mem_list=[]):
        try:
            self.dispatchListner()
            print '>>>>>>>>>>>> initializing WFLUT'
            self.initializeWFLUT(wf_import)
            for init_mem in init_mem_list:
                mem_id = init_mem.get('id')
                if mem_id:
                    mem_obj_ref = init_mem.get('obj_ref')
                    self._wfm_mem[mem_id] = mem_obj_ref

            print 'initialized'
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    # def checkConfigSyntax(self)
    def lookupWFMMemory(self, mem_id):
        return self._wfm_mem.get(mem_id)


    def executeWorkflow(self, wf_name, **kwargs):
        #self._runtime_kwargs = kwargs
#        if kwargs:
#            self._runtime_kwargs = kwargs
        #self.logger.debug('EXECUTEWORKFLOWWWWWWWWWWWWWWWWW '+ str(wf_name) + ' ' + str(kwargs))
        self.logger.debug('executeWorkflow: (' + wf_name + ', ' + str(kwargs) + ')' + str(self))
        wf_handle = self.getWorkflow(wf_name)

        if kwargs:
            for arg_name, arg_val in kwargs.iteritems():
                wf_handle.storeRuntimeTaskArg(arg_name, arg_val)
        return wf_handle.executeThreadTask() #**kwargs)
        # if wf_handle.flow_control == WFTask.THREAD_SPLIT:
        #     return wf_handle.executeThreadTask()
        # else:
        #     return wf_handle.executeTask()

    def getWorkflow(self, wf_name):
        parsed_wf = None
        # check cached workflow
        parsed_wf = self._wf_def_lut.get(wf_name)
        # if not in the cache
        if not parsed_wf:
            parsed_wf = self.addWorkflowByName(wf_name) # try to add the workflow definition, if available

        self.logger.debug('getWorkflow: (' + wf_name + ', ' + str(parsed_wf) + ')' + str(self))
        if parsed_wf:
            self.logger.debug('getWorkflow('+str(wf_name)+') got '+ str(parsed_wf))
        else:
            self.logger.debug('getWorkflow('+str(wf_name)+') got nothing!!!!')

        return parsed_wf

    def initializeWFLUT(self, wf_import):
        try:
            with open(wf_import, 'r') as cfile:
                parsed_wf_import = yaml.load(cfile.read())
            for wf in parsed_wf_import.get('import'):
                print '$$$$$$$$$$$$$$$$$$$$$$ ', wf
                (wf_id, wf_import_path) = wf.popitem()
                self._wf_import_paths[wf_id] = wf_import_path

        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def addWorkflowByName(self, wf_name):
        wf_import_path = self._wf_import_paths.get(wf_name)
        #print '777777777777777777 ', wf_name, self._wf_import_paths
        if wf_import_path:
            return self.addWorkflowFromYAML(wf_name, wf_import_path)
        return None

    def addWorkflowFromYAML(self, wf_name, yaml_config):
        try:
            with open(yaml_config, 'r') as cfile:
                wf_task_def = yaml.load(cfile.read())

            #print 'initializing workflow ', wf_name
            self.logger.debug('initializing workflow: ' + wf_name + ' ' + str(self))
            parsed_wf = WFWorkflow(wf_task_def.get('workflow'), self, None) # second arg passes wfm ref to workflow instance
            #print '################### addWorkflowFromYAML', parsed_wf, parsed_wf.wfm_root, parsed_wf._local_lut
            self._wf_def_lut[wf_name] = parsed_wf
            #for wf_lut_inst in self._wf_def_lut.items():
            #    print 'WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW ', wf_lut_inst[0], wf_lut_inst[1]._local_lut #wf_name, parsed_wf._local_lut
            return parsed_wf
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def removeWorkflowByName(self, wf_name):
        #print 'RRRRRRRRRRRRRRRRRRRRRR removing workflow: ', wf_name, self
        if self._wf_def_lut.has_key(wf_name):
            wf_lut_inst = self._wf_def_lut.pop(wf_name)
            self.logger.debug('removeWorkflowByName: (' + wf_name + ',' + str(wf_lut_inst) + ')' + str(self))
        #print 'WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWPPP ', wf_name, wf_lut_inst._local_lut




