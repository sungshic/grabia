__author__='spark'

from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty

from gbia_screens.base.filebrowser import FileBrowser

Builder.load_string('''
<ImageSequenceBrowser>:
	clear_list: [popup_demo]
	background: background
	cur_path: None
	BoxLayout:
		orientation: 'vertical'
		canvas.before:
			Color:
				rgba: 0,0,0,1
			Rectangle:
				pos: self.pos
				size: self.size
		FloatLayout:
			#on_touch_down: self.is_dependency_complete = True # app.wfm.executeTask()
			Button:
				text: 'Select image folder'
				pos_hint: {'x': .2, 'y': .8}
				size_hint: .2, .1
				#theme: ('green', 'accent')
				on_release: popup_demo.open()
				popup_demo: popup_demo.__self__
			Button:
				text: 'Next'
				pos_hint: {'x': .6, 'y': .8}
				size_hint: .2, .1
				#theme: ('green', 'accent')
				on_release: popup_demo.open()
				popup_demo: popup_demo.__self__
			Scatter:
				id: background
				do_rotation: False
				do_translation: False
				do_scale: False
				Image:
					source: root.source
					width: root.width
					height: root.height / 2.0
			Popup:
				id: popup_demo
				title: 'Flat popup demo'
				title_color_tuple: ('Gray', '1000')
				size_hint: .9, .9
				on_parent: if self.parent: self.parent.remove_widget(self)

				FileBrowser:
					select_string: 'Select'
				#MPLFigureWidget:
				#	size_hint: 1, 1
				#	#size: root.size
				#	pos: self.pos

				#GBIA_MPL_Image:
					#mplcanvas: _mplcanvas
					#BoxLayout:
					#	id: _mplcanvas
					#	orientation: 'vertical'
				#GBIAImgCropWidget:
				#	size: root.size
				#	pos: root.pos
				#	img_src: 'gbia_widgets/testimg.jpg'
					#text: "hello world"
''')

class FileBrowserScreen(Screen):
	source = StringProperty()
	is_dependency_complete = ObjectProperty(None)

	def __init__(self, **kargs):
		super(FileBrowserScreen, self).__init__(**kargs)

	def doReturn(self):
		print 'executed doReturn'
		pass

class ImageSequenceBrowser(FileBrowserScreen):

	def __init__(self, **kargs):
		super(ImageSequenceBrowser, self).__init__(**kargs)

