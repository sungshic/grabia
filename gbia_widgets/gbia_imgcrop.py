from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button

from kivy.uix.scatter import Scatter
from kivy.uix.image import Image, AsyncImage
from kivy.core.image import Image as CoreImage
from kivy.core.image import ImageData

from kivy.uix.boxlayout import BoxLayout

from kivy.properties import ObjectProperty,  NumericProperty, StringProperty

from kivy.graphics import Line, Rectangle, Color
from kivy.graphics.texture import Texture
from kivy.core.image.img_pygame import ImageLoaderPygame
from kivy.core.image.img_pil import ImageLoaderPIL

from kivy.lang import Builder

from PIL import Image as PILImage
import StringIO
import io

import matplotlib as mpl
#mpl.use("TkAgg") 
#mpl.use('module://kivy.garden.matplotlib.backend_kivy')
#from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.backend_bases import MouseEvent
#import Tkinter as tk

import numpy as np
from mpl_toolkits.mplot3d import Axes3D


Builder.load_file('gbia_widgets/gbia_imgcrop.kv')

MPL_DPI=60

class MPLFigureWidget(Widget): #BoxLayout):
    fig = None
    axe = None
    fig_disp_data = None
    disp_dataval = None
    memimage = ObjectProperty(None)
    mplcanvas = ObjectProperty(None)
    tmp_cur_data = [1,2,3]

    scatter = ObjectProperty(None)
    boxlayout = ObjectProperty(None)
    img_src = StringProperty(None)
    pil_image = None

    #gbia workflow manager handle


    def mpl_on_click(self, event):
        # 'button_press_callback': whenever a mouse button is pressed
        if event.dblclick:
            print("DBLCLICK", event.x, event.y)
        else:
            print("DOWN    ", event.x, event.y)

    def mpl_on_release(self, event):
        # 'button_release_callback': whenever a mouse button is released
        #print("UP     ", event)
        pass

    def mpl_on_drag(self, event):
        pass
        # 'motion_notify_event': whenever mouse is moved
        #print("MOUSE moved   ", event.x, event.y)


    def __init__(self, **kargs):
        super(MPLFigureWidget, self).__init__(**kargs)
        print '##################### here'
        #self.fig = plt.figure(1) #Figure(figsize=(5,5), dpi=100)
        self.fig = Figure(figsize=(5,5), dpi=100)
        self.mplcanvas = FigureCanvas(self.fig)
        #self.axe = self.fig.add_subplot(1,1,1)
        #self.axe.plot(self.tmp_cur_data)
        self.axe = self.fig.gca(projection='3d')
        theta = np.linspace(-4*np.pi, 4*np.pi, 100)
        z = np.linspace(-2,2,100)
        r = z**2 + 1
        x = r * np.sin(theta)
        y = r * np.cos(theta)
        self.axe.plot(x,y,z, label='parametric curve')

        # setup mpl callbacks
        self.fig.canvas.mpl_connect('button_press_event', self.mpl_on_click)
        self.fig.canvas.mpl_connect('button_release_event', self.mpl_on_release)
        self.fig.canvas.mpl_connect('motion_notify_event', self.mpl_on_drag)

        # setup kivy callbacks
        #self.bind(on_touch_down=self.on_touch_down)
        #self.bind(on_touch_move=self.on_touch_move)

        ##buf, size = self.fig.canvas.print_to_buffer()
        ##self.pil_image = PILImage.frombuffer('RGBA', size, buf, 'raw', 'RGBA', 0, 1)
        #self.disp_dataval = self.ImageData(pil_image.size[0], pil_image.size[1], 'RGBA', pil_image.tostring(), source=None)
        ##print ImageData._supported_fmts
        ##self.fig_disp_data = ImageData(self.pil_image.size[0], self.pil_image.size[1], 'rgba', self.pil_image.tobytes(), source=None)

        #if self.memimage:
        #	self.memimage.memory_data = self.fig_disp_data
        #plt.ioff()
        #plt.draw()
        self.fig_disp_data = StringIO.StringIO()
        self.mplcanvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
        print 'disp mem class: ', str(self.fig_disp_data.__class__)
        self.disp_dataval = self.fig_disp_data.getvalue()
        if self.memimage:
            self.memimage.memory_data = self.fig_disp_data##.getvalue()
            print 'debug: ', self.memimage._filename
        ###self.memimage = MemoryImage(self.fig_disp_data.getvalue())
        #self.memimage.memory_data = self.fig_disp_data.getvalue()
        #with self.canvas:
        #self.boxlayout.add_widget(self.memimage)
        ###self.add_widget(self.memimage)

    def on_touch_move(self, touch):
        #print "debuggggg mouse moved ", touch.pos
        ##buf, size = self.fig.canvas.print_to_buffer()
        ##self.pil_image = PILImage.frombuffer('RGBA', size, buf, 'raw', 'RGBA', 0, 1)
        ##self.fig_disp_data = ImageData(self.pil_image.size[0], self.pil_image.size[1], 'rgba', self.pil_image.tobytes(), source=None)
        ##self.memimage.memory_data = self.fig_disp_data

        self.fig_disp_data = StringIO.StringIO()
        self.mplcanvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
        self.memimage.memory_data = self.fig_disp_data##.getvalue()
        mpl_event_name = 'motion_notify_event'
        (mpl_x, mpl_y) = self.convertKivyPosToMPLPos(touch.pos, self.memimage)
        mpl_mouseevent = MouseEvent(mpl_event_name, self.fig.canvas, x=mpl_x, y=mpl_y, button=1, key=self.fig.canvas._key, step=0, dblclick=touch.is_double_tap, guiEvent=None)
        self.fig.canvas.callbacks.process(mpl_event_name, mpl_mouseevent) # trigger mpl mouse event
        return True

    def on_touch_down(self, touch):
        # if the touch occurred outside the widget
        if not self.collide_point(*touch.pos):
            # see if the super class knows how to handle this touch
            super(ImageCropWidget,self).on_touch_down(touch)
        elif touch.button in( 'left'): # if left mouse button is clicked inside the widget
            #print "debuggggg left click, touch.pos ", self.to_window(*touch.pos)
            #print "debuggggg left click, self.pos ", self.pos
            #print "debuggggg left click, self.scatter.pos ", self.scatter.pos, self.scatter.x, self.scatter.y
            #print "debuggggg left click, self.boxlayout.pos ", self.convertKivyPosToMPLPos(touch.pos, self.boxlayout)
            #print "debuggggg left click, self.memimage.pos ", self.memimage.pos, self.memimage.width, self.memimage.height, self.memimage.img_loader.size
#			self.cur_selection_pos = touch.pos # keep a record of the first mouse click position
#			if self.cur_selection_area:
#				self.canvas.remove(self.cur_selection_area)
#			self.cur_selection_area = None
            mpl_event_name = 'button_press_event'

            (mpl_x, mpl_y) = self.convertKivyPosToMPLPos(touch.pos, self.memimage)
            mpl_mouseevent = MouseEvent(mpl_event_name, self.fig.canvas, x=mpl_x, y=mpl_y, button=1, key=self.fig.canvas._key, step=0, dblclick=touch.is_double_tap, guiEvent=None)
            self.fig.canvas.callbacks.process(mpl_event_name, mpl_mouseevent) # trigger mpl mouse event

            #self.tmp_cur_data = [a * 2 for a in self.tmp_cur_data]
            #self.axe.plot(self.tmp_cur_data)
            #plt.ioff()
            #plt.draw()
            ##buf, size = self.fig.canvas.print_to_buffer()
            ##self.pil_image = PILImage.frombuffer('RGBA', size, buf, 'raw', 'RGBA', 0, 1)
            ##self.fig_disp_data = ImageData(self.pil_image.size[0], self.pil_image.size[1], 'rgba', self.pil_image.tobytes(), source=None)
            ##self.memimage.memory_data = self.fig_disp_data
            self.fig_disp_data = StringIO.StringIO()
            self.mplcanvas.print_figure(self.fig_disp_data, dpi=MPL_DPI)
            #print 'disp mem class: ', str(self.fig_disp_data.__class__)
            self.memimage.memory_data = self.fig_disp_data##.getvalue()
            #self.memimage.on_memory_data()

        return True

    def convertKivyPosToMPLPos(self, touch_pos, kivy_widget):
        kivy_pos = kivy_widget.pos
        ##memimg_width = self.pil_image.size[0] ##self.memimage.img_loader.width
        ##memimg_height = self.pil_image.size[1] ##self.memimage.img_loader.height
        memimg_width = self.memimage.img_loader.width
        memimg_height = self.memimage.img_loader.height
        #memimg_len = max(memimg_width, memimg_height)
        kivyimg_width = kivy_widget.width
        kivyimg_height = kivy_widget.height
        #kivy_len = min(kivy_widget_width, kivy_widget_height)

        # convert from Kivy to GBIA coordinate
        width_ratio = memimg_width / kivyimg_width
        height_ratio = memimg_height / kivyimg_height

        ratio = max(width_ratio, height_ratio)
        ##print 'KivyToGBIA ratio: ', ratio

        memimg_kivy_width = memimg_width / ratio
        memimg_kivy_height = memimg_height / ratio

        ##print 'memimg kivy size: ', memimg_kivy_width, memimg_kivy_height

        offset_pos = (abs(kivyimg_width - memimg_kivy_width)/2.0, abs(kivyimg_height - memimg_kivy_height)/2.0)
        #print 'offset_pos: ', offset_pos
        gbia_xpos = touch_pos[0]-self.memimage.x-offset_pos[0]
        gbia_ypos = touch_pos[1]-self.memimage.y-offset_pos[1]

        # convert from GBIA to MPL coordinate and finally return MPL coordinate
        mpl_width_ratio = memimg_width / memimg_kivy_width
        mpl_height_ratio = memimg_height / memimg_kivy_height

        #print 'convertion: ', touch_pos, gbia_xpos, gbia_ypos, mpl_width_ratio, mpl_height_ratio

        return (gbia_xpos * mpl_width_ratio, gbia_ypos * mpl_height_ratio)







class GBIAMemoryImage(AsyncImage):
    """Display an image already loaded in memory."""
    memory_data = ObjectProperty(None)
    img_loader = ObjectProperty(None)
    # Kivy 1.9 does not support uix.Image to be initialized from memory-based image data.
    # matplotlib integration through image transfer requires the use of memory-based image data
    # the following hack will save image data into a temporary file for Kivy to be happy for now...
    _filename = '_tmp_io_file.png'

    def __init__(self, **kwargs): #(self, memory_data, **kwargs):
        super(GBIAMemoryImage, self).__init__(**kwargs)

        print 'debuggg: MemoryImage initializing... '
        #self.memory_data = memory_data
        if self.memory_data:
            ##data = StringIO.StringIO(self.memory_data)
            self.img_loader = ImageLoaderPygame(filename=self._filename, ext='png', inline=True, rawdata=self.memory_data, nocache=True)


    def on_memory_data(self, *args):
        """Load image from memory."""
        #data = StringIO.StringIO(self.memory_data)
        if self.memory_data:
            #data = io.BytesIO(self.memory_data)
            ##data = StringIO.StringIO(self.memory_data)
            ####with open(self._filename, 'wb') as f:
            ####	f.write(data.read())
            #CoreImage can be instantiated from memory-based image data
            #loaded_img = CoreImage(data, ext="png")
            #with self.canvas:
                #AsyncImage(source='self.filename')
            #print '#################################: ', self._filename
            #self.img_loader = ImageLoaderPygame(self._filename, nocache=True)
            #self.img_loader.filename = self._filename
            ##self.img_loader._data = self.img_loader.load(data) #self._filename)
            ##self.texture = Texture.create_from_data(self.img_loader._data, self.img_loader._mipmap)
            ####self.img_loader = ImageLoaderPygame(filename=self._filename, nocache=True)
            self.img_loader = ImageLoaderPygame(filename=self._filename, ext='png', inline=True, rawdata=self.memory_data, nocache=True)
            self.texture = self.img_loader.texture
            ##self.img_loader = ImageLoaderPygame(filename=None, ext='png', inline=True, rawdata=self.memory_data, nocache=True)
                ##self.add_widget(Image(source=self._filename))
                #im._coreimage = loaded_img
                #self.add_widget(im)
                 #Rectangle(texture=loaded_img.texture)

class GBIA_MPL_Image(Widget):
    pass

class GBIAImgCropWidget(Widget):
    pass


class ImageCropWidget(Widget):
    scatter = ObjectProperty(None)
    image = ObjectProperty(None)
    rect = ObjectProperty(None)
    toolbox = ObjectProperty(None)
    xmin = NumericProperty(20)
    xmax = NumericProperty(40)
    ymin = NumericProperty(20)
    ymax = NumericProperty(40)

    img_src = StringProperty(None)

    cur_selection_area = None
    cur_selection_pos = None

    def __init__(self, **kargs):
        super(ImageCropWidget, self).__init__(**kargs)
        #self.bind( size = self.rescale_rec)
        #self.oldsize = self.size
        print 'debugggggg'
        print self.size
        print self.pos
        #self.hideLimits()
        #self.defaultLimits()
        self._resizing = []
        
    def defaultLimits(self):
        self.xmin = self.width/6.
        self.xmax = self.width - self.xmin
        self.ymin = self.width/6.
        self.ymax = self.height - self.ymin

    def hideLimits(self):
        self.xmin = 0
        self.xmax = self.width
        self.ymin = 0
        self.ymax = self.height


        
    def rescale_rec(self,*l):
        print 'debuggggggggggg resize ', self.oldsize, self.size

        if self.oldsize[0] != 0:
            f_w = float(self.size[0])/self.oldsize[0]
        else:
            f_w = float(self.oldsize[0])
        if self.oldsize[1] != 0:
            f_h = float(self.size[1])/self.oldsize[1]
        else:
            f_h = float(self.oldsize[1])
        self.xmin *= f_w
        self.ymin *= f_h
        self.xmax *= f_w
        self.ymax *= f_h
        self.oldsize = self.size

    def on_touch_down(self, touch):
        # if the touch occurred outside the widget
        if not self.collide_point(*touch.pos):
            # see if the super class knows how to handle this touch
            super(ImageCropWidget,self).on_touch_down(touch)
        elif touch.button in( 'left'): # if left mouse button is clicked inside the widget
            print "debuggggg left click ", touch.pos
            self.cur_selection_pos = touch.pos # keep a record of the first mouse click position
            if self.cur_selection_area:
                self.canvas.remove(self.cur_selection_area)
            self.cur_selection_area = None

        return True

    def on_touch_move(self, touch):
        # if the touch occurred inside the widget
        if self.collide_point(*touch.pos):
            cur_pos = touch.pos
            cur_size = (cur_pos[0] - self.cur_selection_pos[0], cur_pos[1] - self.cur_selection_pos[1])
            if self.cur_selection_area is None:
                with self.canvas:
                    self.cur_selection_area = Line(rectangle=self.cur_selection_pos+cur_size) # initialize
            else:
                with self.canvas:
                    self.cur_selection_area.rectangle = self.cur_selection_pos+cur_size # update size
        else:
            super(ImageCropWidget,self).on_touch_move(touch)

        return True

    def on_touch_up(self, touch):
        if self.cur_selection_area:
            print 'selection: ', self.cur_selection_area.points
            print 'image: ', self.image.pos, self.image.size
            print 'scatter pos: ', self.scatter.pos
            print 'scale: ', self.scatter.scale
        # if the touch occurred outside the widget
        if not self.collide_point(*touch.pos):
            super(ImageCropWidget,self).on_touch_up(touch)
        return True

    def reset(self):
        self.scatter.rotation = 0
        self.scatter.scale = 1.
        self.scatter.pos = self.pos
        self.hideLimits()

class GBIAImageEditor(BoxLayout):
    cropW = ObjectProperty(None)
    #buttonBox = BoxLayout(id='_buttonBox', size_hint=(1,None), orientation='horizontal') # ObjectProperty(None)
    #ButtonClass= ObjectProperty(Button)
    buttonBox = ObjectProperty(None)

    img_src = StringProperty(None)

    
    def __init__(self, **kargs):
        super(GBIAImageEditor, self).__init__(**kargs)
        #BC  = self.ButtonClass
        cancelB = Button(text='cancel')
        cancelB.bind( on_release = self.canceled )
        self.buttonBox.add_widget( cancelB)
        cb = Button(text='Crop')
        cb.bind( on_press = self.crop_pressed)
        self.buttonBox.add_widget( cb  )
        rb = Button(text='Rotate')
        rb.bind( on_press = self.rotate_pressed)
        self.buttonBox.add_widget( rb  )
        okb = Button(text='Ok')
        okb.bind( on_release = self.save_image)
        self.buttonBox.add_widget( okb   )

    def crop_pressed(self,*l):
        self.cropW.defaultLimits()
        pass

    def rotate_pressed(self,*l):
        self.cropW.scatter.rotation -=90.

    def canceled(self, *l):
        self.cropW.reset()
        
    def save_image(self, *l):
        from PIL import Image
        im = Image.open(self.img_src)
        scatter = self.cropW.scatter
        cropW = self.cropW

        # get the exact ratio, as seen on screen :
        #f_window = cropW.image.norm_image_size[0]/float(im.size[0])
        f_window = cropW.image.norm_image_size[0]/float(im.size[0])
        ##f_window *= scatter.scale

        # rotate as on screent
        im=im.rotate( scatter.rotation)


        # get the image position on screen, within the scatter widget
        size = int(im.size[0]*f_window) , int(im.size[1]*f_window)
        xmin_im = scatter.center_x -size[0]/2 - cropW.x
        xmax_im = xmin_im + size[0]
        ymin_im = scatter.center_y -size[1]/2 - cropW.y
        ymax_im = ymin_im + size[1]

        # resize the image as on screen
        im=im.resize( size, Image.ANTIALIAS )

        # compute crop coordinates (translating from on screen limits position)
        xmin = int(cropW.xmin-xmin_im)
        xmax = int(cropW.xmax-xmin_im)
        ymax = int( ymax_im -  cropW.ymax )
        ymin = int( ymax_im -  cropW.ymin )

        # crop : left, upper, right, and lower
        im=im.crop( (xmin, ymax, xmax, ymin) )

        name_fields = self.img_src.split('.')
        ext = name_fields[-1]
        new_name = '.'.join(name_fields[:-1])+'_edited.'+ext
        print new_name
        im.save( new_name)
        #im.show()
        pass

from kivy.factory import Factory
Factory.register("GBIAImageEditor",GBIAImageEditor)
Factory.register("ImageCropWidget",ImageCropWidget)

cw = GBIAImageEditor(img_src='testimg.jpg') #img_src=    "photo.jpeg"    )


class MyApp(App):
    def build(self):
        return cw


ap=MyApp()


if __name__ == '__main__':
    ap.run()
