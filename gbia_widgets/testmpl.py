import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt

def mpl_on_click(event):
	print ("MOUSE clicked: ", event.x, event.y)

def mpl_on_drag(event):
	print ("MOUSE moved: ", event.x, event.y)

mpl.rcParams['legend.fontsize'] = 10

fig = plt.figure()
ax = fig.gca(projection='3d')
theta = np.linspace(-4 * np.pi, 4 * np.pi, 100)
z = np.linspace(-2, 2, 100)
r = z**2 + 1
x = r * np.sin(theta)
y = r * np.cos(theta)
ax.plot(x, y, z, label='parametric curve')
ax.legend()

fig.canvas.mpl_connect('button_press_event', mpl_on_click)
fig.canvas.mpl_connect('motion_notify_event', mpl_on_drag)

plt.show()
