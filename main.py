__author__='spark'

import logging
import os
from os.path import expanduser
# create logger with 'grabia_app'
logger = logging.getLogger('grabia_app')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
logfile_dir = expanduser("~")+'/.grabia/log/'
logfile_name = 'grabia_app.log'
if not os.path.exists(logfile_dir):
    os.makedirs(logfile_dir)
fh = logging.FileHandler(logfile_dir+logfile_name)
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)
logger.debug('logger initialized')
#import os, random
#os.environ['KIVY_TEXT'] = 'pil'
import threading

from kivy.uix.widget import Widget
from kivy.lang import Builder

from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.screenmanager import FadeTransition
#from kivy.animation import Animation
from kivy.clock import Clock, mainthread

#from gbia_screens.init_screen import GBIAInitScreen
#from gbia_screens.mpl_test import MPLFigureScreen
#from flat_kivy.flatapp import FlatApp
#from flat_kivy.uix.flattextinput import FlatTextInput

#from gbia_widgets.gbia_settings import GBIA_Settings
#from gbia_widgets.gbia_imgcrop import GBIAImageEditor, GBIAImgCropWidget, MPLFigureWidget, GBIAMemoryImage

from gbia_wf.wf_manager import WorkflowManager
from gbia_screens.manager import GBIAScreenManager
from docker.backend import BackendManager
#from functools import partial

#Builder.load_file('main.kv')


class GraBIA(App):
    icon = 'grabia_icon.png'
    title = 'GraBIA v0.94 beta'
    wfm = None  # workflow manager handle
    scm = None  # screen manager handle
    backend_manager = None # docker backend manager handle

    def __init__(self):
        super(GraBIA, self).__init__()
        self.scm = GBIAScreenManager()
        self.backend_manager = BackendManager()
        print '################$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 1'
        self.wfm = WorkflowManager(wf_import='/grabia/gbia_wf/defs/wfm_init.yaml', init_mem_list=[{'id':'scm','obj_ref':self.scm}, {'id':'backend_m', 'obj_ref':self.backend_manager}])
        print '#################### ', self.wfm.lookupWFMMemory('scm')

        #self.wfm.executeWorkflow('stats_wf') #getWorkflow('init_wf').executeTask()
        self.wfm.executeWorkflow('init_wf')
        #self.wfm.executeWorkflow('imagechooser_wf')
        #self.wfm.executeWorkflow('dashboard_wf') #getWorkflow('init_wf').executeTask()
        #self.wfm.executeWorkflow('init_wf') #getWorkflow('init_wf').executeTask()
        #self.wfm.executeWorkflow('dashboard_wf')
        #self.wfm.addWorkflowFromYAML('test_wf', 'gbia_wf/defs/gbia_workflow_def.yaml')
        print '################$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 2'
#		print self.wfm.wfm_root

        #self.wfm.executeTask()

    def build(self):
        print '##################################$$$$$$$$$$$$$'
        #Clock.schedule_once(self.scm.initScreen, 10)
        return self.scm  # this will set self.root to self.scm

    def on_stop(self):
        # The Kivy event loop is about to stop, set a stop signal;
        # otherwise the app window will close, but the Python process will
        # keep running until all secondary threads exit.
        self.wfm.stop.set() # set stop threading event
        self.scm.stop.set() # set stop threading event
        self.backend_manager.stop.set()
        #print '###############################################################', self.root, self.scm, self.wfm
        #self.root.stop.set()

if __name__ == '__main__':
    GraBIA().run()


