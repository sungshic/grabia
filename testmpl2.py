import numpy as np
import matplotlib
#matplotlib.use('Agg')
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.widgets import RadioButtons, Button
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.backend_bases import MouseEvent

xc = np.arange(8) / 1 % 2 - 0.5
yc = np.arange(8) / 2 % 2 - 0.5
zc = np.arange(8) / 4 % 2 - 0.5
on_off = ('ON', 'OFF')
fig = plt.figure()

#fig = Figure()
#FigureCanvas(fig)

#rax = plt.axes([0.02, 0.10, 0.10, 0.15])
rax = fig.add_axes([0.02, 0.10, 0.10, 0.15])
RB = RadioButtons(rax, on_off)
#bax = plt.axes([0.1, 0.8, 0.1, 0.15])
bax = fig.add_axes([0.1, 0.8, 0.1, 0.15])
bax2 = fig.add_axes([0.1, 0.1, 0.1, 0.15])
btn = Button(bax, 'test', color='0.6', hovercolor='0.975')
btn2 = Button(bax2, 'test 2', color='0.6', hovercolor='0.975')
click_x = 3
click_y = 3

def onClick(event):
    print 'event ', event, dir(event)
    print 'xy coords: ', globals()['click_x'], globals()['click_y']
    globals()['click_x'] = event.x
    globals()['click_y'] = event.y
    print 'hello ', event.guiEvent

def onClick2(event):
    print 'world ', event.x, event.y, event.guiEvent, fig.get_figwidth(), fig.get_figheight(), fig.canvas.get_width_height(), event, bax2.get_position(), bax2.get_position().min, bax2.get_position().max, dir(bax2.get_position())
    #mpl_mouseevent = MouseEvent('button_press_event', fig.canvas, x=globals()['click_x'], y=globals()['click_y'], button=1, step=0, guiEvent=None)
    mpl_mouseevent = MouseEvent('button_press_event', fig.canvas, x=75, y=452, button=1, step=0, guiEvent=None)
    fig.canvas.callbacks.process('button_press_event', mpl_mouseevent) # trigger
    mpl_mouseevent2 = MouseEvent('button_release_event', fig.canvas, x=75, y=452, button=1, step=0, guiEvent=None)
    fig.canvas.callbacks.process('button_release_event', mpl_mouseevent2) # trigger
    fig.canvas.draw()
    return True

def mpl_on_click(event):
    print 'MPL DONW ', event.x, event.y

fig.canvas.mpl_connect('button_press_event', mpl_on_click)

btn.on_clicked(onClick)

#fig.canvas.mpl_connect('button_press_event', onClick2)
btn2.on_clicked(onClick2)


def redraw_c(label):
    global show_c        
    if label == 'ON':
        print " ON! "
        show_c = True
    else:
        print "OFF! "
        show_c = False
    go_plot()
    #plt.show()  # redundant
    fig.canvas.draw()  # redundant

def go_plot():        
    global show_c, show_s
    print "  go_plot called with show_c = ", show_c
    ax.clear()
    ax.plot(xc, yc, zc, 'ob', markersize = 2)  # just some dots for corner markers
    if show_c:
        ax.plot(xc, yc, zc, 'or', markersize = msize)
    fig.canvas.draw()  # redundant

RB.on_clicked(redraw_c)
msize = 20
show_c = True

ax  = fig.add_axes([0.15, 0.1, 0.80, 0.80], projection = '3d')
ax.plot(xc, yc, zc, 'or', markersize = msize)
ax.plot(xc, yc, zc, 'ob', markersize = 2)  # keep something visible
plt.show()
#fig.show()
